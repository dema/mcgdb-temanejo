#include<stdio.h>
#include "ayu_mpi.h"
#include <stdio.h>
#include <string.h>



int MPI_Send(AYU_MPI_COND_CONST void *buf, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm ) {
    int ierr;
    int my_comm;

    counterMPI++;

    my_comm = comm_id(comm);
    mpiElem elem = { aympi_rank, counterMPI,"MPI_Send", dest, tag, my_comm, NULL}; /*id,fname,rank,partner,tag,comm,request*/

#ifdef AYU_MPI_DEBUG

    printf ("MPI task: MyRank = %d | ID = %d | Name = %s | partner = %d | tag = %d | comm = %i \n", elem.rank, elem.id, elem.fname, elem.partner, elem.tag, elem.comm);
    fflush(stdout);
#endif

#ifdef AYUDAME

    if(ayu_event) {
        /* take a client ID and declaration of variables */
        ayu_client_id_t myclient_id = get_client_id(AYU_CLIENT_MPI);
        ayu_id_t scope_id = 0;
        ayu_id_t task_id = 0;
        ayu_id_t dependency_id;
        ayu_event_data_t data;
        data.common.client_id = myclient_id;

        task_id = counterMPI;

        data.add_task.task_id = task_id;
        data.add_task.scope_id = scope_id;
        data.add_task.task_label= "AYU_ADDTASK_MPI_Send";
        ayu_event(AYU_ADDTASK, data);
        ayu_wipe_data(&data);


        data.common.client_id = myclient_id;
        data.add_dependency.from_id = task_id - 1;
        data.add_dependency.to_id = task_id;
        // TODO rough hack
        dependency_id = 1000 + (task_id-1)*task_id;
        data.add_dependency.dependency_id = dependency_id;
        data.add_dependency.dependency_label= "AYU_ADDDEPENDENCY_MPI_Send";
        ayu_event(AYU_ADDDEPENDENCY, data);
        ayu_wipe_data(&data);


        /**** events for the matcher ****/
        mpi_matcher_t m_envelope[6] = {0}; /* indexes 0 -rank, -taskID, 1 - partner, 2 - tag, 3 -comm, 4- MPI call*/
        m_envelope[0] = (ayu_id_t) aympi_rank;
        m_envelope[1] = task_id;
        m_envelope[2] = elem.partner;
        m_envelope[3] = elem.tag;
        m_envelope[4] = elem.comm;
        m_envelope[5] = MPI_SEND;

        data.common.client_id = myclient_id;
        //TODO calculate correct size
        char str[50];
        sprintf(str, "MPI#%d#%d#%d#%d#%d#%d", m_envelope[0], m_envelope[1], m_envelope[2], m_envelope[3], m_envelope[4], m_envelope[5]);
        //printf ("%d:##########%s \n",aympi_rank,str);

        data.userdata.data = str;

        ayu_event(AYU_USERDATA, data);
        ayu_wipe_data(&data);


    }
#endif



    ierr = PMPI_Send(buf, count, datatype, dest, tag, comm);

    return ierr;
}

int MPI_Recv( void *buf, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Status *status ) {
    int ierr;
    int my_comm;
    MPI_Status mystatus;
    int flag = 0;

    counterMPI++;

    my_comm = comm_id(comm);
    mpiElem elem = { aympi_rank, counterMPI,"MPI_Recv", dest, tag, my_comm, NULL}; /*id,fname,rank,partner,tag,comm,request*/

#ifdef AYU_MPI_DEBUG

    if(tag != MPI_ANY_TAG && dest != MPI_ANY_SOURCE) {
        printf ("MPI task: MyRank = %d | ID = %d | Name = %s | partner = %d | tag = %d | comm = %i \n", elem.rank, elem.id, elem.fname, elem.partner, elem.tag, elem.comm);
        flag = 1;
    }
#endif

#ifdef AYUDAME
    if(ayu_event) {
        /* take a client ID and declaration of variables */
        ayu_client_id_t myclient_id = get_client_id(AYU_CLIENT_MPI);
        ayu_id_t scope_id = 0;
        ayu_id_t task_id = 0;
        ayu_id_t dependency_id;
        ayu_event_data_t data;
        data.common.client_id = myclient_id;



        task_id = counterMPI;

        data.add_task.task_id = task_id;
        data.add_task.scope_id = scope_id;
        data.add_task.task_label= "AYU_ADDTASK_MPI_Recv";
        ayu_event(AYU_ADDTASK, data);
        ayu_wipe_data(&data);

        data.common.client_id = myclient_id;
        dependency_id = 1000 + (task_id-1)*task_id;
        data.add_dependency.dependency_id = dependency_id;
        data.add_dependency.from_id = task_id - 1;
        data.add_dependency.to_id = task_id;
        data.add_dependency.dependency_label= "AYU_ADDDEPENDENCY_MPI_Recv";
        ayu_event(AYU_ADDDEPENDENCY, data);
        ayu_wipe_data(&data);

    }
#endif

    ierr = PMPI_Recv(buf, count, datatype, dest, tag, comm, &mystatus);

    elem.partner = mystatus.MPI_SOURCE;
    elem.tag = mystatus.MPI_TAG;

    status = &mystatus;

#ifdef AYUDAME

    if(ayu_event) {

        ayu_client_id_t myclient_id = get_client_id(AYU_CLIENT_MPI);
        ayu_id_t task_id = 0 ;
        ayu_event_data_t data;


        task_id = counterMPI;

        /**** events for the matcher ****/
        mpi_matcher_t m_envelope[6] = {0}; /* indexes 0 -rank,-taskID, 1 - partner, 2 - tag, 3 -comm , 4 -MPI call*/
        m_envelope[0] = (ayu_id_t) aympi_rank;
        m_envelope[1] = task_id;
        m_envelope[2] = elem.partner;
        m_envelope[3] = elem.tag;
        m_envelope[4] = elem.comm;
        m_envelope[5] = MPI_RECV;

        data.common.client_id = myclient_id;
        //TODO calculate correct size
        char str[50];
        sprintf(str, "MPI#%d#%d#%d#%d#%d#%d", m_envelope[0], m_envelope[1], m_envelope[2], m_envelope[3], m_envelope[4], m_envelope[5]);
        //printf ("%d:##########%s \n",aympi_rank,str);

        data.userdata.data = str;

        ayu_event(AYU_USERDATA, data);
        ayu_wipe_data(&data);
    }
#endif

#ifdef AYU_MPI_DEBUG
    if(flag == 0)
        printf ("MPI task: MyRank = %d | ID = %d | Name = %s | partner = %d | tag = %d | comm = %i \n", elem.rank, elem.id, elem.fname, elem.partner, elem.tag, elem.comm);
#endif

    return ierr;
}

int MPI_Isend(AYU_MPI_COND_CONST void *buf, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request *request) {
    int ierr;
    int my_comm;
    //printf ("MPI_Isend\n");
    counterMPI++;

    my_comm = comm_id(comm);
    mpiElem elem = { aympi_rank, counterMPI,"MPI_Isend", dest, tag, my_comm, request}; /*id,fname,rank,partner,tag,comm,request*/

    
    int i =0;
    while(i<ARRAY_SIZE){
        if(requestList[i].rank==-1 && requestList[i].id==0 && requestList[i].partner==-1){
            requestList[i]=elem;
            break;
        }
        i++;
    }


#ifdef AYU_MPI_DEBUG

    printf ("MPI task: MyRank = %d | ID = %d | Name = %s | partner = %d | tag = %d | comm = %i | request = %p \n", elem.rank, elem.id, elem.fname, elem.partner, elem.tag, elem.comm, elem.request);
    fflush(stdout);
    #endif

#ifdef AYUDAME

    if(ayu_event) {
        /* take a client ID and declaration of variables */
        ayu_client_id_t myclient_id = get_client_id(AYU_CLIENT_MPI);
        ayu_id_t scope_id = 0;
        ayu_id_t task_id = 0;
        ayu_id_t dependency_id;
        ayu_event_data_t data;
        data.common.client_id = myclient_id;



        task_id = counterMPI;

        data.add_task.task_id = task_id;
        data.add_task.scope_id = scope_id;
        data.add_task.task_label= "AYU_ADDTASK_MPI_Isend";
        ayu_event(AYU_ADDTASK, data);
        ayu_wipe_data(&data);

        data.common.client_id = myclient_id;
        dependency_id = 1000 + (task_id-1)*task_id;
        data.add_dependency.dependency_id = dependency_id;
        data.add_dependency.from_id = task_id - 1;
        data.add_dependency.to_id = task_id;
        data.add_dependency.dependency_label= "AYU_ADDDEPENDENCY_MPI_Isend";
        ayu_event(AYU_ADDDEPENDENCY, data);
        ayu_wipe_data(&data);

    }
#endif

    ierr = PMPI_Isend(buf, count, datatype, dest, tag, comm, request);

    return ierr;
}

int MPI_Irecv(void *buf, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm, MPI_Request *request) {
    int ierr;
    int my_comm;

    counterMPI++;
    //printf ("MPI_Irecv\n");
    my_comm = comm_id(comm);
    mpiElem elem = { aympi_rank, counterMPI,"MPI_Irecv", dest, tag, my_comm, request}; /*id,fname,rank,partner,tag,comm,request*/

    
    int i =0;
    while(i<ARRAY_SIZE){
        if(requestList[i].rank==-1 && requestList[i].id==0 && requestList[i].partner==-1){
            requestList[i]=elem;
            break;
        }
        i++;
    }
    

#ifdef AYU_MPI_DEBUG

    printf ("MPI task: MyRank = %d | ID = %d | Name = %s | partner = %d | tag = %d | comm = %i | request = %p \n", elem.rank, elem.id, elem.fname, elem.partner, elem.tag, elem.comm, elem.request);
    fflush(stdout);

#endif

#ifdef AYUDAME

    if(ayu_event) {
        /* take a client ID and declaration of variables */
        ayu_client_id_t myclient_id = get_client_id(AYU_CLIENT_MPI);
        ayu_id_t scope_id = 0;
        ayu_id_t task_id = 0;
        ayu_id_t dependency_id;
        ayu_event_data_t data;
        data.common.client_id = myclient_id;



        task_id = counterMPI;

        data.add_task.task_id = task_id;
        data.add_task.scope_id = scope_id;
        data.add_task.task_label= "AYU_ADDTASK_MPI_Irecv";
        ayu_event(AYU_ADDTASK, data);
        ayu_wipe_data(&data);


        data.common.client_id = myclient_id;
        dependency_id = 1000 + (task_id-1)*task_id;
        data.add_dependency.dependency_id = dependency_id;
        data.add_dependency.from_id = task_id - 1;
        data.add_dependency.to_id = task_id;
        data.add_dependency.dependency_label= "AYU_ADDDEPENDENCY_MPI_Irecv";
        ayu_event(AYU_ADDDEPENDENCY, data);
        ayu_wipe_data(&data);
    }
#endif

    ierr = PMPI_Irecv(buf, count, datatype, dest, tag, comm, request);

    return ierr;
}


int MPI_Wait(MPI_Request *request, MPI_Status *status) {
    int ierr;

    counterMPI++;

    mpiElem elem = { aympi_rank, counterMPI,"MPI_Wait", -1, -1, -1, request}; /*id,fname,rank,partner,tag,comm,request*/

    ierr = PMPI_Wait(request, status);

    elem.partner = status->MPI_SOURCE;
    elem.tag = status->MPI_TAG;

#ifdef AYU_MPI_DEBUG

    printf ("MPI task: MyRank = %d | ID = %d | Name = %s | partner = %d | tag = %d | request = %p \n", elem.rank, elem.id, elem.fname, elem.partner, elem.tag, elem.request);
    fflush(stdout);
    #endif

#ifdef AYUDAME

    if(ayu_event) {


        int i = 0;
        for(i=0;i<=ARRAY_SIZE-1;i++){
            //if(requestList[i].rank!=-1 && requestList[i].id!=0 && requestList[i].partner!=-1){
            if(requestList[i].request == request){
                mpiElem elem_tmp =requestList[i];
                //printf ("%d:##########elem_tmp#######MPI task: MyRank = %d | ID = %d | Name = %s | partner = %d | tag = %d | request = %p \n", aympi_rank, elem_tmp.rank, elem_tmp.id, elem_tmp.fname, elem_tmp.partner, elem_tmp.tag, elem_tmp.request);
                //printf ("%d:##########elem###########MPI task: MyRank = %d | ID = %d | Name = %s | partner = %d | tag = %d | request = %p \n", aympi_rank, elem.rank, elem.id, elem.fname, elem.partner, elem.tag, elem.request);
                


                /* take a client ID and declaration of variables */
                ayu_client_id_t myclient_id = get_client_id(AYU_CLIENT_MPI);
                ayu_id_t scope_id = 0;
                ayu_id_t task_id = 0;
                ayu_id_t dependency_id;
                ayu_event_data_t data;
                data.common.client_id = myclient_id;



                task_id = counterMPI;

                data.add_task.task_id = task_id;
                data.add_task.scope_id = scope_id;
                data.add_task.task_label= "AYU_ADDTASK_MPI_Wait";
                ayu_event(AYU_ADDTASK, data);
                ayu_wipe_data(&data);


                data.common.client_id = myclient_id;
                dependency_id = 1000 + (task_id-1)*task_id;
                data.add_dependency.dependency_id = dependency_id;
                data.add_dependency.from_id = elem_tmp.id;
                data.add_dependency.to_id = task_id;
                data.add_dependency.dependency_label= "AYU_ADDDEPENDENCY_MPI_Wait";
                ayu_event(AYU_ADDDEPENDENCY, data);
                ayu_wipe_data(&data);

                
                data.common.client_id = myclient_id;
                data.set_property.property_owner_id= dependency_id;
                data.set_property.key="dep_type";
                data.set_property.value="real";
                ayu_event(AYU_SETPROPERTY, data);
                ayu_wipe_data(&data);
                


                /**** events for the matcher ****/
                mpi_matcher_t m_envelope[6] = {0}; /* indexes 0 -rank,-taskID, 1 - partner, 2 - tag, 3 -comm , 4 -MPI call*/
                m_envelope[0] = (ayu_id_t) aympi_rank;
                //m_envelope[1] = elem_tmp.id;
                m_envelope[2] = elem_tmp.partner;
                m_envelope[3] = elem_tmp.tag;
                m_envelope[4] = elem_tmp.comm;
                if(strcmp(elem_tmp.fname,"MPI_Irecv")){
                    m_envelope[1] = elem_tmp.id;
                    m_envelope[5] = MPI_RECV;    
                }else if(strcmp(elem_tmp.fname,"MPI_Isend")){
                    m_envelope[1] = elem.id;
                    m_envelope[5] = MPI_SEND;   
                }else{
                     printf ("error");
                }
                

                data.common.client_id = myclient_id;
                //TODO calculate correct size
                char str[50];
                sprintf(str, "MPI#%d#%d#%d#%d#%d#%d", m_envelope[0], m_envelope[1], m_envelope[2], m_envelope[3], m_envelope[4], m_envelope[5]);
                //printf ("%d:##########%s \n",aympi_rank,str);

                data.userdata.data = str;

                ayu_event(AYU_USERDATA, data);
                ayu_wipe_data(&data); 
         }
    }



    }
#endif

    return ierr;
}
