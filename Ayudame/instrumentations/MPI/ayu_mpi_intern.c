#include "ayu_mpi.h"
MPI_Comm listCOMM[1000];

int comm_id( MPI_Comm comm )
{
    int i, result;

//  listCOMM[0] = MPI_COMM_WORLD;

    for ( i = 0; i < counterCOMM + 1; i++){
        if ( i == counterCOMM)
            listCOMM[i] = MPI_COMM_WORLD;
        MPI_Comm_compare( listCOMM[i], comm, &result);
        if ( result == MPI_IDENT )
            return i;
    }

    listCOMM[counterCOMM] = comm;
    counterCOMM++;
    return counterCOMM - 1;
}
