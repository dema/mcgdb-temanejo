#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include "ayu_mpi.h"

int get_rank()
{
    return aympi_rank;
} 

int get_hostnames( int *name_len, int *world_size, char **hostnames)
{
    char *sb;
    int i;

    MPI_Comm_size(MPI_COMM_WORLD, world_size);

    char processor_name[MPI_MAX_PROCESSOR_NAME];
    MPI_Get_processor_name(processor_name, name_len);
    *name_len =  MPI_MAX_PROCESSOR_NAME;

    sb = (char *)malloc((*world_size)*MPI_MAX_PROCESSOR_NAME*sizeof(char));
    *hostnames = (char *)malloc((*world_size)*MPI_MAX_PROCESSOR_NAME*sizeof(char));

    for(i = 0; i < (*world_size); i++)
        strncpy( &sb[i*MPI_MAX_PROCESSOR_NAME], 
                processor_name, 
                MPI_MAX_PROCESSOR_NAME*sizeof(char));
   
    MPI_Alltoall(sb, 
                MPI_MAX_PROCESSOR_NAME, 
                MPI_CHAR, 
                *hostnames, 
                MPI_MAX_PROCESSOR_NAME, 
                MPI_CHAR, 
                MPI_COMM_WORLD);
    
    return 0;   
}
