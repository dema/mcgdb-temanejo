#include<stdio.h>
#include "ayu_mpi.h"
int MPI_Bcast( void *buf, int count, MPI_Datatype datatype, int root, MPI_Comm comm )
{
    int ierr;
    int my_comm;

    counterMPI++;

    my_comm = comm_id(comm);
    mpiElem elem = { aympi_rank, counterMPI,"MPI_Bcast", root, 0, my_comm, NULL}; /*id,fname,rank,partner,tag,comm,request*/

#ifdef AYU_MPI_DEBUG
    printf ("MPI task: MyRank = %d | ID = %d | Name = %s | root = %d |  comm = %i \n", elem.rank, elem.id, elem.fname, elem.partner,  elem.comm);
#endif

    ierr = PMPI_Bcast(buf, count, datatype, root, comm);

    return ierr;
}


int MPI_Reduce ( AYU_MPI_COND_CONST void *sendbuf, void *recvbuf, int count, MPI_Datatype datatype, MPI_Op op, int root, MPI_Comm comm )
{
    int ierr;
    int my_comm;

    counterMPI++;

    my_comm = comm_id(comm);
    mpiElem elem = { aympi_rank, counterMPI,"MPI_Reduce", root, 0, my_comm, NULL}; /*id,fname,rank,partner,tag,comm,request*/

#ifdef AYU_MPI_DEBUG
    printf ("MPI task: MyRank = %d | ID = %d | Name = %s | root = %d | comm = %i \n", elem.rank, elem.id, elem.fname, elem.partner, elem.comm);
#endif

    ierr = PMPI_Reduce ( sendbuf, recvbuf, count, datatype, op,root,  comm );

    return ierr;
}
