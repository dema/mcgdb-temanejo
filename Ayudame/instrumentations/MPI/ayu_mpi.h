#include <mpi.h>
#ifdef AYUDAME
#include <ayudame.h>
#endif
#include "ayu_mpi_extern.h"

#include <sys/types.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/socket.h>

//#define AYU_MPI_DEBUG
#define MPI_SEND 0
#define MPI_RECV 1

#define ARRAY_SIZE 100
/*
 * MPI standards from version 3 on declare function parameters as const
 * wherever possible. See https://svn.mpi-forum.org/trac/mpi-forum-web/ticket/140
 * Add the constant AYU_MPI_COND_CONST wherever necessary!
 */
#if MPI_VERSION >= 3
#define AYU_MPI_COND_CONST const
#else
#define AYU_MPI_COND_CONST
#endif

typedef uint64_t mpi_matcher_t;

typedef struct {
    int rank;
    unsigned int id;
    char *fname;
    int partner;
    int tag;
    int comm;
    MPI_Request *request;
} mpiElem;

extern unsigned int counterMPI;
extern unsigned int counterCOMM;
extern int aympi_rank;
//extern int get_rank();
//extern int get_hostnames(int *, int *, char **);





int ranks;

// TODO use malloc instead
// requestList =(MPI_Request*)malloc((size-1)*sizeof(MPI_Request));
mpiElem requestList[ARRAY_SIZE];



int comm_id( MPI_Comm );
