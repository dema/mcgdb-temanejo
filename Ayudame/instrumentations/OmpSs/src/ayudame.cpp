#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#define NUMA
#ifdef NUMA
#include <numa.h>
#include <sched.h>
#endif //NUMA
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <pthread.h>
#include "plugin.hpp"
#include "system.hpp"
#include "instrumentation.hpp"
#include "instrumentationcontext_decl.hpp"
#include "ayudame.h"
#include <cassert>
#include "smpdd.hpp" // FIXME: this the include should not be here (just testing smpdd)

#include "nanos.h"

//needed for get path in initialize
#include <unistd.h>
#include "tca_runtime.h"
#include "simple_stepper.hpp"

#include <unistd.h>
#ifndef VERBOSE
#define VERBOSE 1
#endif
#define MAX_QUEUE 100

#define MAIN_SCOPE 0

static void set_sender_id(uint64_t *set_id, uint64_t sender_id)
{
    *set_id |= sender_id << 0;
}
static void set_receiver_id(uint64_t *set_id, uint64_t receiver_id)
{
    *set_id |= receiver_id << 24;
}
static void set_dep_adrress_id(uint64_t *set_id, uint64_t value)
{
    *set_id |= value << 48;
}

#ifdef __cplusplus
extern "C"
{
#endif
//TODO seperate to header file
void tca_initialize(tca_function_lookup_t lookup,
                    const char *runtime_version,
                    unsigned int tca_version) __attribute__ ((weak));
#ifdef __cplusplus
}
#endif

//#define NDEBUG

int get_wd()
{
    //TODO return wd;
    //fprintf(stderr, "huhuhuhuhuhuh %d", nanos_current_wd());
    //fprintf(stderr, "huhuhuhuhuhuh %d", nanos_get_wd_id(nanos_current_wd()));
    int tmp = (int) nanos_get_wd_id(nanos_current_wd());
    return tmp;
}

namespace nanos
{

class InstrumentationAyudame: public Instrumentation
{

#ifndef NANOS_INSTRUMENTATION_ENABLED
public:
// constructor
    InstrumentationAyudame()
        : Instrumentation()
    {
    }

// destructor
    ~InstrumentationAyudame()
    {
    }

// low-level instrumentation interface (mandatory functions)
    void initialize(void)
    {
    }
    void finalize(void)
    {
    }
    void disable(void)
    {
    }
    void enable(void)
    {
    }
    void addEventList(unsigned int count, Event *events)
    {
    }
    void addResumeTask(WorkDescriptor &w)
    {
    }
    void addSuspendTask(WorkDescriptor &w)
    {
    }
    void threadStart(BaseThread &thread)
    {
    }
    void threadFinish(BaseThread &thread)
    {
    }
    void incrementMaxThreads(void)
    {
    }
#else

private:

    //TODO this is just a rough hack
    //this is necessary because events could occur after finalize was called.
    //This happens mostly for suspending tasks.
    bool active;
    pthread_mutex_t active_mutex = PTHREAD_MUTEX_INITIALIZER;

    std::list < uint64_t > threadid_vec;
    std::multimap < int, std::pair< int, nanos_event_value_t > > virtuel_task_multimap;

    pthread_mutex_t threadid_vec_mutex = PTHREAD_MUTEX_INITIALIZER;
    pthread_mutex_t virtuel_task_multimap_mutex = PTHREAD_MUTEX_INITIALIZER;

    /**
     *
     * @param wd_id_num
     * @param wd
     * @param num_deps
     * @param da
     */
    void addTask(int64_t wd_id_num, WorkDescriptor *wd) {

        ayu_client_id_t myclient_id = get_client_id(AYU_CLIENT_OMPSS);
        int64_t funct_id = (int64_t) ((ext::SMPDD &) (wd->getActiveDevice())).getWorkFct(); // func_id
        char *wd_description = (char*) wd->getDescription();//wd_description function name
        WorkDescriptor *parent = wd->getParent();
        int64_t priority = wd->getPriority();// Priority
        ayu_id_t scope_id = 0;
        ayu_event_data_t data;

        if (parent->getParent() == NULL) { // not nested, my parent is a thread
            scope_id = 0;// not nested
        } else { // nested
            scope_id = parent->getId();// id of parent task
        }
        data.common.client_id = myclient_id;
        data.add_task.task_id = wd_id_num;
        data.add_task.scope_id = scope_id;
        data.add_task.task_label = "task";
        ayu_event(AYU_ADDTASK, data);
        ayu_wipe_data(&data);

        char funct_id_buffer[1024];
        sprintf(funct_id_buffer,"%llu", funct_id);
        data.common.client_id = myclient_id;
        data.set_property.property_owner_id= wd_id_num;
        data.set_property.key="func_id";
        data.set_property.value=funct_id_buffer;
        ayu_event(AYU_SETPROPERTY, data);
        ayu_wipe_data(&data);

        char wd_description_buffer[1024];
        sprintf(wd_description_buffer,"%s", wd_description);
        data.common.client_id = myclient_id;
        data.set_property.property_owner_id= wd_id_num;
        data.set_property.key="function_name";
        data.set_property.value=wd_description_buffer;
        ayu_event(AYU_SETPROPERTY, data);
        ayu_wipe_data(&data);

        char priority_buffer[1024];
        sprintf(priority_buffer,"%llu", priority);
        data.common.client_id = myclient_id;
        data.set_property.property_owner_id= wd_id_num;
        data.set_property.key="priority";
        data.set_property.value=priority_buffer;
        ayu_event(AYU_SETPROPERTY, data);
        ayu_wipe_data(&data);

        /*
         // when the task had no dependence. At submit we are creating the events "create-wd-ptr" and "wd-num-deps".
         // These two events can be used to determines if  (nanos::WD *) (<"create-wd-ptr">)->your_identifier() has dependences or not "<wd-num-deps>" == 0 (ready) and <"wd-num-deps"> != 0 (blocked).
         for(int i = 0; i< num_deps;i++){
         if(da[i].isInput()){
         fprintf (stderr,"dd");
         return;
         }
         }
         int thread_id = 0;
         fprintf (stderr,"Emitting AYU_ADDTASKTOQUEUE with data={%ld,%ld,%ld}, id=%ld\n",
         AYU_data[0], AYU_data[1], AYU_data[2], wd_id_num);
         AYU_event (AYU_ADDTASKTOQUEUE, (int64_t) wd->getId(), (void*)thread_id);
         */
    }

    /**
     *
     * @param sender_id
     * @param receiver_id
     * @param dep_direction_value
     * @param dep_address_value
     */
    void addDependency(int sender_id, int receiver_id, nanos_event_value_t dep_direction_value, nanos_event_value_t dep_address_value) {

#if VERBOSE > 0
        fprintf (stderr,"++++DEP %d -> %d  \n",sender_id,receiver_id);
#endif
        ayu_client_id_t myclient_id = get_client_id(AYU_CLIENT_OMPSS);
        int64_t address_id = (int64_t) dep_address_value;
        ayu_event_data_t data;

        switch (dep_direction_value) {

            case 1:     // True dependence
#if VERBOSE > 0

            fprintf (stderr,"++DEP RaW dependence %d -> %d  , %d \n",sender_id,receiver_id, threadid_vec.size());
            for (std::list<uint64_t>::iterator iter = threadid_vec.begin();iter != threadid_vec.end(); ++iter) {
                fprintf (stderr," %d\n",*iter);
            }
            fprintf (stderr,"++DEP RaW dependence %d -> %d  , %d \n",sender_id,receiver_id, threadid_vec.size());

#endif

            pthread_mutex_lock(&threadid_vec_mutex);
            if ( /*!(std::find(threadid_vec.begin(), threadid_vec.end(), (uint64_t)receiver_id)!=threadid_vec.end()) &&*/
                !(std::find(threadid_vec.begin(), threadid_vec.end(), (uint64_t)sender_id) !=threadid_vec.end()) ) {

                uint64_t dep_id=0;
                set_sender_id(&dep_id, (uint64_t)sender_id);
                set_receiver_id(&dep_id,(uint64_t)receiver_id);
                set_dep_adrress_id(&dep_id,dep_address_value);

                //fprintf (stderr,"HUHUHU %lu -> %lu   , %lu \n",sender_id,receiver_id, dep_id);

                data.common.client_id = myclient_id;
                data.add_dependency.dependency_id= dep_id;
                data.add_dependency.from_id=sender_id;
                data.add_dependency.to_id=receiver_id;
                data.add_dependency.dependency_label="dep";
                ayu_event(AYU_ADDDEPENDENCY, data);
                ayu_wipe_data(&data);

                char dep_address_value_buffer[1024];
                sprintf(dep_address_value_buffer,"%llu", dep_address_value);
                data.common.client_id = myclient_id;
                data.set_property.property_owner_id= dep_id;
                data.set_property.key="dep_address_value";
                data.set_property.value=dep_address_value_buffer;
                ayu_event(AYU_SETPROPERTY, data);
                ayu_wipe_data(&data);

                char dep_direction_value_buffer[1024];
                sprintf(dep_direction_value_buffer,"%llu", dep_direction_value);
                data.common.client_id = myclient_id;
                data.set_property.property_owner_id= dep_id;
                data.set_property.key="dep_direction_value";
                data.set_property.value=dep_direction_value_buffer;
                ayu_event(AYU_SETPROPERTY, data);
                ayu_wipe_data(&data);
            } else {
                //---------------------------------
                //TASK WAIT ON
                //---------------------------------
#if VERBOSE > 0
                fprintf (stderr,"Emitting Event ADDDEPENDENCY to a thread \n" );
#endif

                uint64_t dep_id=0;
                set_sender_id(&dep_id, (uint64_t)sender_id);
                set_receiver_id(&dep_id,(uint64_t)receiver_id);
                set_dep_adrress_id(&dep_id,dep_address_value);

                // add a task which is a thread to generate an dependency for taskwait on
                data.common.client_id = myclient_id;
                //data.add_task.task_id = receiver_id;//this is a thread
                data.add_task.task_id = get_wd();
                data.add_task.scope_id = 0;//thread has scope 0
                data.add_task.task_label = "task wait on";
                ayu_event(AYU_ADDTASK, data);
                ayu_wipe_data(&data);

                data.common.client_id = myclient_id;
                data.set_property.property_owner_id= receiver_id;
                data.set_property.key="sync_type";
                data.set_property.value="Task wait on";
                ayu_event(AYU_SETPROPERTY, data);
                ayu_wipe_data(&data);

                data.common.client_id = myclient_id;
                data.add_dependency.dependency_id= dep_id;
                data.add_dependency.from_id=sender_id;
                data.add_dependency.to_id=receiver_id;
                data.add_dependency.dependency_label="dep";
                ayu_event(AYU_ADDDEPENDENCY, data);
                ayu_wipe_data(&data);

                char dep_address_value_buffer[1024];
                sprintf(dep_address_value_buffer,"%llu", dep_address_value);
                data.common.client_id = myclient_id;
                data.set_property.property_owner_id= dep_id;
                data.set_property.key="dep_address_value";
                data.set_property.value=dep_address_value_buffer;
                ayu_event(AYU_SETPROPERTY, data);
                ayu_wipe_data(&data);

                char dep_direction_value_buffer[1024];
                sprintf(dep_direction_value_buffer,"%llu", dep_direction_value);
                data.common.client_id = myclient_id;
                data.set_property.property_owner_id= dep_id;
                data.set_property.key="dep_direction_value";
                data.set_property.value=dep_direction_value_buffer;
                ayu_event(AYU_SETPROPERTY, data);
                ayu_wipe_data(&data);
                /*
                 // add a task which is a thread to generate an dependency for taskwait on
                 AYU_data[0] = -1;
                 AYU_data[1] = 0;     // FIXME: Is Critical/Priority?
                 AYU_data[1] = receiver_id;
                 AYU_event (AYU_WAITON,  receiver_id, AYU_data);

                 // add dependency to a thread with an the memory address of the variable

                 AYU_data[0] = sender_id;
                 AYU_data[1] = address_id;
                 AYU_data[2] = address_id;
                 AYU_event(AYU_ADDDEPENDENCY, receiver_id, AYU_data );
                 */

            }
            pthread_mutex_unlock(&threadid_vec_mutex);

            break;

            case 2:    // Anti-dependence

#if VERBOSE > 0

            fprintf (stderr,"++DEP WaR dependence %d -> %d \n",sender_id,receiver_id);
#endif

            break;

            case 3:     // Output dependence

#if VERBOSE > 0

            fprintf (stderr,"++DEP WaW dependence %d -> %d \n",sender_id,receiver_id);
#endif

            break;

            case 4:     // Output dependence   wd -> concurrent

#if VERBOSE > 0

            fprintf (stderr,"++DEP CloseConcurrent dependence (wd -> concurrent) %d -> %d  data={%ld}\n",sender_id,receiver_id, (int64_t)dep_address_value);
#endif

            pthread_mutex_lock(&virtuel_task_multimap_mutex);
            virtuel_task_multimap.insert(std::make_pair(receiver_id,std::make_pair(sender_id,dep_address_value)));
            pthread_mutex_unlock(&virtuel_task_multimap_mutex);

            break;

            case 5:     // Output dependence concurrent -> wd

#if VERBOSE > 0

            fprintf (stderr,"++DEP OpenConcurrent dependence (concurrent -> wd) %d -> %d \n",sender_id,receiver_id);
#endif

            break;

            case 6:     // wd -> commutative
#if VERBOSE > 0

            fprintf (stderr,"++DEP CloseCommutative  dependence (wd -> commutative) %d -> %d \n",sender_id,receiver_id);
#endif

            pthread_mutex_lock(&virtuel_task_multimap_mutex);
            virtuel_task_multimap.insert(std::make_pair(receiver_id,std::make_pair(sender_id,dep_address_value)));
            pthread_mutex_unlock(&virtuel_task_multimap_mutex);

            break;

            case 7:     // commutative -> wd
#if VERBOSE > 0

            fprintf (stderr,"++DEP OpenCommutative dependence (commutative -> wd) %d -> %d \n",sender_id,receiver_id);
#endif

            break;

            case 8:     // wd -> common
#if VERBOSE > 0

            fprintf (stderr,"++DEP CloseGeneral  dependence (wd -> common)  %d -> %d \n",sender_id,receiver_id);
#endif

            break;

            case 9:     // common -> wd

#if VERBOSE > 0

            fprintf (stderr,"++DEP OpenGeneral dependence   (common -> wd) %d -> %d \n",sender_id,receiver_id);
#endif

            pthread_mutex_lock(&virtuel_task_multimap_mutex);
            for(std::multimap<int,std::pair<int,nanos_event_value_t> > ::const_iterator i(virtuel_task_multimap.begin()),end(virtuel_task_multimap.end()); i!=end; ++i) {
                if(i->first==sender_id) {
                    int local_sender_id = i->second.first;
                    int64_t local_dep_address_value = i->second.second;

                    uint64_t dep_id=0;
                    set_sender_id(&dep_id, (uint64_t)sender_id);
                    set_receiver_id(&dep_id,(uint64_t)receiver_id);
                    set_dep_adrress_id(&dep_id,local_dep_address_value);

                    data.common.client_id = myclient_id;
                    data.add_dependency.dependency_id= dep_id;
                    data.add_dependency.from_id=local_sender_id;
                    data.add_dependency.to_id=receiver_id;
                    data.add_dependency.dependency_label="dep";
                    ayu_event(AYU_ADDDEPENDENCY, data);
                    ayu_wipe_data(&data);

                    char local_dep_address_value_buffer[1024];
                    sprintf(local_dep_address_value_buffer,"%llu", local_dep_address_value);
                    data.common.client_id = myclient_id;
                    data.set_property.property_owner_id= dep_id;
                    data.set_property.key="dep_address_value";
                    data.set_property.value=local_dep_address_value_buffer;
                    ayu_event(AYU_SETPROPERTY, data);
                    ayu_wipe_data(&data);

                    char dep_direction_value_buffer[1024];
                    sprintf(dep_direction_value_buffer,"%llu", dep_direction_value);
                    data.common.client_id = myclient_id;
                    data.set_property.property_owner_id= dep_id;
                    data.set_property.key="dep_direction_value";
                    data.set_property.value=dep_direction_value_buffer;
                    ayu_event(AYU_SETPROPERTY, data);
                    ayu_wipe_data(&data);

                }
            }
            virtuel_task_multimap.erase(sender_id);
            pthread_mutex_unlock(&virtuel_task_multimap_mutex);
            break;

            default:
            fprintf (stderr,"++DEP Unexpected type dependency");
            break;
        }
    }

    /*
     *  addWaitOn
     *
     */
    void addTaskWait(long key, long value) {
        fprintf(stderr,"Wait_on key=%ld, value =%ld \n",key, value);

        /*
         * int_64_t current_wd = myThread->getCurrentWD()->getId());
         * taskwait(current_wd);
         */
        //AYU_event(AYU_BARRIER, 0, 0);
        // starting taskwait
    }

    /**
     *  task ready
     * @param wd
     */
    void taskStateReady(WorkDescriptor *wd ) {
        ayu_client_id_t myclient_id = get_client_id(AYU_CLIENT_OMPSS);
        ayu_event_data_t data;

        if ( !(std::find(threadid_vec.begin(), threadid_vec.end(), (uint64_t)wd->getId())!=threadid_vec.end()) ) {
            int64_t thread_id = 0;

            char thread_id_buffer[1024];
            sprintf(thread_id_buffer,"%llu", thread_id);
            data.common.client_id = myclient_id;
            data.set_property.property_owner_id= wd->getId();
            data.set_property.key="thread";
            data.set_property.value=thread_id_buffer;
            ayu_event(AYU_SETPROPERTY, data);
            ayu_wipe_data(&data);

            data.common.client_id = myclient_id;
            data.set_property.property_owner_id= wd->getId();
            data.set_property.key="state";
            data.set_property.value="ready";
            ayu_event(AYU_SETPROPERTY, data);
            ayu_wipe_data(&data);

        }

    }

    /**
     *  task blocked
     * @param wd
     */
    void taskStateBlocked(WorkDescriptor *wd ) {
        ayu_client_id_t myclient_id = get_client_id(AYU_CLIENT_OMPSS);
        ayu_event_data_t data;

        if ( !(std::find(threadid_vec.begin(), threadid_vec.end(), (uint64_t)wd->getId())!=threadid_vec.end()) ) {
            int64_t thread_id = 0;
            char thread_id_buffer[1024];
            sprintf(thread_id_buffer,"%llu", thread_id);
            data.common.client_id = myclient_id;
            data.set_property.property_owner_id= wd->getId();
            data.set_property.key="thread";
            data.set_property.value=thread_id_buffer;
            ayu_event(AYU_SETPROPERTY, data);
            ayu_wipe_data(&data);

            data.common.client_id = myclient_id;
            data.set_property.property_owner_id= wd->getId();
            data.set_property.key="state";
            data.set_property.value="blocked";
            ayu_event(AYU_SETPROPERTY, data);
            ayu_wipe_data(&data);
        }
    }

    /**
     *
     */
    void enteringWD(long value) {

    }

    /**
     *
     */
    void enteringUserCode(long value) {

#if VERBOSE > 0
        fprintf(stderr,"Emitting Event RUNTASK entering usercode %lu\n",value); // events[i].getValue() taskid
#endif
        //TODO get thread ID taskID
        //stepper::stepper_request_progress(w.getId(),thread_id);

        ayu_client_id_t myclient_id = get_client_id(AYU_CLIENT_OMPSS);
        ayu_event_data_t data;
        data.common.client_id = myclient_id;
        data.set_property.property_owner_id= value;// events[i].getValue() taskid;
        data.set_property.key="state";
        data.set_property.value="running";
        ayu_event(AYU_SETPROPERTY, data);
        ayu_wipe_data(&data);
    }

    /**
     *
     */
    void enteringFunctionLocation(long key, long value, std::string description) {
        // TODO: get function location when task is created!
        // and communicate to ayudame
        ayu_client_id_t myclient_id = get_client_id(AYU_CLIENT_OMPSS);
        ayu_event_data_t data;
        data.common.client_id = myclient_id;
        data.set_property.property_owner_id=  get_wd();//value;// events[i].getValue() taskid;
        data.set_property.key="function_location";
        data.set_property.value=description.c_str();
        ayu_event(AYU_SETPROPERTY, data);
        ayu_wipe_data(&data);



        char address_buffer[1024];
        sprintf(address_buffer,"%llu", value);
        data.common.client_id = myclient_id;
        data.set_property.property_owner_id=  get_wd();//value;// events[i].getValue() taskid;
        data.set_property.key="function_address";
        data.set_property.value=address_buffer;
        ayu_event(AYU_SETPROPERTY, data);
        ayu_wipe_data(&data);

#if VERBOSE > 0
        printf("funct_location\n");
        fprintf(stderr,"NANOS++: Executing %s function location (key = %ld, value =%ld)\n",
            description.c_str(),key,value);
#endif
    }

    /**
     *
     */
    void leavingWD(long value) {

    }

    /**
     *
     */
    void leavingUserCode(long value) {
#if VERBOSE > 0
        fprintf(stderr,"Emitting Event REMOVETASK leaving usercode %lu\n",value); // events[i].getValue() taskid
#endif
        ayu_client_id_t myclient_id = get_client_id(AYU_CLIENT_OMPSS);
        ayu_event_data_t data;
        data.common.client_id = myclient_id;
        data.set_property.property_owner_id= value; // events[i].getValue() taskid
        data.set_property.key="state";
        data.set_property.value="finished";
        ayu_event(AYU_SETPROPERTY, data);
        ayu_wipe_data(&data);

    }

    /**
     *
     */
    void leavingFunctionLocation(long key,long value, std::string description) {

#if VERBOSE > 0
        fprintf(stderr,"NANOS++: (TASK) Function: %s\n", description.c_str() );
#endif
        std::size_t found = description.find("main(");
        if (found!=std::string::npos) {

            pthread_mutex_lock(&active_mutex);
            fprintf(stderr,"####################Event FINISH emitted\n");
            ayu_client_id_t myclient_id = get_client_id(AYU_CLIENT_OMPSS);
            ayu_event_data_t data;
            data.common.client_id = myclient_id;
            ayu_event(AYU_FINISH, data);

            active=false;
            pthread_mutex_unlock(&active_mutex);

        }
    }

public:
// constructor
    InstrumentationAyudame() : Instrumentation( *NEW InstrumentationContextDisabled() ) {
        printf ("InstrumentationAyudame() Begin\n");
        pthread_mutex_lock(&active_mutex);
        active=true;
        pthread_mutex_unlock(&active_mutex);

        //TODO check for real nanox runtime versio string

        printf("tca_initialize: %p\n",tca_initialize);
        if(tca_initialize) {
            tca_initialize(lookup,"Nanos++", TCA_VERSION);
        } else {
            printf("No tca_initialize availible");
        }

        printf ("InstrumentationAyudame() End\n");
    }
// destructor
    ~InstrumentationAyudame() {
        pthread_mutex_lock(&active_mutex);
        active=false;
        pthread_mutex_unlock(&active_mutex);

        //fprintf(stderr,"####################Event FINISH emitted2222222222\n");
        //ayu_client_id_t myclient_id = get_client_id(AYU_CLIENT_OMPSS);
        //ayu_event_data_t data;
        //data.common.client_id = myclient_id;
        //ayu_event(AYU_FINISH, data);

        //fprintf (stderr," ~InstrumentationAyudame()\n");
    }

// low-level instrumentation interface (mandatory functions)

    /**
     *
     */
    void initialize( void ) {
        ayu_client_id_t myclient_id = get_client_id(AYU_CLIENT_OMPSS);
        ayu_event_data_t data;

        data.common.client_id = myclient_id;
        ayu_register_get_callback(get_wd,TCA_REGISTER_GET_WD_ID,data);

        ayu_wipe_data(&data);

        data.common.client_id = myclient_id;
        data.add_task.task_id = MAIN_SCOPE;
        data.add_task.scope_id = MAIN_SCOPE;
        data.add_task.task_label = "Main";
        ayu_event(AYU_ADDTASK, data);
        ayu_wipe_data(&data);

        int pid= getpid();
        char pid_buffer[1024];
        sprintf(pid_buffer,"%llu",pid);
        data.common.client_id = myclient_id;
        data.set_property.property_owner_id= MAIN_SCOPE;
        data.set_property.key="pid";
        data.set_property.value=pid_buffer;
        ayu_event(AYU_SETPROPERTY, data);
        ayu_wipe_data(&data);

        char hostname[128];
        gethostname(hostname, sizeof hostname);
        struct hostent* h;
        h = gethostbyname(hostname);

        //sprintf(buffer,"%s",  h->h_name);
        printf("Host: %s\n", h->h_name);

        //struct in_addr **addr_list;
        //addr_list = (struct in_addr **)h->h_addr_list;
        //for(int i = 0; addr_list[i] != NULL; i++) {
        //    printf("%s ", inet_ntoa(*addr_list[i]));
        //}
        //printf("\n");
        char h_name_buffer[1024];
        sprintf(h_name_buffer,"%s", h->h_name);
        data.common.client_id = myclient_id;
        data.set_property.property_owner_id= MAIN_SCOPE;
        data.set_property.key="hostname";
        data.set_property.value=h_name_buffer;
        ayu_event(AYU_SETPROPERTY, data);
        ayu_wipe_data(&data);
        char buff[1024];
        memset(buff,0,sizeof(buff));
        //char application_buffer[1024];
        ssize_t len = readlink("/proc/self/exe", buff, sizeof(buff)-1);
        //printf("APPLICATION: %s\n", buff);
        //sprintf(application_buffer,"%s", buff);
        //printf("APPLICATION: %s   %s\n", buff,application_buffer);
        //std::cout << buff << std::endl;
        data.common.client_id = myclient_id;
        data.set_property.property_owner_id= MAIN_SCOPE;
        data.set_property.key="path+application";
        data.set_property.value=buff;
        ayu_event(AYU_SETPROPERTY, data);
        ayu_wipe_data(&data);

        /*
         ayu_runtime_t ayu_rt = AYU_RT_OMPSS;
         AYU_event(AYU_PREINIT, 0, (void *) &ayu_rt);
         */
#if VERBOSE > 0

        fprintf(stderr,"Event PREINIT emitted\n");
#endif

        /* buggy code should be fixed in build system
         * check ayu1 code
         const unsigned long n_threads = sys.getSMPPlugin()->getNumWorkers();
         */
#if VERBOSE > 0

        fprintf (stderr, "Event INIT about to be emitted, n_threads \n");
#endif

        /*
         AYU_event (AYU_INIT, 0, &n_threads);
         */
#if VERBOSE > 0

        fprintf (stderr, "Event INIT emitted, n_threads \n");
#endif

    }

    /*
     *
     */
    void finalize( void ) {

        //ayu_client_id_t myclient_id = get_client_id(AYU_CLIENT_OMPSS);
        //ayu_event_data_t data;
        //data.common.client_id = myclient_id;

        //ayu_event(AYU_FINISH, data);

    }

    /*
     *
     */
    void disable( void ) {}

    /*
     *
     */
    void enable( void ) {}

    /*
     *
     */
    void addResumeTask( WorkDescriptor &w ) {
        pthread_mutex_lock(&active_mutex);
        if(active==false) {
            pthread_mutex_unlock(&active_mutex);
            return;
        }
        pthread_mutex_unlock(&active_mutex);

        ayu_client_id_t myclient_id = get_client_id(AYU_CLIENT_OMPSS);
        ayu_event_data_t data;

        WorkDescriptor *parent = w.getParent();
        if (parent == NULL) { // task is a thread
#if VERBOSE > 0
            fprintf(stderr,"+++ No parent +++ %d \n", w.getId());
#endif
            // TODO: treat thread creation!

            pthread_mutex_lock(&threadid_vec_mutex);
            threadid_vec.push_back((int64_t)w.getId());
            pthread_mutex_unlock(&threadid_vec_mutex);

        }
        if (parent != NULL) { // task is a real task
#if VERBOSE > 0
            fprintf(stderr,"+++ parent: %d +++\n", w.getId());
            fprintf(stderr,"+++ parent: %d +++\n", parent->getId());
#endif

            int thread_id = pthread_self();

#if VERBOSE > 0
            fprintf(stderr,"Emitting Event PRERUNTASK %d\n", w.getId());
#endif

            char thread_id_buffer[1024];
            sprintf(thread_id_buffer,"%llu", thread_id);
            data.common.client_id = myclient_id;
            data.set_property.property_owner_id= w.getId();
            data.set_property.key="thread";
            data.set_property.value=thread_id_buffer;
            ayu_event(AYU_SETPROPERTY, data);
            ayu_wipe_data(&data);

            stepper::stepper_request_progress(w.getId(),thread_id);

            data.common.client_id = myclient_id;
            data.set_property.property_owner_id= w.getId();
            data.set_property.key="state";
            data.set_property.value="running";
            ayu_event(AYU_SETPROPERTY, data);
            ayu_wipe_data(&data);

#ifdef NUMA

            char sched_getcpu_buffer[1024];
            sprintf(sched_getcpu_buffer,"%llu", sched_getcpu());
            data.common.client_id = myclient_id;
            data.set_property.property_owner_id= w.getId();
            data.set_property.key="sched_getcpu";
            data.set_property.value=sched_getcpu_buffer;
            ayu_event(AYU_SETPROPERTY, data);
            ayu_wipe_data(&data);

            char numa_node_of_cpu_buffer[1024];
            sprintf(numa_node_of_cpu_buffer,"%llu", numa_node_of_cpu(sched_getcpu()));
            data.common.client_id = myclient_id;
            data.set_property.property_owner_id= w.getId();
            data.set_property.key="numa_node_of_cpu";
            data.set_property.value=numa_node_of_cpu_buffer;
            ayu_event(AYU_SETPROPERTY, data);
            ayu_wipe_data(&data);

#endif //NUMA

        }
    }

    /*
     *
     */
    void addSuspendTask( WorkDescriptor &w, bool last ) {
        pthread_mutex_lock(&active_mutex);
        if(active==false) {
            pthread_mutex_unlock(&active_mutex);
            return;
        }
        pthread_mutex_unlock(&active_mutex);
        ayu_client_id_t myclient_id = get_client_id(AYU_CLIENT_OMPSS);
        ayu_event_data_t data;
        //char buffer[sizeof(nanos_event_value_t)];

        if (last) {
#if VERBOSE > 0
            fprintf(stderr,"Emitting Event REMOVETASK %d\n",(int) w.getId());
#endif

            data.common.client_id = myclient_id;
            data.set_property.property_owner_id= w.getId();
            data.set_property.key="state";
            data.set_property.value="finished";
            ayu_event(AYU_SETPROPERTY, data);
            ayu_wipe_data(&data);
        }
    }

    /**
     *
     * @param count
     * @param events
     */
    void addEventList ( unsigned int count, Event *events ) {
        pthread_mutex_lock(&active_mutex);
        if(active==false) {
            pthread_mutex_unlock(&active_mutex);
            return;
        }
        pthread_mutex_unlock(&active_mutex);

        InstrumentationDictionary *iD = sys.getInstrumentation()->getInstrumentationDictionary();
        static const nanos_event_key_t create_wd_id = iD->getEventKey("create-wd-id"); //13
        static const nanos_event_key_t create_wd_ptr = iD->getEventKey("create-wd-ptr");//14
        static const nanos_event_key_t wd_num_deps = iD->getEventKey("wd-num-deps");// 15
        static const nanos_event_key_t wd_deps_ptr = iD->getEventKey("wd-deps-ptr");// 16
        static const nanos_event_key_t dependence = iD->getEventKey("dependence");//36
        static const nanos_event_key_t funct_location = iD->getEventKey("user-funct-location");//21
        static const nanos_event_key_t user_code = iD->getEventKey("user-code");
        static const nanos_event_key_t dep_direction = iD->getEventKey("dep-direction");//37
        static const nanos_event_key_t dep_address = iD->getEventKey("dep-address");//43
        //static const nanos_event_key_t graph_size = iD->getEventKey("graph-size");//23
        static const nanos_event_key_t wd_id_event =  iD->getEventKey("wd-id");
        static const nanos_event_key_t wd_ready = iD->getEventKey("wd-ready");
        static const nanos_event_key_t wd_blocked = iD->getEventKey("wd-blocked");
        static const nanos_event_key_t api = getInstrumentationDictionary()->getEventKey("api");
        static const nanos_event_value_t wait_group = getInstrumentationDictionary()->getEventValue("api","wg_wait_completion");

        // 20 22 31 32 18 33 34 not interresting

#if VERBOSE > 2

        fprintf(stderr,"addEventList\n");
        for (unsigned int ii=0; ii<count;ii++) {
            fprintf (stderr,
                "key=%d value=%d \n",events[ii].getKey(),events[ii].getValue());
        }
#endif

        for (unsigned int i = 0; i < count; i++) {

            switch ( events[i].getType() ) {
                case NANOS_STATE_START:
                break;
                case NANOS_STATE_END:
                break;
                case NANOS_SUBSTATE_START:
                case NANOS_SUBSTATE_END:
                case NANOS_PTP_START:
                case NANOS_PTP_END:
                break;
                case NANOS_POINT:
                if ( events[i].getKey() == create_wd_ptr ) { //It's a create task event
                    //----------------read all data to event -----------------
                    WorkDescriptor *wd = (WorkDescriptor *) events[i].getValue();
                    int64_t wd_id_num = wd->getId();
                    //----------------read all data to event -----------------
                    addTask(wd_id_num, wd);
                }
                if ( events[i].getKey() == dependence ) { //It's a dependence event
                    //----------------read all data to event -----------------
                    nanos_event_value_t dependence_value = events[i].getValue();
                    int sender_id = (int) ( dependence_value >> 32 );
                    int receiver_id = (int) ( dependence_value & 0xFFFFFFFFFF );
                    i++;
                    assert(i < count);
                    assert(events[i].getKey() == dep_direction);
                    nanos_event_value_t dep_direction_value = events[i].getValue();
                    i++;
                    assert(i < count);
                    assert(events[i].getKey() == dep_address);
                    nanos_event_value_t dep_address_value = events[i].getValue();
                    //----------------read all data to event -----------------
                    addDependency(sender_id, receiver_id, dep_direction_value, dep_address_value);
                }
                if(events[i].getKey() == wd_ready) {
                    WorkDescriptor *wd = (WorkDescriptor *) events[i].getValue();
                    taskStateReady(wd);
                }
                if(events[i].getKey() == wd_blocked) {
                    WorkDescriptor *wd = (WorkDescriptor *) events[i].getValue();
                    taskStateBlocked(wd);
                }
                break;
                case NANOS_BURST_START:
                if ( events[i].getKey() == wd_id_event ) {
                    enteringWD(events[i].getValue());
                }
                if ( events[i].getKey() == user_code ) {
                    enteringUserCode(events[i].getValue());
                }
                if ( events[i].getKey() == funct_location ) {
                    std::string description = iD->getValueDescription( events[i].getKey(), events[i].getValue());
                    enteringFunctionLocation(events[i].getKey(), events[i].getValue(), description);
                }
                if ( events[i].getKey() == api && events[i].getValue() == wait_group ) {
                    addTaskWait(events[i].getKey(),events[i].getValue());
                }
                break;
                case NANOS_BURST_END:
                if ( events[i].getKey() == wd_id_event ) {
                    leavingWD(events[i].getValue());
                }
                if ( events[i].getKey() == user_code ) {
                    leavingUserCode(events[i].getValue());
                }
                if ( events[i].getKey() == funct_location ) {
                    std::string description = iD->getValueDescription( events[i].getKey(), events[i].getValue());
                    leavingFunctionLocation(events[i].getKey(), events[i].getValue(), description);
                }
                break;
                default:
                break;
            } // switch event type
        } // for loop
#if VERBOSE > 2
        fprintf(stderr,"addEventList done\n\n");
#endif

    } // func: addEventList()

    /**
     *
     * @param thread
     */
    void threadStart( BaseThread &thread ) {
    }

    /**
     *
     * @param thread
     */
    void threadFinish ( BaseThread &thread ) {}

#endif //NANOX_EXTRAE_SUPPORTED_VERSION
}
;

namespace ext
{

class InstrumentationAyudamePlugin: public Plugin
{
public:
    InstrumentationAyudamePlugin()
        :
          Plugin(
                 "Instrumentation which implements Ayudame/Temanejo protocol.",
                 1)
    {
    }
    ~InstrumentationAyudamePlugin()
    {
    }

    void config(Config &cfg)
    {
    }

    void init()
    {
    sys.setInstrumentation( NEW InstrumentationAyudame());
}
};

} // namespace ext

} // namespace nanos

DECLARE_PLUGIN("intrumentation-ayudame",
    nanos::ext::InstrumentationAyudamePlugin)
;
