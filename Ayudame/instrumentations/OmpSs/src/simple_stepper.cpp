#include "simple_stepper.hpp"


#include <iostream>
#include <list>
#include <map>
#include <thread>
#include <chrono>
#include <algorithm>
#include <queue>

namespace stepper
{

int progress = TCA_CONTINUE; //0=continue, 1=break, 2 = step

int step_counter = 0;

std::queue <tca_thread_id_t> thread_queue;

std::list <tca_thread_id_t> thread_progress_list;
std::list <tca_task_id_t> task_progress_list;
std::map <tca_dependency_id_t, std::pair <tca_task_id_t, tca_task_id_t> > dependency_progress_map;

pthread_mutex_t step_counter_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t task_progress_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t thread_progress_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t dependency_progress_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t progress_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t thread_queue_mutex = PTHREAD_MUTEX_INITIALIZER;

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++start of part called by the instrumentation+++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**
 *
 * @param value
 */
void set_progress(tca_progress_t value)
{
    pthread_mutex_lock(&progress_mutex);
    progress = value;
    pthread_mutex_unlock(&progress_mutex);
#if STEPPER_VERBOSE > 0
    std::cout << "set_progress " << tca_progress_str[progress] << "\n";
#endif
}
/**
 *
 * @param value
 */
void add_task(tca_task_id_t value)
{
    pthread_mutex_lock(&task_progress_mutex);
    task_progress_list.push_back(value);
#if STEPPER_VERBOSE > 0
    std::cout << "task_progress_list adding: " << value << "\n";
    std::cout << "task_progress_list contains: ";
    for (auto it = task_progress_list.begin(); it != task_progress_list.end();
        ++it)
        std::cout << ' ' << *it;
    std::cout << '\n';
#endif
    pthread_mutex_unlock(&task_progress_mutex);
}

/**
 *
 * @param value
 */
void remove_task(tca_task_id_t value)
{
    pthread_mutex_lock(&task_progress_mutex);
    task_progress_list.remove(value);
#if STEPPER_VERBOSE > 0
    std::cout << "task_progress_list removing: " << value << "\n";
    std::cout << "task_progress_list contains: ";
    for (auto it = task_progress_list.begin(); it != task_progress_list.end();
        ++it)
        std::cout << ' ' << *it;
    std::cout << '\n';
#endif
    pthread_mutex_unlock(&task_progress_mutex);

}
/**
 *
 * @param value
 */
void add_thread(tca_thread_id_t value)
{
    pthread_mutex_lock(&thread_progress_mutex);
    thread_progress_list.push_back(value);
    pthread_mutex_unlock(&thread_progress_mutex);
}
/**
 *
 * @param value
 */
void remove_thread(tca_thread_id_t value)
{
    pthread_mutex_lock(&thread_progress_mutex);
    thread_progress_list.remove(value);
    pthread_mutex_unlock(&thread_progress_mutex);
}

/**
 *
 * @param from
 * @param to
 */
void add_dependency(tca_task_id_t from, tca_task_id_t to)
{
    pthread_mutex_lock(&dependency_progress_mutex);
    tca_dependency_id_t dep = from;
    dep |= to << 32;
    dependency_progress_map.insert(std::make_pair(dep, std::make_pair(from, to)));
    pthread_mutex_unlock(&dependency_progress_mutex);

    //TODO
    /*
     ayu_client_id_t myclient_id = get_client_id(AYU_CLIENT_OMPSS);
     ayu_event_data_t data;

     data.common.client_id = myclient_id;
     data.add_dependency.dependency_id= dep;
     data.add_dependency.from_id=from;
     data.add_dependency.to_id=to;
     data.add_dependency.dependency_label="dep";
     ayu_event(AYU_ADDDEPENDENCY, data);
     ayu_wipe_data(&data);

     data.common.client_id = myclient_id;
     data.set_property.property_owner_id= dep;
     data.set_property.key="type";
     data.set_property.value="virtual_dependency";
     ayu_event(AYU_SETPROPERTY, data);
     ayu_wipe_data(&data);

     */

}
/**
 *
 * @param from
 * @param to
 */
void remove_dependency(tca_dependency_id_t id)
{
    pthread_mutex_lock(&dependency_progress_mutex);
    dependency_progress_map.erase(id);
    pthread_mutex_unlock(&dependency_progress_mutex);
}

void increase_step_counter()
{
    pthread_mutex_lock(&step_counter_mutex);
    if(step_counter<=0){
        step_counter=0;
    }
    step_counter++;
    pthread_mutex_unlock(&step_counter_mutex);
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++end of part called by the instrumentation+++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++   start of part called by the runtime    ++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void do_step_for_thread(tca_thread_id_t thread_id)
{
#if STEPPER_VERBOSE > 0
    fprintf(stderr, "do_step_for_threadTask : %d\n", thread_id);
#endif
    pthread_mutex_lock(&step_counter_mutex);
    while (step_counter <= 0) {

        //---------------check if progess was switched to continue-> release threads
        pthread_mutex_lock(&progress_mutex);
        if (progress == TCA_CONTINUE) {
            pthread_mutex_unlock(&progress_mutex);
            break;
        }
        pthread_mutex_unlock(&progress_mutex);
        //---------------check if progess was switched to continue-> release threads


        pthread_mutex_unlock(&step_counter_mutex);
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        pthread_mutex_lock(&step_counter_mutex);
    }
#if STEPPER_VERBOSE > 0
    fprintf(stderr, "step_counter %d\n", step_counter);
#endif
    step_counter--;
    pthread_mutex_unlock(&step_counter_mutex);
}

/**
 *
 */
void add_thread_to_queue_and_step(tca_thread_id_t thread_id)
{
#if STEPPER_VERBOSE > 0
    fprintf(stderr, "add_thread_to_queue_and_step :%d\n", thread_id);
#endif

    pthread_mutex_lock(&thread_queue_mutex);
    thread_queue.push(thread_id);
    pthread_mutex_unlock(&thread_queue_mutex);

    pthread_mutex_lock(&thread_queue_mutex);
    while (thread_queue.front() != thread_id) {
        pthread_mutex_unlock(&thread_queue_mutex);
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        //std::this_thread::yield();
        pthread_mutex_lock(&thread_queue_mutex);
    }
    pthread_mutex_unlock(&thread_queue_mutex);

    do_step_for_thread(thread_id);

    pthread_mutex_lock(&thread_queue_mutex);
    thread_queue.pop();
    pthread_mutex_unlock(&thread_queue_mutex);

}

/**
 *
 * @param id
 */
void stepper_request_progress_task(tca_task_id_t id)
{
    pthread_mutex_lock(&task_progress_mutex);
    while (std::find(task_progress_list.begin(), task_progress_list.end(), id) != task_progress_list.end()) {
        pthread_mutex_unlock(&task_progress_mutex);
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        pthread_mutex_lock(&task_progress_mutex);
    }
    pthread_mutex_unlock(&task_progress_mutex);
}

/**
 *
 * @param id
 */
void stepper_request_progress_thread(tca_thread_id_t id)
{
    pthread_mutex_lock(&thread_progress_mutex);
    while (std::find(thread_progress_list.begin(), thread_progress_list.end(), id) != thread_progress_list.end()) {
        pthread_mutex_unlock(&thread_progress_mutex);
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        pthread_mutex_lock(&thread_progress_mutex);
    }
    pthread_mutex_unlock(&thread_progress_mutex);
}

/**
 *
 */
void stepper_request_progress(tca_task_id_t task_id, tca_thread_id_t thread_id)
{

#if STEPPER_VERBOSE > 0
    fprintf(stderr, "stepper_request_progress %d: %d\n", task_id, thread_id);
#endif


    stepper_request_progress_task(task_id);

    stepper_request_progress_thread(thread_id);

    pthread_mutex_lock(&progress_mutex);
    if (progress == TCA_CONTINUE) {
        pthread_mutex_unlock(&progress_mutex);
        return;
    }
    pthread_mutex_unlock(&progress_mutex);
    //while (progress == TCA_BREAK) {
    //    pthread_mutex_unlock(&progress_mutex);
    //    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    //    pthread_mutex_lock(&progress_mutex);
    //}
    //pthread_mutex_unlock(&progress_mutex);

    pthread_mutex_lock(&progress_mutex);
    if (progress == TCA_BREAK) {
        pthread_mutex_unlock(&progress_mutex);
        add_thread_to_queue_and_step(thread_id);

    }
    pthread_mutex_unlock(&progress_mutex);
}

/**
 *
 * @param from
 * @param to
 */
/*
 void stepper_request_progress_dependency(tca_dependency_id_t from, tca_dependency_id_t to)
 {

 bool lock = true;
 pthread_mutex_lock(&dependency_progress_mutex);
 for (auto it = dependency_progress_map.begin();
 it != dependency_progress_map.end(); ++it) {
 if (it->second.first == from && it->second.second == to) {
 pthread_mutex_unlock(&dependency_progress_mutex);
 std::this_thread::sleep_for(std::chrono::milliseconds(100));
 pthread_mutex_lock(&dependency_progress_mutex);
 }
 }
 pthread_mutex_unlock(&dependency_progress_mutex);
 }
 */

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++   end of part called by the runtime    ++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
}
