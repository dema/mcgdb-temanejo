#ifndef SIMPLE_STEPPER_HPP
#define SIMPLE_STEPPER_HPP

#include "tca_runtime.h"

#ifndef STEPPER_VERBOSE
#define STEPPER_VERBOSE 0
#endif

namespace stepper
{


void stepper_request_progress(tca_task_id_t task_id,tca_thread_id_t thrad_id);

//void stepper_request_progress_task(tca_task_id_t id);

//void stepper_request_progress_dependency(tca_dependency_id_t from, tca_dependency_id_t to);

//void stepper_request_progress_thread(tca_thread_id_t id);

void set_progress(tca_progress_t value);

void increase_step_counter();

void add_task(tca_task_id_t value);
void remove_task(tca_task_id_t value);
void add_thread(tca_thread_id_t value);
void remove_thread(tca_thread_id_t value);
void add_dependency(tca_task_id_t from, tca_task_id_t to);
void remove_dependency(tca_dependency_id_t id);
}

#endif //SIMPLE_STEPPER_HPP
