#ifndef TCA_TYPES_H
#define TCA_TYPES_H

#define TCA_VERSION 1
#define _comparelimit 80

#include <stdint.h>

/* indicates if a control request was executed succesfully */
/* maybe better use error strings, and NULL if success */
typedef enum
{
    TCA_OK,    // request execute without error
    TCA_FAIL,  // failed to execute request
} tca_success_t;
static const char * const tca_success_str[] = { "TCA_OK",
                                              "TCA_FAIL" };

typedef enum
{
    TCA_CONTINUE,
    TCA_BREAK
} tca_progress_t;
static const char * const tca_progress_str[] = { "TCA_CONTINUE",
                                              "TCA_BREAK" };

typedef uint64_t tca_task_id_t;

typedef uint64_t tca_dependency_id_t;

typedef uint64_t tca_thread_id_t;

typedef uint64_t tca_priority_t;


typedef void (*tca_interface_fn_t)(void);

typedef tca_interface_fn_t (*tca_function_lookup_t)(const char *entry_point);

#endif  //TCA_TYPES_H
