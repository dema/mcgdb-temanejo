//#include <ayudame_types.h>
#include <ayudame.h>
#include <stdio.h>

#include <ayudame_lib.hpp>
#include "client_event_handler/client_event_handler.hpp"

extern "C" {

    void ayu_event(ayu_event_t event, ayu_event_data_t data) {

        //std::shared_ptr<Client_event_handler> ayu ( (Client_event_handler*) data.common.client_id );
        Client_event_handler* ayu = (Client_event_handler*) data.common.client_id;
        if(ayu==nullptr) {
            printf("Client_event_handler == NULL");
            return;
        }

        ayu->event(event, data);
    }



    void tca_initialize(tca_function_lookup_t lookup,
                        const char *runtime_version,
                        unsigned int tca_version){
        if(NULL != strstr(runtime_version, "Nanos")){
            Client_event_handler* ayu = get_ayudame_ptr(AYU_CLIENT_OMPSS);
            ayu->tca_initialize(lookup, runtime_version, tca_version);
        }else{
            return;
        }
    }



    void ayu_register_get_callback(int (*f)(),tca_callback_t type,ayu_event_data_t data){


        //std::shared_ptr<Client_event_handler> ayu ( (Client_event_handler*) data.common.client_id );
        Client_event_handler* ayu = (Client_event_handler*) data.common.client_id;
        if(ayu==nullptr) {
            printf("Client_event_handler == NULL");
            return;
        }

        ayu->register_get_callback(f,type);
    }



    /*use this like so:
     * ayu_client_id_t my_client_id = get_client_id(AYU_CLIENT_WHATEVER);
     * // data construction
     * data.common.client_id = my_client_id
     * ayu_event(AYU_SOMEEVENT, data);
     */
    ayu_client_id_t get_client_id(ayu_client_t client) {
        auto& ayu = ayu::dame(); // give me ayudame
        return (ayu_client_id_t) ayu.get_client_handler(client);
    }

    /*use this like so:
     * auto ayu = get_ayudame_ptr(AYU_CLIENT_WHATEVER);
     * ayu->event(AYU_SOMEEVENT, data);
     */
    // TODO: provide reference?
    Client_event_handler* get_ayudame_ptr(ayu_client_t client) {
        auto& ayu = ayu::dame(); // give me ayudame
        return ayu.get_client_handler(client);
    }

    void ayu_wipe_data(ayu_event_data_t *data) {
        memset(data, 0, sizeof(*data));
    }

}
