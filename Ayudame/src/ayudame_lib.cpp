/**
 * \file ayudame_lib.cpp
 * \brief implementation of the class Ayudame and the namspace ayu
 * */

#include "ayudame_lib.hpp"
#include "ayudame_log.h"

#ifndef AYULOG
// ** FOLLOWING LINE SHOULD BE USED ONCE AND ONLY ONCE IN WHOLE APPLICATION **
_INITIALIZE_EASYLOGGINGPP
#endif

Ayudame::Ayudame(std::string config_filename)
    : m_config(),
      m_connect_manager(*this),
      m_client_event_manager(*this),
      m_start_time(chrono_clock::now()),
      m_mutex_event(),
      my_rank(0),
      m_connection_tree(false),
      m_buffer( { }),
      m_read_buffer_thread(nullptr),
      m_request_socket_thread(nullptr),
      m_active_read_buffer_thread(false),
      m_active_request_socket_thread(false)
{

    init_logging();
    LOG(INFO) << "---AYUDAME_CTOR---" << this << " " << std::this_thread::get_id();

    m_config.init(config_filename);
#ifdef AYULOG

    reconfigure_logging_ayu();
#else

    reconfigure_logging_el();
#endif

    //-----------------------------------
    //-----get mpi information ----------
    char* OMPI_COMM_WORLD_RANK; //- the MPI rank of this process
	//OMPI_COMM_WORLD_RANK = getenv("$PBS_NUM_NODES");
    OMPI_COMM_WORLD_RANK = getenv("OMPI_COMM_WORLD_RANK");
    printf("OMPI_COMM_WORLD_RANK %s \n", OMPI_COMM_WORLD_RANK);
    if (NULL != OMPI_COMM_WORLD_RANK) {
        my_rank = atoi(OMPI_COMM_WORLD_RANK);
    } else {
        my_rank = 0;
        m_connection_tree.store(true);
        init_connect_manager(); // otherwise it will be called from Event handler
    }
    //-----------------------------------

    //-----------------------------------
    //----------CREATE LOOPS-------------
    create_buffer_loop();
    create_request_socket_loop();
    //-----------------------------------

    std::time_t start_time = chrono_clock::to_time_t(m_start_time);
    std::string start_time_str(ctime(&start_time));
    start_time_str = start_time_str.substr(0, start_time_str.length() - 1);
    LOG(INFO) << start_time_str;
}

Ayudame::~Ayudame()
{

    //TODO shopulnt be called ...this is necessary because of the destruction process in nanos
//    int i = 0;
//    while (i != 100) {
//        std::this_thread::sleep_for(std::chrono::milliseconds(10));
//        i++;
//        std::this_thread::yield();
//    }

    LOG(INFO) << my_rank << "---AYUDAME_DTOR---";
    //TODO shopulnt be called ...this is necessary because of the destruction process in nanos
    //m_client_event_manager.force_shutdown();

    std::time_t start_time = chrono_clock::to_time_t(m_start_time);
    std::string start_time_str(ctime(&start_time));
    start_time_str = start_time_str.substr(0, start_time_str.length() - 1);
    LOG(INFO) << start_time_str;
    LOG(INFO) << get_timestamp() << " ms since start";
    LOG(INFO) << my_rank << "---AYUDAME_DTOR---++++";

    Log::CloseLogFile();
    fprintf (stderr,"logfile closed");
}

void Ayudame::shutdown()
{

    LOG(DEBUG) << "Ayudame::shutdown()";
    while (get_buffer_size() != 0) {
        //LOG(DEBUG)<< "m_buffer.size()" << get_buffer_size();
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    m_active_read_buffer_thread.store(false);

    while (m_read_buffer_thread != nullptr) {
        if (m_read_buffer_thread->joinable()) {
            m_read_buffer_thread->join();
            m_read_buffer_thread = nullptr;
        } else {
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
            LOG(ERROR) << "Cant join read socket thread";
        }
    }

    m_active_request_socket_thread.store(false);

    while (m_request_socket_thread != nullptr) {
        if (m_request_socket_thread->joinable()) {
            m_request_socket_thread->join();
            m_request_socket_thread = nullptr;
        } else {
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
            LOG(ERROR) << "Cant join request socket thread";
        }
    }

    m_connect_manager.shutdown();

    if (get_buffer_size() != 0) {
        LOG(ERROR) << "BUFFER NOT emtpty";
    }


}

void Ayudame::init_logging()
{
#ifdef AYULOG
    set_default_logging_ayu();
#else

    el::Configurations el_conf;
    set_default_logging_el(el_conf);
    el::Loggers::reconfigureLogger("default", el_conf);
#endif

}

#ifdef AYULOG
void Ayudame::set_default_logging_ayu()
{
}
#else

void Ayudame::set_default_logging_el(el::Configurations &el_default_conf) {
    el_default_conf.setToDefault();
    //    el_default_conf.setGlobally(el::ConfigurationType::Format, "%datetime{%Y-%M-%d %H-%m-%s} %level: %msg");
    el_default_conf.setGlobally(el::ConfigurationType::Format, "Ayudame-%level: %msg");
    //el_default_conf.set(el::Level::Debug, el::ConfigurationType::Format, "Ayudame-%level: %host:%loc:%func:\n\t%msg");
    el_default_conf.set(el::Level::Debug, el::ConfigurationType::Format, "Ayudame-%level: \t%msg");
    //el_default_conf.set(el::Level::Fatal, el::ConfigurationType::Format, "Ayudame-%level: %host:%loc:%func:\n\t%msg");
    el_default_conf.set(el::Level::Fatal, el::ConfigurationType::Format, "Ayudame-%level: \t%msg");
    el_default_conf.set(el::Level::Verbose, el::ConfigurationType::Format, "Ayudame-%level %vlevel: %msg");

    el::Helpers::addFlag(el::LoggingFlag::ColoredTerminalOutput);
    el::Helpers::addFlag(el::LoggingFlag::ImmediateFlush);
    el::Helpers::addFlag(el::LoggingFlag::LogDetailedCrashReason);

}
#endif

#ifdef AYULOG
void Ayudame::reconfigure_logging_ayu()
{

    set_default_logging_ayu();

    std::stringstream logfile_name;

    logfile_name << "ayu_log" << my_rank << ".log";

    // set new log file name
    Log::LogFileName(logfile_name.str());

    if (m_config.get_bool("logging.debug") == true) {
        Log::ReportingLevel() = DEBUG4; // TODO
    } else if (m_config.get_bool("logging.info") == true) {
        Log::ReportingLevel() = INFO;
    } else if (m_config.get_bool("logging.warning") == true) {
        Log::ReportingLevel() = WARNING;
    } else {
        Log::ReportingLevel() = ERROR;
    }

}

#else

void Ayudame::reconfigure_logging_el() {
    el::Configurations el_conf;

    set_default_logging_el(el_conf);

    std::stringstream logfile_name;
    //#ifdef AYU_MPI
    logfile_name << "ayu_log" << get_rank() << ".log";
    //#else
    //    logfile_name << "logs/ayu_log.log";
    //#endif //AYU_MPI

    el_conf.setGlobally(el::ConfigurationType::Filename, logfile_name.str().c_str() );

    el_conf.setGlobally(el::ConfigurationType::LogFlushThreshold, "1");

    unsigned verbosity_level = 0;
    verbosity_level = m_config.get_unsigned("logging.verbosity_level");
    int argc_elpp = 2;
    std::stringstream verbosity_option;
    verbosity_option << "--v=" << verbosity_level;

    const char *argv_elpp[] = {"ayudame", verbosity_option.str().c_str()
    };
    //const char *argv_elpp[] = {"ayudame", verbosity_option.str().c_str(), logfile_option.str().c_str() };
    _START_EASYLOGGINGPP(argc_elpp, argv_elpp);

    if (m_config.get_bool("logging.debug") == true) {
        el_conf.set(el::Level::Debug, el::ConfigurationType::Enabled, "true");
    } else {
        el_conf.set(el::Level::Debug, el::ConfigurationType::Enabled, "false");
    }

    if (m_config.get_bool("logging.info") == true) {
        el_conf.set(el::Level::Info, el::ConfigurationType::Enabled, "true");
    } else {
        el_conf.set(el::Level::Info, el::ConfigurationType::Enabled, "false");
    }

    if (verbosity_level > 0) {
        el_conf.set(el::Level::Verbose, el::ConfigurationType::Enabled, "true");
    } else {
        el_conf.set(el::Level::Verbose, el::ConfigurationType::Enabled, "false");
    }

    if (m_config.get_bool("logging.warning") == true) {
        el_conf.set(el::Level::Warning, el::ConfigurationType::Enabled, "true");
    } else {
        el_conf.set(el::Level::Warning, el::ConfigurationType::Enabled, "false");
    }

    if (m_config.get_bool("logging.error") == true) {
        el_conf.set(el::Level::Error, el::ConfigurationType::Enabled, "true");
    } else {
        el_conf.set(el::Level::Error, el::ConfigurationType::Enabled, "false");
    }

    el::Loggers::reconfigureLogger("default", el_conf);

}
#endif

void Ayudame::init_connect_manager()
{

    if (m_config.get_bool("connect.stdout_human") == true) {
        m_connect_manager.init_connect(AYU_CONNECT_STDOUT_HUMAN);
    }
    if (m_config.get_bool("connect.stdout_raw") == true) {
        m_connect_manager.init_connect(AYU_CONNECT_STDOUT_RAW);
    }
    if (m_config.get_bool("connect.dot") == true) {
        m_connect_manager.init_connect(AYU_CONNECT_DOT);
    }
    if (m_config.get_bool("connect.xml") == true) {
           m_connect_manager.init_connect(AYU_CONNECT_XML);
       }
    if (m_config.get_bool("connect.temanejo1") == true) {
        m_connect_manager.init_connect(AYU_CONNECT_TEMANEJO1);
    }
    if (m_config.get_bool("connect.temanejo") == true) {
        m_connect_manager.init_connect(AYU_CONNECT_TEMANEJO);
    }

}

void Ayudame::event(std::shared_ptr <Intern_event> data)
{
    //std::lock_guard<std::mutex> local_mutex(m_mutex_event);
    //write_buffer(data);
    buffer_push(data);

}

void Ayudame::set_connection_tree_true()
{
    m_connection_tree.store(true);
}

void Ayudame::create_buffer_loop()
{
    m_active_read_buffer_thread.store(true);
    m_read_buffer_thread = std::make_shared <std::thread>
                           (&Ayudame::read_buffer_loop, this);
    if (m_read_buffer_thread == nullptr) {
        LOG(ERROR) << "m_read_buffer_thread== nullptr";
    }
}

void Ayudame::create_request_socket_loop()
{
    m_active_request_socket_thread.store(true);
    m_request_socket_thread = std::make_shared <std::thread>
                              (&Ayudame::request_socket_loop, this);
    if (m_request_socket_thread == nullptr) {
        LOG(ERROR) << "m_request_socket_thread== nullptr";
    }
}

void Ayudame::read_buffer_loop()
{

    bool data_received = false;
    while (m_active_read_buffer_thread.load() == true) {
        if (m_connection_tree.load() == true) {
            data_received = read_buffer();
            if (!data_received) {
                //sleep
            }
        }    //else sleep
    }
}

bool Ayudame::read_buffer()
{
    if (get_buffer_size() != 0) {
        m_connect_manager.msg_send_out(buffer_pop());
        return true;
    }
    return false;
}

void Ayudame::request_socket_loop()
{
    while (m_active_request_socket_thread.load() == true) {
        if (m_connection_tree.load() == true) {
            std::shared_ptr <Intern_event> request = m_connect_manager.msg_read();
            if (request != nullptr) {
                m_client_event_manager.msg_send_out(request);
            } else {
                std::this_thread::sleep_for(std::chrono::milliseconds(10));
            }
        } else {
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
    }
}

std::shared_ptr <Intern_event> Ayudame::buffer_pop()
{
    std::lock_guard <std::mutex> local_mutex(m_mutex_event);
    std::shared_ptr <Intern_event> tmp = *m_buffer.begin();
    m_buffer.erase(m_buffer.begin());
    return tmp;

}
void Ayudame::buffer_push(std::shared_ptr <Intern_event> data)
{
    std::lock_guard <std::mutex> local_mutex(m_mutex_event);
    m_buffer.push_back(data);
}

int Ayudame::get_buffer_size()
{
    std::lock_guard <std::mutex> local_mutex(m_mutex_event);
    return m_buffer.size();
}

/* Ayudame::get_client_handler must return a pointer because its return value
 * will be stored as a number in user code.
 */
Client_event_handler* Ayudame::get_client_handler(const ayu_client_t client)
{
    return m_client_event_manager.get_client_handler(client).get();
}

const Configuration& Ayudame::get_config() const
{
    return m_config;
}

Connect_manager* Ayudame::get_connect_manager() // const?
{
    return &m_connect_manager;
}

Client_event_manager* Ayudame::get_client_event_manager() // const?
{
    return &m_client_event_manager;
}

uint64_t Ayudame::get_timestamp() const
{
    chrono_clock::time_point now_time = chrono_clock::now();

    // std::chrono::duration<unsigned, std::milli>
    auto timestamp = std::chrono::duration_cast <std::chrono::milliseconds>
                     (now_time - m_start_time);
    return timestamp.count();
}

/*
 uint64_t Ayudame::rdtsc()
 {
 uint32_t lo, hi;
 __asm__ __volatile__ (	// serialize
 "xorl %%eax,%%eax \n        cpuid":::"%rax",
 "%rbx", "%rcx", "%rdx");
 __asm__ __volatile__ ("rdtsc":"=a" (lo), "=d" (hi));
 return (uint64_t) hi << 32 | lo;
 }
 */

namespace ayu
{
std::mutex dame_lock;
Ayudame& dame()
{
    std::lock_guard <std::mutex> lck(dame_lock);
    static Ayudame ayu("ayudame.cfg");   // TODO: this is thread safe, isn't it?
    return ayu;

}
}
