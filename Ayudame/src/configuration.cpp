#include "configuration.hpp"
#include "ayudame_log.h"

Configuration::Configuration()
  : m_filename("")
{
    LOG(DEBUG)<< "Configuration::Configuration()";
}

Configuration::~Configuration() {
    LOG(DEBUG)<< "Configuration::~Configuration()";
}

void Configuration::init(std::string filename) {
    set_default();
    if (filename != "") {
        set_from_file(filename);
    }
}

std::string Configuration::get(const std::string key) const {
    return get_string(key);
}

std::string Configuration::get_string(const std::string key) const {
    std::string result("");

    try {
        result = m_data.at(key);
    }
    catch (const std::out_of_range& oor) {
        LOG(ERROR) << "Out of Range error: " << oor.what();
        LOG(ERROR) << "key \"" << key << "\" does not exist in configuration";
    }

    //    LOG(INFO) << key << " retrieved " << result;

    return result;
}

unsigned Configuration::get_unsigned(const std::string key) const {
    unsigned result(strtol(get_string(key).c_str(),nullptr,10));
    return result;
}

bool Configuration::get_bool(const std::string key) const {
    bool result(false);
    std::string result_str = get_string(key);

    if (result_str == "true") {
        result = true;
    } else if (result_str == "false") {
        result = false;
    } else {
        LOG(ERROR) << key << " has unknown value " << result_str
            << " and can not be retrieved.";
        return result;
    }

    return result;
}


void Configuration::set(const std::string key, const char* value) {
    set(key, std::string(value));
}


void Configuration::set(const std::string key, const std::string value)
{
    try {
        m_data.at(key) = value;
    }
    catch (const std::out_of_range& oor) {
        LOG(ERROR) << "Out of Range error: " << oor.what() << '\n';
        LOG(ERROR) << "key \"" << key << "\" does not exist in configuration";
    }

    //    LOG(INFO) << key << " set to \"" << value << "\"";
}

void Configuration::set(const std::string key, const unsigned value) {
    std::stringstream value_str;
    value_str << value;
    try {
        m_data.at(key) = value_str.str();
    }
    catch (const std::out_of_range& oor) {
        LOG(ERROR) << "Out of Range error: " << oor.what() << '\n';
        LOG(ERROR) << "key \"" << key << "\" does not exist in configuration";
    }

    //    LOG(INFO) << key << " set to \"" << value_str.str() << "\"";
}

void Configuration::set(const std::string key, const bool value) {
    std::string value_str;
    if (value == true) {
        value_str = "true";
    } else {
        value_str = "false";
    }
    try {
        m_data.at(key) = value_str;
    }
    catch (const std::out_of_range& oor) {
        LOG(ERROR) << "Out of Range error: " << oor.what() << '\n';
        LOG(ERROR) << "key \"" << key << "\" does not exist in configuration";
    }

    //    LOG(INFO) << key << " set to \"" << value_str << "\"";
}

void Configuration::set_from_file(const std::string filename)
{
    LOG(INFO) << "Reading configuration from file " << filename;
    c4c::Configuration* cfg = c4c::Configuration::create();
    try {
        cfg->parse(filename.c_str());
        m_filename = filename;
        for (auto& param : m_data) {
            std::string scope(param.first.substr(0,param.first.find('.')));
            std::string key(param.first.substr(param.first.find('.') + 1,
                                               std::string::npos));
            set(param.first, cfg->lookupString(scope.c_str(), key.c_str()));
        }

    } catch(const c4c::ConfigurationException & ex) {
        LOG(ERROR) << ex.c_str();
    }
    cfg->destroy();
}

void Configuration::set_default()
{
    LOG(DEBUG) << "Configuration set to default values";
    m_filename = "";
    m_data = m_default_data;
}
