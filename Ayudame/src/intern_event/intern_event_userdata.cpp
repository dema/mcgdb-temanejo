#include "intern_event/intern_event_userdata.hpp"
#include "ayudame_log.h"

Intern_event_userdata::Intern_event_userdata(ayu_client_id_t _client_id,
                                             ayu_event_t _event,
                                             ayu_count_t _timestamp,
                                             std::string _data)
  : Intern_event(_client_id, _event, _timestamp),
    m_data(_data)
{
}

Intern_event_userdata::Intern_event_userdata(ayu_event_data_t data)
  :  Intern_event_userdata(data.common.client_id,
                           data.common.event,
                           data.common.timestamp,
                           std::string(data.userdata.data))
{
}

Intern_event_userdata::Intern_event_userdata(const char *message)
  : m_data("")
{

    ayu_count_t msg_index = 0; ///< temprary index while packing the data
    ayu_count_t msg_size = 0; ///< message size, temporarily used while packing
    ayu_count_t label_size = 0; ///< label size, temporarily set while packing

    init(message, &msg_index, &msg_size, &label_size);

    if (label_size != 0) {
        char tmp[label_size];
        std::char_traits<char>::copy(tmp, &message[msg_index], label_size);
        tmp[label_size] = '\0';
        m_data=std::string(tmp);
        msg_index += label_size;
    }
    /*
    LOG(INFO) << "UNPACK_2 event " << ayu_event_str[m_event] << " msg_size:" << msg_size
    << " label_size:" << label_size<< " client_id:" << m_client_id << " timestamp:" << m_timestamp
    << " data_size:" << m_data.size() << " data:" << m_data;
    */
}


void Intern_event_userdata::pack(char* message) {

    ayu_count_t msg_index = 0; ///< temprary index while packing the data
    ayu_count_t msg_size = 0; ///< message size, temporarily used while packing
    ayu_count_t label_size = 0; ///< label size, temporarily set while packing

    msg_size =  get_message_size();
    label_size = m_data.size();

    Intern_event::pack(message, &msg_index, &msg_size, &label_size);


    if (m_data.size() != 0) {
        std::char_traits<char>::copy(&message[msg_index],
                                     m_data.c_str(),
                                     m_data.size());
        msg_index += m_data.size();
    }

    /*
    LOG(INFO) << "PACK_2 event " << ayu_event_str[m_event] << " msg_size:" << msg_size
    << " label_size:" << label_size<< " client_id:" << m_client_id << " timestamp:" << m_timestamp
    << " data_size:" << m_data.size() << " data:" << m_data;
    */


}

void Intern_event_userdata::print(ayu_printformat_t printformat,
                                        std::ostream &os) {
    Intern_event::print(printformat, os);
    if (RAW == printformat) {
        os << m_data.size() << " " << m_data.data() << "\n";
    } else if (HUMAN == printformat) {
        os << "Size=" << m_data.size() << " UserData=" << m_data.data() << "\n";
    }
}


ayu_count_t Intern_event_userdata::get_message_size() const {
    ayu_count_t message_size = 0;
    message_size += Intern_event::get_message_size();
    message_size +=  m_data.size() ; // m_data


    return message_size;
}
