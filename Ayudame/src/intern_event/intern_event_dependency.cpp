#include "intern_event/intern_event_dependency.hpp"
#include "ayudame_log.h"



Intern_event_dependency::Intern_event_dependency(ayu_client_id_t _client_id,
        ayu_event_t _event,
        ayu_count_t _timestamp,
        ayu_id_t _dependency_id,
        ayu_id_t _from_id,
        ayu_client_id_t _to_client_id,
        ayu_id_t _to_id,
        std::string _dependency_label)
        : Intern_event(_client_id, _event, _timestamp),
        m_dependency_id(_dependency_id),
        m_from_id(_from_id),
        m_to_client_id(_to_client_id),
        m_to_id(_to_id),
m_dependency_label(_dependency_label) {}



Intern_event_dependency::Intern_event_dependency(ayu_event_data_t data)
        :  Intern_event_dependency(data.common.client_id,
                                   data.common.event,
                                   data.common.timestamp,
                                   data.add_dependency.dependency_id,
                                   data.add_dependency.from_id,
                                   data.common.client_id,
                                   data.add_dependency.to_id,
                           std::string(data.add_dependency.dependency_label)) {}

Intern_event_dependency::Intern_event_dependency(const char *message)
        : m_dependency_id(0),
        m_from_id(0),
        m_to_client_id(0),
        m_to_id(0),
m_dependency_label("") {

    ayu_count_t msg_index = 0; ///< temprary index while packing the data
    ayu_count_t msg_size = 0; ///< message size, temporarily used while packing
    ayu_count_t label_size = 0; ///< label size, temporarily set while packing

    init(message,&msg_index, &msg_size, &label_size);

    // add_dependency:   dependency_id
    std::char_traits<char>::copy((char*) &m_dependency_id, &message[msg_index], ayu_sizeof_value);
    msg_index += ayu_sizeof_value;

    // add_dependency:   froid
    std::char_traits<char>::copy((char*) &m_from_id, &message[msg_index], ayu_sizeof_value);
    msg_index += ayu_sizeof_value;

    // add_dependency:   to_client_id
    std::char_traits<char>::copy((char*) &m_to_client_id, &message[msg_index], ayu_sizeof_value);
    msg_index += ayu_sizeof_value;

    // add_dependency:   to_id
    std::char_traits<char>::copy((char*) &m_to_id, &message[msg_index], ayu_sizeof_value);
    msg_index += ayu_sizeof_value;

    if (label_size != 0) {
        // add_dependency:   dependency_label
        char tmp[label_size];
        std::char_traits<char>::copy(tmp, &message[msg_index], label_size);
        tmp[label_size] = '\0'; // append null-character
        msg_index += label_size;
        m_dependency_label =  std::string(tmp);

    }


    //LOG(INFO) << "UNPACK_2 event " << ayu_event_str[m_event] << " msg_size:" << msg_size
    //<< " label_size:" << label_size<< " client_id:" << m_client_id << " timestamp:" << m_timestamp
    //<< " dependency_id:" << m_dependency_id << " froid:" << m_from_id
    //<< " to_id:" << m_to_id << " dependency_label:" << m_dependency_label;


}


void Intern_event_dependency::pack(char* message) {

    ayu_count_t msg_index = 0; ///< temprary index while packing the data
    ayu_count_t msg_size = 0; ///< message size, temporarily used while packing
    ayu_count_t label_size = 0; ///< label size, temporarily set while packing

    msg_size =  get_message_size();
    label_size = m_dependency_label.size();

    Intern_event::pack(message, &msg_index, &msg_size, &label_size);

    std::char_traits<char>::copy(&message[msg_index],
                                 (char*) &m_dependency_id,
                                 ayu_sizeof_value);
    msg_index += ayu_sizeof_value;
    std::char_traits<char>::copy(&message[msg_index],
                                 (char*) &m_from_id,
                                 ayu_sizeof_value);
    msg_index += ayu_sizeof_value;
    std::char_traits<char>::copy(&message[msg_index],
                                 (char*) &m_to_client_id,
                                 ayu_sizeof_value);
    msg_index += ayu_sizeof_value;
    std::char_traits<char>::copy(&message[msg_index],
                                 (char*) &m_to_id,
                                 ayu_sizeof_value);
    msg_index += ayu_sizeof_value;
    if (label_size != 0) {
        std::char_traits<char>::copy(&message[msg_index],
                                     m_dependency_label.c_str(),
                                     label_size);
        msg_index += label_size;
    }

    //LOG(INFO) << "PACK_2 event " << ayu_event_str[m_event] << " msg_size:" << msg_size
    //<< " label_size:" << label_size<< " client_id:" << m_client_id << " timestamp:" << m_timestamp
    //<< " dependency_id:" << m_dependency_id << " from_id:" << m_from_id
    //<< " to_id:" << m_to_id << " dependency_label:" << m_dependency_label;


}

void Intern_event_dependency::print(ayu_printformat_t printformat,
                                    std::ostream &os) {
    Intern_event::print(printformat, os);
    if (RAW == printformat) {
        os << m_dependency_id << " " << m_from_id << " " << m_to_id << " " << m_to_client_id << " " << m_dependency_label << "\n";
    } else if(HUMAN == printformat) {
        os << "DependencyID=" << m_dependency_id << " FromID=" << m_from_id << " To_ID="  << m_to_id << " To_CLIENT_ID="  << m_to_client_id <<" DependencyLabel=" << m_dependency_label << "\n";
    } else if(DOT == printformat) {
        os << m_from_id << " -> " << m_to_id << "\n";
    } else if(XML == printformat) {
        os << xml_seperator << m_dependency_id <<  xml_seperator << m_from_id << xml_seperator << m_to_id << xml_seperator << m_to_client_id  << xml_seperator << m_dependency_label <<"\n";
    }
}

ayu_count_t Intern_event_dependency::get_message_size() const {
    ayu_count_t message_size = 0;
    message_size += Intern_event::get_message_size();
    message_size += 4 * ayu_sizeof_value; // m_dependency_id, m_from_id, m_to_id, m_to_client_id
    message_size +=  m_dependency_label.size()  ; // m_task_label

    return message_size;
}
