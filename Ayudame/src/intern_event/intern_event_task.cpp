#include "intern_event/intern_event_task.hpp"
#include "ayudame_log.h"

Intern_event_task::Intern_event_task(ayu_client_id_t _client_id,
                                     ayu_event_t _event,
                                     ayu_count_t _timestamp,
                                     ayu_id_t _task_id,
                                     ayu_id_t _scope_client_id,
                                     ayu_id_t _scope_id,
                                     std::string _task_label)
  : Intern_event(_client_id, _event, _timestamp),
    m_task_id(_task_id),
    m_scope_client_id(_scope_client_id),
    m_scope_id(_scope_id),
    m_task_label(_task_label)
{
}

Intern_event_task::Intern_event_task(ayu_event_data_t data)
  : Intern_event_task(data.common.client_id,
                      data.common.event,
                      data.common.timestamp,
                      data.add_task.task_id,
                      data.common.client_id,//_scope_client_id default set to client_id
                      data.add_task.scope_id,
                      std::string(data.add_task.task_label))
{
}

Intern_event_task::Intern_event_task(const char *message)
  : m_task_id(0),
    m_scope_client_id(0),
    m_scope_id(0),
    m_task_label("")
{

    ayu_count_t msg_index = 0; ///< temprary index while packing the data
    ayu_count_t msg_size = 0; ///< message size, temporarily used while packing
    ayu_count_t label_size = 0; ///< label size, temporarily set while packing

    init(message, &msg_index, &msg_size, &label_size);

    // add_task: task_id
    std::char_traits<char>::copy((char*) &m_task_id, &message[msg_index], ayu_sizeof_value);
    msg_index += ayu_sizeof_value;

    // add_task: scope_client_id
    std::char_traits<char>::copy((char*) &m_scope_client_id, &message[msg_index], ayu_sizeof_value);
    msg_index += ayu_sizeof_value;

    // add_task: scope_id
    std::char_traits<char>::copy((char*) &m_scope_id, &message[msg_index], ayu_sizeof_value);
    msg_index += ayu_sizeof_value;

    if (label_size != 0) {
        // add_task: task_label
        char tmp[label_size];
        std::char_traits<char>::copy(tmp, &message[msg_index], label_size);
        tmp[label_size] = '\0'; // append null-character
        msg_index += label_size;
        m_task_label= std::string(tmp);
    }

    //LOG(INFO) << "UNPACK_2 event " << ayu_event_str[m_event] << " msg_size:" << msg_size
    //<< " label_size:" << label_size<< " client_id:" << m_client_id << " timestamp:" << m_timestamp
    //<< " task_id:" << m_task_id << " scope_id:" << m_scope_id
    //<< " task_label:" << m_task_label;

}





void Intern_event_task::pack(char* message) {

    ayu_count_t msg_index = 0; ///< temprary index while packing the data
    ayu_count_t msg_size = 0; ///< message size, temporarily used while packing
    ayu_count_t label_size = 0; ///< label size, temporarily set while packing

    msg_size =  get_message_size();
    label_size = m_task_label.size();

    Intern_event::pack(message, &msg_index, &msg_size, &label_size);

    std::char_traits<char>::copy(&message[msg_index],
                                 (char*) &m_task_id,
                                 ayu_sizeof_value);
    msg_index += ayu_sizeof_value;
    std::char_traits<char>::copy(&message[msg_index],
                                 (char*) &m_scope_client_id,
                                 ayu_sizeof_value);
    msg_index += ayu_sizeof_value;
    std::char_traits<char>::copy(&message[msg_index],
                                 (char*) &m_scope_id,
                                 ayu_sizeof_value);
    msg_index += ayu_sizeof_value;
    if (label_size != 0) {
        std::char_traits<char>::copy(&message[msg_index],
                                     m_task_label.c_str(),
                                     label_size);
        msg_index += label_size;
    }

    //LOG(INFO) << "PACK_2 event " << ayu_event_str[m_event] << " msg_size:" << msg_size
    //<< " label_size:" << label_size<< " client_id:" << m_client_id << " timestamp:" << m_timestamp
    //<< " task_id:" << m_task_id << " scope_id:" << m_scope_id
    //<< " task_label:" << m_task_label;

}

void Intern_event_task::print(ayu_printformat_t printformat,
                              std::ostream &os) {
    Intern_event::print(printformat, os);
    if (RAW == printformat) {
        os << m_task_id << " "<< m_scope_client_id << ":" << m_scope_id << " " << m_task_label << "\n";
    } else if(HUMAN == printformat) {
        os << "TaskID=" << m_task_id  << " ScopeID=" << m_scope_client_id <<":" << m_scope_id << " TaskLabel=" << m_task_label << "\n" ;
    } else if(DOT == printformat) {
        os << m_task_id << "\n";
    }else if(XML == printformat) {
        os << xml_seperator << m_task_id <<  xml_seperator << m_scope_client_id << xml_seperator << m_scope_id << xml_seperator << m_task_label << "\n";
    }

}




ayu_count_t Intern_event_task::get_message_size() const {
    ayu_count_t message_size = 0;
    message_size += Intern_event::get_message_size();
    message_size += 3 * ayu_sizeof_value; // m_task_id, m_scope_client_id, m_scope_id
    message_size +=  m_task_label.size() ; // m_task_label

    return message_size;
}
