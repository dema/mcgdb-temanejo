#include "intern_event/intern_request.hpp"
#include "ayudame_log.h"

Intern_request::Intern_request(ayu_client_id_t _client_id,
                      ayu_count_t _timestamp,
                      ayu_request_t _request,
                      std::string _key,
                      std::string _value)
  : Intern_event(_client_id, AYU_REQUEST, _timestamp),
    m_request(_request),
    m_key(_key),
    m_value(_value)
{
}



Intern_request::Intern_request(const char *message)
  :  m_request(AYU_REQUEST_NULL),
     m_key(""),
     m_value("")
{

    ayu_count_t msg_index = 0; ///< temprary index while packing the data
    ayu_count_t msg_size = 0; ///< message size, temporarily used while packing
    ayu_count_t label_size = 0; ///< label size, temporarily set while packing

    init(message, &msg_index, &msg_size, &label_size);


    std::char_traits<char>::copy((char*) &m_request, &message[msg_index], sizeof(ayu_request_t));
    msg_index += sizeof(ayu_request_t);

    if (label_size != 0) {

        ayu_count_t msg_size_part_one;
        std::char_traits<char>::copy((char*) &msg_size_part_one, &message[msg_index], ayu_sizeof_value);
        msg_index += ayu_sizeof_value;

        // add_request:   key
        char tmp[msg_size_part_one];
        std::char_traits<char>::copy(tmp, &message[msg_index], msg_size_part_one);
        tmp[msg_size_part_one] = '\0'; // append null-character
        msg_index += msg_size_part_one;
        m_key=std::string(tmp);

        std::size_t msg_size_part_two;
        msg_size_part_two = label_size-msg_size_part_one-ayu_sizeof_value;

        // add_request:   value
        char tmp2[msg_size_part_two];
        std::char_traits<char>::copy(tmp2, &message[msg_index], msg_size_part_two);
        tmp2[msg_size_part_two] = '\0'; // append null-character
        msg_index += msg_size_part_two;
        m_value=std::string(tmp2);
    }

}

void Intern_request::pack(char* message) {

    ayu_count_t msg_index = 0; ///< temprary index while packing the data
    ayu_count_t msg_size = 0; ///< message size, temporarily used while packing
    ayu_count_t label_size = 0; ///< label size, temporarily set while packing

    msg_size =  get_message_size();
    label_size = ayu_sizeof_value + m_key.size() + m_value.size();

    Intern_event::pack(message, &msg_index, &msg_size, &label_size);

    std::char_traits<char>::copy(&message[msg_index],(char*) &m_request, sizeof(ayu_request_t));
    msg_index += sizeof(ayu_request_t);


    if (label_size != 0) {
        std::size_t key_size = m_key.size();
        std::char_traits<char>::copy(&message[msg_index],(char*) &key_size, ayu_sizeof_value);
        msg_index += ayu_sizeof_value;

        std::char_traits<char>::copy(&message[msg_index], m_key.c_str(), m_key.size());
        msg_index +=  m_key.size();

        std::char_traits<char>::copy(&message[msg_index], m_value.c_str(), m_value.size());
        msg_index += m_value.size();
    }
}




void Intern_request::print(ayu_printformat_t printformat,
                                        std::ostream &os) {

    Intern_event::print(printformat, os);
    if (RAW == printformat) {
        os << m_request << " " << m_key << " " << m_value << "\n";
    } else if(HUMAN == printformat) {
        os << "Request=" << ayu_request_str[m_request] << " Key=" << m_key << " Value="  << m_value << "\n";
    } else if(DOT == printformat) {
        // TODO
    }
}




ayu_count_t Intern_request::get_message_size() const {
    ayu_count_t message_size = 0;
    message_size += Intern_event::get_message_size();
    message_size += sizeof(ayu_request_t); // sizeof(ayu_request_t)
    message_size += ayu_sizeof_value; //message part one size
    message_size += m_key.size(); // m_key
    message_size += m_value.size() ; // m_value
    return message_size;
}
