#include "client_event_handler/client_event_manager.hpp"
#include "ayudame_log.h"
#include "ayudame_lib.hpp"

Client_event_manager::Client_event_manager(Ayudame& ayu)
    : m_ayu(ayu),
      m_client_event_handlers(),
      shutdown_counter(0)
{
    LOG(DEBUG) << "Client_event_manager::Client_event_manager()";
}

Client_event_manager::~Client_event_manager()
{
    LOG(DEBUG) << "Client_event_manager::~Client_event_manager()";

}

void Client_event_manager::shutdown()
{
    shutdown_counter++;
    //assert(shutdown_counter <= m_client_event_handlers.size());
    if (shutdown_counter == m_client_event_handlers.size()) {
        for (auto& client_event_handler : m_client_event_handlers) {
            client_event_handler.second->shutdown();
        }
        m_client_event_handlers.clear();
        m_ayu.shutdown();

    }
}

void Client_event_manager::force_shutdown()
{
    for (auto& client_event_handler : m_client_event_handlers) {
        client_event_handler.second->shutdown();
    }
    m_client_event_handlers.clear();
    m_ayu.shutdown();
}

// TODO get_client_handler should only get the handler.
std::shared_ptr <Client_event_handler> Client_event_manager::get_client_handler(const ayu_client_t client)
{
    // TODO this should be called from Ayudame::get_client_handler (?)
    if (m_client_event_handlers.find(client) == m_client_event_handlers.end()) {
        std::shared_ptr <Client_event_handler> client_handler = create_client_handler(client);
        // TODO check return value of insert!!!
        m_client_event_handlers.insert(std::make_pair(client, client_handler));
    }

    return m_client_event_handlers[client];
}

std::shared_ptr <Client_event_handler> Client_event_manager::create_client_handler(const ayu_client_t client)
{
    switch (client) {
    case AYU_CLIENT_GENERIC:
        case AYU_CLIENT_GOMP:
        case AYU_CLIENT_CILK:
        return std::shared_ptr <Client_event_handler>
        (new Client_event_handler_generic(*this, m_ayu, AYU_CLIENT_CILK));
    case AYU_CLIENT_CPPSS:
        return std::shared_ptr <Client_event_handler>
        (new Client_event_handler_cppss(*this, m_ayu, AYU_CLIENT_CPPSS));
    case AYU_CLIENT_SMPSS:
        return std::shared_ptr <Client_event_handler>
        (new Client_event_handler_smpss(*this, m_ayu, AYU_CLIENT_SMPSS));
    case AYU_CLIENT_OMPSS:
        return std::shared_ptr <Client_event_handler>
        (new Client_event_handler_ompss(*this, m_ayu, AYU_CLIENT_OMPSS));
    case AYU_CLIENT_STARPU:
        return std::shared_ptr <Client_event_handler>
        (new Client_event_handler_starpu(*this, m_ayu, AYU_CLIENT_STARPU));
    case AYU_CLIENT_RESERVED:
        return std::shared_ptr <Client_event_handler>
        (new Client_event_handler_test(*this, m_ayu, AYU_CLIENT_RESERVED));
    case AYU_CLIENT_MPI:
        #ifdef AYU_MPI
        return std::shared_ptr <Client_event_handler>
        (new Client_event_handler_mpi(*this, m_ayu, AYU_CLIENT_MPI));
        #else
        LOG(ERROR) << "NO MPI SUPPORT ENABLED";
        #endif
    default:
        LOG(ERROR) << "default case reached in create_client_handler";
        break;
    }

    return std::shared_ptr <Client_event_handler>
    (new Client_event_handler_generic(*this, m_ayu, AYU_CLIENT_GENERIC));
}

void Client_event_manager::msg_send_out(std::shared_ptr <Intern_event> request)
{

    auto it_event_handler = m_client_event_handlers.find(AYU_CLIENT_MPI);
    // MPI present
    if (it_event_handler != m_client_event_handlers.end()) {
        auto event_handler = it_event_handler->second;
        ayu_id_t id = event_handler->get_m_id();
        // we are on the right node and the right process
        if (get_master_id(request->get_client_id()) == get_master_id(id) &&
            get_proc_id(request->get_client_id()) == get_proc_id(id)) {
            // send to client requested by request ;)
            it_event_handler = m_client_event_handlers.find((ayu_client_t) get_client_id(request->get_client_id()));

            if (it_event_handler != m_client_event_handlers.end()) {
                it_event_handler->second->msg_send_out(request);
            } else {
                LOG(ERROR) << "cannot find " << ayu_client_str[get_client_id(request->get_client_id())] << ":" << get_client_id(request->get_client_id()) << " in request handlers";
            }

            // we are not on the right side of the moon, let MPI handler handle this .../'''
        } else {
            event_handler->msg_send_out(request);
        }

        // MPI not present
    } else {
        it_event_handler = m_client_event_handlers.find((ayu_client_t) get_client_id(request->get_client_id()));
        if (it_event_handler != m_client_event_handlers.end()) {
            it_event_handler->second->msg_send_out(request);
        } else {
            LOG(ERROR) << "cannot find " << ayu_client_str[get_client_id(request->get_client_id())] << " in request handlers";
        }

    }

}

bool Client_event_manager::get_nesting_level(std::shared_ptr <Intern_event_task> data)
{

    for (auto& client_event_handler : m_client_event_handlers) {
        if (client_event_handler.first == AYU_CLIENT_OMPSS) {
            if (client_event_handler.second->get_m_id() != data->get_client_id()) {
                data->set_scope_id(client_event_handler.second->m_get_wd_id_function());
                data->set_scope_client_id(client_event_handler.second->get_m_id());
                return true;
            }

        }
    }
    return false;
}

