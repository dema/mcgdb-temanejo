#include "client_event_handler/client_event_handler_mpi.hpp"
#include "client_event_handler/client_event_matcher_mpi.hpp"
#include "ayudame_lib.hpp"
#include "ayudame_log.h"

#include <sys/time.h>
#include <sys/unistd.h>

Client_event_handler_mpi::Client_event_handler_mpi(Client_event_manager& manager, Ayudame& ayu, ayu_client_t client)
    : Client_event_handler(manager, ayu, client),
      m_server_socket(nullptr),
      m_master(false),
      m_super(false),
      m_hostname_for_rank( { }),
      m_my_hostname(""),
      m_mutex_user_data_MPI(),
      m_read_sockets_thread(nullptr),
      m_active_read_sockets_thread(true),
      m_matcher(nullptr)
{
    //LOG(INFO)<< "Client_event_handler_mpi";
    auto port_string = m_ayu.get_config().get_string("connect.ayu_port");
    int port = std::stoul(port_string);

    // THIS has to be done by all because there is an mpi all to all inside get_hostnames()
    // MPI_instrumentation takes care of memory management
    char *hostnames_buf;
    int len_of_host, num_of_ranks;
    get_hostnames(&len_of_host, &num_of_ranks, &hostnames_buf);

    char tmp_hostname[128];
    gethostname(tmp_hostname, sizeof(tmp_hostname));
    std::string my_hostname = std::string(tmp_hostname);
    LOG(INFO) << "my_hostname " << my_hostname;


    //------------------------------------------
    // copy the received hostname into m_hostname_for_rank
    m_hostname_for_rank.resize(num_of_ranks);
    int rank = 0;
    for (int i = 0; i < num_of_ranks * len_of_host; i += len_of_host) {
        m_hostname_for_rank[rank] = std::string(&hostnames_buf[i]);
        rank++;
    }

    free(hostnames_buf);
    //------------------------------------------

    //------------------------------------------
    // make list of all hosts
    std::vector <std::string> host_uniq(m_hostname_for_rank.size());
    std::copy(m_hostname_for_rank.begin(), m_hostname_for_rank.end(), host_uniq.begin());
    auto it = std::unique(host_uniq.begin(), host_uniq.end());
    host_uniq.resize(std::distance(host_uniq.begin(), it));
    //------------------------------------------

    //----------set master_id ------------------
    //------------------------------------------
    it = std::find(host_uniq.begin(), host_uniq.end(), my_hostname);
    int index = 0;
    if (it == host_uniq.end()) {
        LOG(ERROR) << "hostname not in host_uniq";
        return;
    } else {
        index = std::distance(host_uniq.begin(), it);
    }
    // sets the unique host id into the m_id
    set_master_id(&m_id, (uint64_t) index);
    //LOG(ERROR)<< "m_id:" << m_id << "index:" <<index ;
    //------------------------------------------

    //------------------------------------------
    // Connect_manager
    Connect_manager *connect_manager = m_ayu.get_connect_manager();
    //------------------------------------------

    //------------------------------------------
    // Building up the connections
    //------------------------------------------

    //------------------------------------------
    // Try to open a Server
    //------------------------------------------
    LOG(DEBUG) << "#Try to open a server on my host";
    m_server_socket = std::shared_ptr <Ayu_Socket>(new Ayu_Socket());
    m_server_socket->set_server_data(port - 1);
    int server_init_successful = m_server_socket->init();

    if (server_init_successful == -1) {
        //------------------------------------------
        // I am client on this node
        //------------------------------------------
        LOG(INFO) << "I am client on this node";

        m_server_socket = nullptr;
        LOG(INFO) << "Connect to localhost";
        connect_manager->init_connect(AYU_CONNECT_SOCKET);
        Connect_handler_socket *connect_handler_socket = static_cast <Connect_handler_socket*>(connect_manager->get_connect(AYU_CONNECT_SOCKET));
        if (connect_handler_socket->init_client(port - 1, "localhost") == -1) {
            LOG(ERROR) << "Connect to localhost FAILED";
        }

        //send init event proc id
        //auto d =Intern_event(m_id,AYU_EVENT_NULL,m_ayu.get_timestamp());
        std::shared_ptr <Intern_event> data(new Intern_event(m_id, AYU_EVENT_NULL, m_ayu.get_timestamp()));
        connect_handler_socket->msg_send_out(data);

    } else {
        //------------------------------------------
        // I am master on this node
        //------------------------------------------
        LOG(INFO) << "I am master on this node";
        m_master = true;

        //---------set hostnames in server-----------
        m_server_socket->set_host_uniq(host_uniq);
        //------------------------------------------

        //---------set masterid in server-----------
        m_server_socket->set_master_id(m_id);
        //------------------------------------------

        //---------set hostname in server-----------
        m_server_socket->set_my_hostname(my_hostname);
        //------------------------------------------

        if (m_hostname_for_rank[0] == my_hostname) {
            m_super = true;
        }

        if (!m_super) {

            int k_index = index - MAX_REMOTE_CHANNELS_PER_NODE + 1; // ??
            int k_index_to = (MAX_REMOTE_CHANNELS_PER_NODE + k_index - 2) / MAX_REMOTE_CHANNELS_PER_NODE;

            std::string host_to = host_uniq[k_index_to];

            LOG(INFO) << "Connect to " << host_to;
            connect_manager->init_connect(AYU_CONNECT_SOCKET);
            Connect_handler_socket *connect_handler_socket = static_cast <Connect_handler_socket*>(connect_manager->get_connect(AYU_CONNECT_SOCKET));
            int client_init_successful = connect_handler_socket->init_client(port - 1, host_to);
            if (client_init_successful != 0) {
                LOG(ERROR) << "intialising client not successful!";
            }
        } else {
            LOG(INFO) << "I am super";
        }

    }

    //------------------------------------------
    // Deactivates all connect handler for rank != 0
    // and activates connect_handler_socket
    //------------------------------------------
    set_connect_handlers_status();

    //------------------------------------------
    // if I'm rank 0 Matcher is generated
    //------------------------------------------
    if (m_super) {
        m_matcher = std::shared_ptr <Matcher>(new Matcher(this));
        m_ayu.init_connect_manager();
    }

    //------------------------------------------
    // creating loop running over all sockets
    //------------------------------------------
    create_sockets_loop();

    m_ayu.set_connection_tree_true();

}

void Client_event_handler_mpi::create_sockets_loop()
{
    // TODO: Is this a rough hack?
    if (m_server_socket != nullptr) {
        m_active_read_sockets_thread.store(true);
        m_read_sockets_thread = std::make_shared <std::thread>
                                (&Client_event_handler_mpi::read_sockets_loop, this);
        if (m_read_sockets_thread == nullptr) {
            LOG(ERROR) << "m_read_sockets_thread== nullptr";
        }
    }
}

Client_event_handler_mpi::~Client_event_handler_mpi()
{
    LOG(DEBUG) << "Client_event_handler_mpi::~Client_event_handler_mpi()";
}

void Client_event_handler_mpi::shutdown()
{
    //LOG(DEBUG) << " Client_event_handler_mpi::shutdown()";

    if (m_matcher != nullptr) {
        while (m_matcher->get_size_pending() != 0) {
            LOG(WARNING) << "m_matcher->get_size_pending()" << m_matcher->get_size_pending();
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
    }

    if (m_master) {
        m_server_socket->shutdown_socket();
    }

    //check if matcher is finished

    m_active_read_sockets_thread.store(false);
    while (m_read_sockets_thread != nullptr) {
        if (m_read_sockets_thread->joinable()) {
            m_read_sockets_thread->join();
            m_read_sockets_thread = nullptr;
        } else {
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
            LOG(ERROR) << "Cant join socket thread";
        }
    }

}

void Client_event_handler_mpi::read_sockets_loop()
{

    bool data_received = false;
    ayu_count_t no_data_received_counter = 0;

    while (m_active_read_sockets_thread.load() == true) {

        data_received = false;
        std::shared_ptr <Intern_event> data = m_server_socket->socket_read();
        if (data != nullptr) {
            event(data);
            //LOG(DEBUG)<< "DATA  RECEIVED " << ayu_event_str[data->get_event()];
            data_received = true;
        } else {
            //LOG_IF( m_active_read_sockets_thread.load(),ERROR) << "RECEIVED NULLPTR ";
        }
        // if no data was sent by the sockets, try to read again.
        // if this happens several times, sleep for a while.

        if (data_received == false) {
            ++no_data_received_counter;
            if (no_data_received_counter > 100) {
                std::chrono::milliseconds sleep_duration(1);
                std::this_thread::sleep_for(sleep_duration);
                no_data_received_counter = 0;
            }
            continue;
        }
    }

}

void Client_event_handler_mpi::event(const ayu_event_t _event,
                                     ayu_event_data_t _data)
{

    if (_event == AYU_FINISH) {
        m_manager.shutdown();
        return;
    }
    _data.common.client_id = m_id;
    _data.common.timestamp = m_ayu.get_timestamp();

    std::shared_ptr <Intern_event> data(Intern_event::create_intern_event(_event, _data));

    //nesting test code for omps

    if (data->get_event() == AYU_ADDTASK) {
        std::shared_ptr <Intern_event_task> task = Intern_event::castToEventTask(data);
        if (task->get_scope_id() == 0) {
            bool nesting_found = m_manager.get_nesting_level(task);
            if (nesting_found == false) {
                LOG(INFO) << "No nesting level found";
            }
            //LOG(ERROR) << task->get_scope_client_id() <<":" << task->get_scope_id()  << "TADA";
        }
    }
    //nesting test code for omps

    event(data);
}

void Client_event_handler_mpi::event(std::shared_ptr <Intern_event> data)
{
    if (m_super) {
        if (data->get_event() == AYU_USERDATA) {
            std::lock_guard <std::mutex> local_mutex(m_mutex_user_data_MPI);
            bool message_accepted = m_matcher->update_buffer(data);
            if (message_accepted) {
                m_matcher->update();
                return;
            }
        }
    }
    m_ayu.event(data);
}

/*
 *
 */
void Client_event_handler_mpi::set_connect_handlers_status()
{
    if (!m_super) {
        //------------------------------------------
        // Connect_manager
        Connect_manager *connect_manager = m_ayu.get_connect_manager();
        //------------------------------------------

        connect_manager->deactivate_all_connect();
        connect_manager->activate_connect(AYU_CONNECT_SOCKET);
    }
}

void Client_event_handler_mpi::msg_send_out(std::shared_ptr <Intern_event> request)
{

    if (get_master_id(request->get_client_id()) == get_master_id(m_id) &&
        get_proc_id(request->get_client_id()) == get_proc_id(m_id)) {
        Client_event_handler::msg_send_out(request);
    } else {
        if (m_server_socket != nullptr) {
            m_server_socket->socket_write(request);
        }
    }

}
