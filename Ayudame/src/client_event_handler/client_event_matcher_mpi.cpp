#include "client_event_handler/client_event_matcher_mpi.hpp"
#include "ayudame_lib.hpp"
#include "ayudame_log.h"

#include<stdio.h>
#include<iostream>

using namespace std;
static const char* MPI_MATCHER_PREFIX="MPI_MATCHER_PREFIX";

Matcher::Matcher(Client_event_handler * client)
        : m_size_pending(0),
          m_client( client ),
        m_envelope(),
        m_buffer(NULL),
        pending_mpi(2, std::vector< envelope >()),
m_unique_dep_id(UINT64_MAX) {
    LOG(INFO)<< "Matcher::Matcher";
}

std::vector<std::string> Matcher::split(std::string str, char delimiter) {
  std::vector<std::string> internal;
  std::stringstream ss(str); // Turn the string into a stream.
  std::string tok;

  while(getline(ss, tok, delimiter)) {
    internal.push_back(tok);
  }

  return internal;
}

bool Matcher::update_buffer(std::shared_ptr<Intern_event>  data) {

    std:string message = ((Intern_event_userdata *)data.get())->get_data();
    std::vector<std::string> split_message = split(message,'#');

    if (split_message[0].compare("MPI") != 0){
        LOG(ERROR) << "userdata is no MPI data";
        return false;
    }

    //LOG(INFO)<< "Matcher::update_buffer";
    /*
    m_envelope.rank = *((uint64_t*) &a[0]);
    m_envelope.task_id = *((uint64_t*) &a[8]);
    m_envelope.partner = *((uint64_t*) &a[16]);
    m_envelope.tag = *((uint64_t*) &a[24]);
    m_envelope.comm = *((uint64_t*) &a[32]);
    m_envelope.routine = *((uint64_t*) &a[40]);
    m_envelope.client_id = data->get_client_id();
    */
    m_envelope.rank =  atoi(split_message[1].c_str());
    m_envelope.task_id =  atoi(split_message[2].c_str());
    m_envelope.partner =  atoi(split_message[3].c_str());
    m_envelope.tag =  atoi(split_message[4].c_str());
    m_envelope.comm =  atoi(split_message[5].c_str());
    m_envelope.routine =  atoi(split_message[6].c_str());
    m_envelope.client_id = data->get_client_id();

/*
    LOG(ERROR) << "Update buffer taskid "<<m_envelope.task_id
    <<" partner " << m_envelope.partner
    << " tag "<< m_envelope.tag
    << " routine "<< m_envelope.routine
    << " rank "<< m_envelope.rank <<endl;
    */
    return true;
}

void Matcher::update() {
    //LOG(INFO)<< "Matcher::update";
    unsigned int i;

    /* DO complement_call() */
    unsigned int t;
    if ( m_envelope.routine == 0) {
        t = 1;
    } else {
        t = 0;
    }

    /*************************/
    if ( pending_mpi[t].size() > 0) {

        for(i = 0; i < pending_mpi[t].size(); i++) {
            if (pending_mpi[t][i].tag == m_envelope.tag &&
                    pending_mpi[t][i].partner == (int) m_envelope.rank ) {

                /*Add dep */
                /* C interface */
                //#ifdef 0
                /*
                              	auto ayu = get_ayudame_ptr(AYU_CLIENT_MPI);
                              	ayu_label_t dependency_label = (ayu_label_t) "matcher_work";
                              	ayu_id_t dependency_id = 1;
                                ayu_id_t from_id = pending_mpi[t][i].task_id;
                                ayu_id_t to_id = m_envelope.task_id;
                                ayu->add_dependency(dependency_id, from_id, to_id, dependency_label);
                */
                //#endif

                /* NEW interface*/

                ayu_label_t dependency_label = (ayu_label_t) "MPI_COMMUNICATION";
                ayu_id_t dependency_id = m_unique_dep_id;
                m_unique_dep_id--;
                ayu_id_t from_id = pending_mpi[t][i].task_id;
                ayu_id_t from_client_id = pending_mpi[t][i].client_id;
                //cout << " NASAO DEP " << pending_mpi[t][i].task_id <<endl;
                ayu_id_t to_id = m_envelope.task_id;
                ayu_id_t to_client_id = m_envelope.client_id;
                if (t == 0) {
                    //m_client->add_dependency(dependency_id, from_id, to_id, dependency_label);
                    m_client->add_dependency_matched(dependency_id, from_client_id, from_id, to_client_id, to_id, dependency_label);
                    m_client->set_property_matched(from_client_id, dependency_id,"dep_type","matched");
                } else {
                    //m_client->add_dependency(dependency_id, to_id, from_id, dependency_label);
                    m_client->add_dependency_matched(dependency_id, to_client_id, to_id, from_client_id, from_id, dependency_label);
                    m_client->set_property_matched(to_client_id, dependency_id,"dep_type","matched");
                }
                /*delete pending comm*/
                pending_mpi[t].erase( pending_mpi[t].begin() + i);
                m_size_pending--;
                return;
            }





        }
        //LOG(INFO)<< "INSIDE PUSH BACK taskid "<<m_envelope.task_id <<" partner " << m_envelope.partner << " tag "<< m_envelope.tag << " routine "<< m_envelope.routine << endl;
        pending_mpi[ m_envelope.routine ].push_back(m_envelope);
        m_size_pending++;
    } else {
        //LOG(INFO)<< "PUSH BACK taskid "<<m_envelope.task_id <<" partner " << m_envelope.partner << " tag "<< m_envelope.tag << " routine "<< m_envelope.routine << endl;
        pending_mpi[ m_envelope.routine ].push_back(m_envelope);
        m_size_pending++;
    }

}


