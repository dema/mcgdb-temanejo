#include "connect_handler/connect_handler_dot.hpp"
#include "ayudame_log.h"

Connect_handler_dot::Connect_handler_dot(Connect_manager& manager,
                                         Ayudame& ayu,
                                         std::string dot_filename)
    : Connect_handler(manager, ayu),
      m_filename(dot_filename),
      m_dot_file()
{
    try {
        m_dot_file.open(m_filename);
        m_dot_file << "digraph {\n";
    } catch (std::ofstream::failure e) {
        LOG(ERROR) << e.what();
    }
}

Connect_handler_dot::~Connect_handler_dot()
{

}

void Connect_handler_dot::shutdown(){
    try {
        if (m_dot_file.is_open()) {
        m_dot_file << "}\n";
            m_dot_file.close();
        }
    } catch (std::ofstream::failure e) {
        LOG(ERROR) << e.what();
    }
}

void Connect_handler_dot::msg_send_out(std::shared_ptr<Intern_event> data)
{
  data->print(DOT, m_dot_file);
}




const std::shared_ptr<Intern_event> Connect_handler_dot::msg_read(){
    LOG(ERROR) << "NO implementation availible for dot";
    return nullptr;
}
