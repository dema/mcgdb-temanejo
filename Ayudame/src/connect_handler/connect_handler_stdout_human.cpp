#include "connect_handler/connect_handler_stdout_human.hpp"

void Connect_handler_stdout_human::msg_send_out(std::shared_ptr<Intern_event> data)
{

    data->print(HUMAN);
}

const std::shared_ptr<Intern_event> Connect_handler_stdout_human::msg_read(){
    LOG(ERROR) << "NO implementation availible for stdout";
    return nullptr;
}
