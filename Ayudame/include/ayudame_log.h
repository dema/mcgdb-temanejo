/**
 * \file ayudame_log.h
 * \brief Header providing logging for Ayudame
 *
 * This file provides infrastructure for logging at different logging levels
 * in Ayudame. It either includes easylogging++ (http://easylogging.org/) or
 * provide the class Log and the convenience macro LOG(), roughly based on
 * code found on http://www.drdobbs.com/cpp/logging-in-c/201804215.
 *
 * \todo
 * - enable different logging level for file and stderr
 * - check whether file lock with lockf() is necessary
 *
 * \remarks
 * - this class is best to be included in the implementation files
 * - may not be multi{thread,process}-safe
 *
 * \author Steffen Brinkmann <brinkmann@hlrs.de>
 * \author Mathias Nachtmann <nachtmann@hlrs.de>
 *
 * \copyright
 * (C) 2015 HLRS, University of Stuttgart.\n
 *    This software  is published under the terms of the BSD license.
 *    See the LICENSE file for details.
 */


#ifndef __AYUDAME_LOG_H__
#define __AYUDAME_LOG_H__

/**
 * Sets the default file name for logfile
 */
#ifndef AYULOG_DEFAULT_FILE
#define AYULOG_DEFAULT_FILE "ayulog_default.log"
#endif // AYULOG_DEFAULT_FILE


#ifndef AYULOG
/**
 * Comment this out to use easylogging++ instead of AyuLog
 */
#define AYULOG
#endif //AYULOG

#ifndef AYULOG_MAX_LEVEL
/**
 * the maximum logging level at compile time
 */
#define AYULOG_MAX_LEVEL DEBUG4
#endif

#ifndef AYULOG

//#define _ELPP_DISABLE_LOGS
//#define  _ELPP_DEFAULT_LOG_FILE = "logs/AYULOG.log"
//#define _ELPP_NO_DEFAULT_LOG_FILE
//#define _ELPP_THREAD_SAFE
//#define _ELPP_STACKTRACE_ON_CRASH

#include "easylogging++.h"

#else // until end of file

#include <sstream>
#include <string>
#include <stdio.h>

inline std::string NowTime();

/**
 * \brief provide a fix set of logging levels
 *
 * \todo get rid of DEBUG due to possible confusion with macro
 */
enum AyuLogLevel {FATAL=0, ///< report only fatal error messages, which lead to forced exit
                  ERROR, ///< report only error messages, usually Ayudame cannot work properly if an error occurs
                  WARNING, ///< report warnings and errors, warning mean , there <i>could</i> be a problem
                  INFO, ///< report more detailed information, warnings and errors
                  DEBUG, ///< report very detailed messages for debugging Ayudame and all of the above
                  DEBUG1, ///< currently not in use
                  DEBUG2, ///< currently not in use
                  DEBUG3, ///< currently not in use
                  DEBUG4 ///< currently not in use
                  };

/**
 * \brief Class providing logging to file descriptor
 *
 * This class will generate output to file. It should only be used only by
 * class AyuLog.
 *
 * */
class Output2FILE
{
public:
    /**
     * \brief return a (non-const) reference to the file pointer in use
     *
     * Stream() holds a ststic member containing the file handle, defaulting to
     * stderr. It returns a non-const reference to the file pointer in use, which
     * can be changed by:
     * \code
     * Output2FILE::Stream() = fopen("file_name.log", "w");
     * \endcode
     * Opening, closing and validation checking of the file pointer must be done
     * outside this function.
     *
     * \return non-const reference to the file pointer in use
     *
     * \see AyuLog::LogFileName(const std::string& file_name)
     */
    static FILE*& Stream();

    /*
     * \brief output the message to the file
     *
     * Output(const std::string& msg) gets the file handle from Stream(), checks
     * whether the file handle is not NULL and valid and writes the message
     * to the file. Finally it flushes the file buffer.
     *
     * \param [in] msg Message to log.
     *
     * \remarks The file is not locked, so this function is not necessarily
     *
     * multithread or multiprocess-safe.
     * \see Stream()
     */
    static void Output(const std::string& msg);
};

inline FILE*& Output2FILE::Stream()
{
    static FILE* pStream = stderr;
    return pStream;
}

inline void Output2FILE::Output(const std::string& msg)
{
    FILE* pStream = Stream();
    if (!pStream && ftell(pStream) != -1L)
        return;
    fprintf(pStream, "%s", msg.c_str());
    fflush(pStream);
}

/**
 * \brief Class providing logging to stderr
 *
 * This class will generate output to stderr. It should only be used only by
 * class AyuLog.
 *
 * */
class Output2stderr
{
public:
     /*
     * \brief output the message to stderr
     *
     * Output(const std::string& msg) writes the message
     * to stderr and flushes the buffer.
     *
     * \param [in] msg Message to log.
     *
     * \remarks The output is not locked, so this function is not necessarily
     * multithread or multiprocess-safe.
     */
    static void Output(const std::string& msg);
};

inline void Output2stderr::Output(const std::string& msg)
{
    fprintf(stderr, "%s", msg.c_str());
    fflush(stderr);
}

/**
 * \brief Logging for Ayudame
 *
 * This class privides the logging infrastructure used by Ayudame. An instance
 * is created every time, a log message is issued. On destruction, it prints
 * the message to a file provided by class Output2FILE and to stderr, provided
 * by Output2stderr.
 *
 * \see Output2FILE, Output2stderr,
 * http://www.drdobbs.com/cpp/logging-in-c/201804215
 */
class AyuLog
{
public:
    /** \brief Default and only constructor
     *
     * The default constructor is the only available constructor. As there are
     * no member variables which need initialising, its body is empty.
     */
    AyuLog();
    /// Deleted copy constructor
    AyuLog(const AyuLog&) = delete;
    /// Deleted assignment operator
    AyuLog& operator =(const AyuLog&) = delete;

    /// Virtual destructor
    virtual ~AyuLog();

    /**
     * \brief Return a stream for logging messages
     *
     * This is the function which will returns a non-const reference to the
     * stream object which bufferes the log message. For convenience it is
     * used as the base class for class Log and wrapped in the macro LOG().
     *
     * Call:
     * \code
     * AyuLog().Get(WARNING) << "Warning message";
     * LOG(INFO) << "Info message"; // using the convenience macro
     * \endcode
     *
     * \param [in] level logging level, one of enum AyuLogLevel.
     * \return non-const reference to stream object for logging messages
     *
     * \see Log, LOG(), AyuLogLevel
     */
    std::ostringstream& Get(AyuLogLevel level = INFO);

    /**
     * \brief Get and set the reporting level
     *
     * \code
     * AyuLog::ReportingLevel() = INFO;
     * std::cout << "The reporting level is" << AyuLog::ToString(AyuLog::ReportingLevel() << std::endl;
     * \endcode
     *
     * \return non-const reference to the current logging level
     *
     * \remarks colourful output works on terminals capable of interpreting
     * the ANSI colour escape codes
     */
    static AyuLogLevel& ReportingLevel();

    /**
     * \brief Convert logging level to string
     *
     * \param [in] level logging level, one of enum AyuLogLevel.
     * \return string representation of the passed logging level
     */
    static std::string ToString(AyuLogLevel level);

    /**
     * \brief Get logging level from string
     *
     *
     * \param [in] level logging level, one of enum AyuLogLevel as string.
     * \return logging level represented by the passed string
     */
    static AyuLogLevel FromString(const std::string& level);

    /**
     * \brief Get and set file name for logfile
     *
     * LogFileName(const std::string& file_name = "") can be called in two ways:
     *
     * - without argument: It will simply return the file name of the currently
     *   used log file.
     * - with a string as an argument: open a file with given file name and
     *   start using it. If a file with another name was in use before that
     *   file is closed.
     *
     * \param [in] file_name logfile name.
     * \return non-const reference to a string object containing the current
     * logfile name in use
     *
     * \todo implement portable way of writing log files to subdirectories
     */
    static const std::string& LogFileName(const std::string& file_name = "");

    /**
     * \brief Close the log file
     *
     */
    static void CloseLogFile();

protected:
    /**
     * \brief Buffer for log messages
     *
     */

    std::ostringstream os;
};

inline AyuLog::AyuLog()
              : os()
{
}

inline std::ostringstream& AyuLog::Get(AyuLogLevel level)
{
    os << "\033[0mAYULOG - " << NowTime();
    os << " " << ToString(level) << ": ";
    os << std::string(level > INFO ? level - DEBUG : 0, '\t');
    os << "\033[0m";
    return os;
}

inline AyuLog::~AyuLog()
{
    os << std::endl;
    Output2FILE::Output(os.str());
    Output2stderr::Output(os.str());
}

inline AyuLogLevel& AyuLog::ReportingLevel()
{
    static AyuLogLevel reportingLevel = DEBUG4;
    return reportingLevel;
}


/*
\033[22;30m - black
\033[22;31m - red
\033[22;32m - green
\033[22;33m - brown
\033[22;34m - blue
\033[22;35m - magenta
\033[22;36m - cyan
\033[22;37m - gray
\033[01;30m - dark gray
\033[01;31m - light red
\033[01;32m - light green
\033[01;33m - yellow
\033[01;34m - light blue
\033[01;35m - light magenta
\033[01;36m - light cyan
\033[01;37m - white
*/
inline std::string AyuLog::ToString(AyuLogLevel level)
{
    static const char* const buffer[] = {"\033[01;31mFATAL",
                                         "\033[01;31mERROR",
                                         "\033[01;33mWARNING",
                                         "\033[0mINFO",
                                         "\033[01;34mDEBUG",
                                         "\033[01;35mDEBUG1",
                                         "\033[01;36mDEBUG2",
                                         "\033[0mDEBUG3",
                                         "\033[0mDEBUG4"};
    return buffer[level];
}

inline AyuLogLevel AyuLog::FromString(const std::string& level)
{
    if (level == "DEBUG4")
        return DEBUG4;
    if (level == "DEBUG3")
        return DEBUG3;
    if (level == "DEBUG2")
        return DEBUG2;
    if (level == "DEBUG1")
        return DEBUG1;
    if (level == "DEBUG")
        return DEBUG;
    if (level == "INFO")
        return INFO;
    if (level == "WARNING")
        return WARNING;
    if (level == "ERROR")
        return ERROR;
    if (level == "FATAL")
        return FATAL;
    AyuLog().Get(WARNING) << "Unknown logging level '" << level << "'. Using INFO level as default.";
    return INFO;
}

inline const std::string& AyuLog::LogFileName(const std::string& file_name)
{
    static std::string log_file_name = "";

    // if the function is called without arguments or the filename is the same
    // as in the previous call, it will just return the current file name
    if (file_name != "" || log_file_name == file_name) {

        // if a file name was set before (i.e. is other than "")
        // and that file is open, close the file
        if(log_file_name.compare("") != 0 &&
           ftell(Output2FILE::Stream()) != -1L) {
            fclose(Output2FILE::Stream());
            Output2FILE::Stream() = NULL;
        }

        // set the new file name
        log_file_name = file_name;

        //open file with new file name and tell Output2FILE
        Output2FILE::Stream() = fopen(log_file_name.c_str(), "w");
    }

    return log_file_name;
}

inline void AyuLog::CloseLogFile()
{
    if(Output2FILE::Stream() != NULL && ftell(Output2FILE::Stream()) != -1L) {
        fclose(Output2FILE::Stream());
        Output2FILE::Stream() = NULL;
    }
}


/**
 * \brief dummy namespace defining a dummy variable with the goal of executing
 * AyuLog::LogFileName(AYULOG_DEFAULT_FILE) once.
 *
 * This namespace and variable definition should not appear in the API
 * documentation
 */
namespace DUMMY{
    static std::string dummy = AyuLog::LogFileName(AYULOG_DEFAULT_FILE);
}


/**
 * \brief __declspec for windows
 *
 * \remarks Something windows seem to need. Are we ever going to compile on
 * windows?
 *
 *
 * */
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__)
#   if defined (BUILDING_FILELOG_DLL)
#       define FILELOG_DECLSPEC   __declspec (dllexport)
#   elif defined (USING_FILELOG_DLL)
#       define FILELOG_DECLSPEC   __declspec (dllimport)
#   else
#       define FILELOG_DECLSPEC
#   endif // BUILDING_DBSIMPLE_DLL
#else
#   define FILELOG_DECLSPEC
#endif // _WIN32


/**
 * \brief The class actually used for log messages
 *
 * Class Log is derived from Ayulog to wrap all its functionality.
 * This way several Loggers can be used in parallel if needed.
 */
class FILELOG_DECLSPEC Log : public AyuLog {};


/**
 * \brief Convenience macro for logging with class Log
 *
 * By defining this macro overhead is minimized if the logging level is low
 * (e.g. ERROR).
 * Usage:
 * \code
 * LOG(INFO) << "some information";
 * \endcode
 */
#define LOG(level) \
    if (level > AYULOG_MAX_LEVEL) ;\
    else if (level > Log::ReportingLevel() || !Output2FILE::Stream()) ; \
    else Log().Get(level)

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__)

#include <windows.h>

/**
 * \brief Get a timestring
 *
 * The function NowTime() is defined for windows and non-windows.
 *
 * \return a string representing the current wall clock time
 *
 * \remarks Is the windows version ever going to be used?
 */
inline std::string NowTime()
{
    const int MAX_LEN = 200;
    char buffer[MAX_LEN];
    if (GetTimeFormatA(LOCALE_USER_DEFAULT, 0, 0,
            "HH':'mm':'ss", buffer, MAX_LEN) == 0)
        return "Error in NowTime()";

    char result[100] = {0};
    static DWORD first = GetTickCount();
    std::sprintf(result, "%s.%03ld", buffer, (long)(GetTickCount() - first) % 1000);
    return result;
}

#else

#include <sys/time.h>


/**
 * \brief Get a timestring
 *
 * The function NowTime() is defined for windows and non-windows.
 *
 * \remarks Is the windows version ever going to be used?
 *
 */
inline std::string NowTime()
{
    char buffer[11];
    time_t t;
    time(&t);
    tm r = {0,0,0,0,0,0,0,0,0,0,0};
    strftime(buffer, sizeof(buffer), "%X", localtime_r(&t, &r));
    struct timeval tv;
    gettimeofday(&tv, 0);
    char result[100] = {0};
    std::sprintf(result, "%s.%03ld", buffer, (long)tv.tv_usec / 1000);
    return result;
}

#endif //WIN32

#endif //!AYULOG

#endif //__LOG_AYUDAME_H__
