/**
 * \file Connect_handler_xml.hpp
 * \brief header of the class Connect_handler_xml
 *
 * \author Mathias Nachtmann <nachtmann@hlrs.de>
 * \copyright
 * (C) 2015 HLRS, University of Stuttgart.\n
 *    This software  is published under the terms of the BSD license.
 *    See the LICENSE file for details.
 *
 */
#ifndef CONNECT_HANDLER_XML_HPP_
#define CONNECT_HANDLER_XML_HPP_

#include <string>
#include <fstream>
#include <memory>

#include "connect_handler/connect_handler.hpp"

class Connect_manager;

/**
 * \class Connect_handler_xml
 *  @copydoc Connect_handler
 * \brief Class Connect_handler_xml
 * */
class Connect_handler_xml: public Connect_handler {

public:

    /**
     * @copydoc Connect_handler::Connect_handler()
     * @param manager
     * @param ayu
     * @param xml_filename
     */
    Connect_handler_xml(Connect_manager& manager,
                        Ayudame& ayu,
                        std::string xml_filename);

    /**
     *
     */
    Connect_handler_xml() = delete;

    /**
     *
     */
    ~Connect_handler_xml();

    void shutdown();

    /**
     *
     * @param data
     */
    virtual void msg_send_out(std::shared_ptr<Intern_event> data);

    virtual const std::shared_ptr<Intern_event> msg_read();

private:

    /**
     *
     */
    std::string m_filename;

    /**
     *
     */
    std::ofstream m_xml_file;
};

#endif /* CONNECT_HANDLER_XML_HPP_ */
