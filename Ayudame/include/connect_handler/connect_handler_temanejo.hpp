/**
 * \file connect_handler_temanejo.hpp
 * \brief header of the class connect_handler_temanejo
 *
 * \author Mathias Nachtmann <nachtmann@hlrs.de>
 * \copyright
 * (C) 2015 HLRS, University of Stuttgart.\n
 *    This software  is published under the terms of the BSD license.
 *    See the LICENSE file for details.
 *
 */
#ifndef CONNECT_HANDLER_TEMANEJO_HPP_
#define CONNECT_HANDLER_TEMANEJO_HPP_

#include <map>
#include <string>
#include <memory>

#include "connect_handler/connect_handler.hpp"
#include "ayu_socket.hpp"

class Connect_manager;

/**
 * \class Connect_handler_temanejo
 *  @copydoc Connect_handler
 * \brief Class Connect_handler_temanejo
 * */
class Connect_handler_temanejo: public Connect_handler {

public:

    /**
     *
     * @param manager
     * @param ayu
     * @param port
     */
    Connect_handler_temanejo(Connect_manager& manager,
                                 Ayudame& ayu,
                                 unsigned port,
                                 std::string host);

    /**
     *
     */
    Connect_handler_temanejo() = delete;
    void shutdown();

    /**
     *
     * @param data
     */
    virtual void msg_send_out(std::shared_ptr<Intern_event> data);
    virtual const std::shared_ptr<Intern_event> msg_read();

private:

    /**
     *
     */
    Ayu_Socket m_client_socket;

};

#endif /* CONNECT_HANDLER_TEMANEJO_HPP_ */
