/**
 * \file Connect_handler_socket.hpp
 * \brief header of the class Connect_handler_socket
 *
 * \author Mathias Nachtmann <nachtmann@hlrs.de>
 * \copyright
 * (C) 2015 HLRS, University of Stuttgart.\n
 *    This software  is published under the terms of the BSD license.
 *    See the LICENSE file for details.
 *
 */
#ifndef CONNECT_HANDLER_SOCKET_HPP_
#define CONNECT_HANDLER_SOCKET_HPP_

#include <memory>

#include "connect_handler/connect_handler.hpp"
#include "ayu_socket.hpp"

class Connect_manager;

/**
 * \class Connect_handler_socket
 *  @copydoc Connect_handler
 * \brief Class Connect_handler_socket
 * */
class Connect_handler_socket: public Connect_handler {

public:

    /**
     *
     * @param manager
     * @param ayu
     */
    Connect_handler_socket(Connect_manager& manager, Ayudame& ayu)
        :Connect_handler(manager, ayu), m_client_socket()
    {
    }

    /**
     *
     * @param data
     */
    virtual void msg_send_out(std::shared_ptr<Intern_event> data);


    virtual const std::shared_ptr<Intern_event>  msg_read();


    /**
     *
     * @param port
     */
    virtual int init_client(int port, std::string host);

    /**
     *
     */
    virtual ~Connect_handler_socket();

    virtual void shutdown();


private:

    /**
     *
     */
    Ayu_Socket m_client_socket;
};

#endif /* CONNECT_HANDLER_SOCKET_HPP_ */
