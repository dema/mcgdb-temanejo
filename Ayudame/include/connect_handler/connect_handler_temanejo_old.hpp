/**
 * \file connect_handler_temanejo_old.hpp
 * \brief header of the class connect_handler_temanejo_old
 *
 * \author Mathias Nachtmann <nachtmann@hlrs.de>
 * \copyright
 * (C) 2015 HLRS, University of Stuttgart.\n
 *    This software  is published under the terms of the BSD license.
 *    See the LICENSE file for details.
 *
 */
#ifndef CONNECT_HANDLER_TEMANEJO_OLD_HPP_
#define CONNECT_HANDLER_TEMANEJO_OLD_HPP_


class Connect_manager;

#include "connect_handler/connect_handler.hpp"
#include "ayudame_types.h"
#include "ayu_socket.hpp"
#include "intern_event/intern_event.hpp"
#include "intern_event/intern_event_task.hpp"
#include "intern_event/intern_event_dependency.hpp"

#include <map>


/**
 * \class Connect_handler_temanejo_old
 *  @copydoc Connect_handler
 * \brief Class Connect_handler_temanejo_old
 * */
class Connect_handler_temanejo_old: public Connect_handler {

public:

    /**
     *
     * @param manager
     * @param ayu
     * @param port
     */
    Connect_handler_temanejo_old(Connect_manager& manager,
                                 Ayudame& ayu,
                                 unsigned port);

    /**
     *
     */
    Connect_handler_temanejo_old() = delete;
    void shutdown(){}

    /**
     *
     * @param data
     */
    virtual void msg_send_out(std::shared_ptr<Intern_event> data);
    virtual const std::shared_ptr<Intern_event> msg_read();

private:

    /**
     *
     * @param data
     */
    virtual void add_task(Intern_event_task *data);

    /**
     *
     * @param data
     */
    virtual void add_dependency(Intern_event_dependency *data);



    /**
     *
     * @param buf0
     * @param buf1
     * @param buf2
     * @param buf3
     * @param buf4
     * @param buf5
     * @param buf6
     * @param ts
     * @param string
     */
    virtual void pack_and_send(ayu_id_t buf0, ayu_id_t buf1, ayu_id_t buf2,
                               ayu_id_t buf3, ayu_id_t buf4, ayu_id_t buf5, ayu_id_t buf6,
                               ayu_id_t ts, const ayu_label_t string);



    /**
     *
     */
    std::map<std::string, int> function_names;

    /**
     *
     */
    const size_t AYU_string_buf_size = 128;

    /**
     *
     */
    const size_t AYU_buf_size = 8;

    /**
     *
     */
    Ayu_Socket m_server_socket;

    /**
     *
     */
    typedef enum
    {
        O_AYU_EVENT_NULL = 0,      //!< O_AYU_EVENT_NULL
        O_AYU_PREINIT = 1,         //!< O_AYU_PREINIT
        O_AYU_INIT = 2,            //!< O_AYU_INIT
        O_AYU_FINISH = 3,          //!< O_AYU_FINISH
        O_AYU_REGISTERFUNCTION = 4,//!< O_AYU_REGISTERFUNCTION
        O_AYU_ADDTASK = 5,         //!< O_AYU_ADDTASK
        O_AYU_ADDHIDDENTASK = 6,   //!< O_AYU_ADDHIDDENTASK
        O_AYU_ADDDEPENDENCY = 7,   //!< O_AYU_ADDDEPENDENCY
        O_AYU_ADDTASKTOQUEUE = 8,  //!< O_AYU_ADDTASKTOQUEUE
        O_AYU_PRESELECTTASK = 9,   //!< O_AYU_PRESELECTTASK
        O_AYU_PRERUNTASK = 10,     //!< O_AYU_PRERUNTASK
        O_AYU_RUNTASK = 11,        //!< O_AYU_RUNTASK
        O_AYU_POSTRUNTASK = 12,    //!< O_AYU_POSTRUNTASK
        O_AYU_RUNTASKFAILED = 13,  //!< O_AYU_RUNTASKFAILED
        O_AYU_REMOVETASK = 14,     //!< O_AYU_REMOVETASK
        O_AYU_WAITON = 15,         //!< O_AYU_WAITON
        O_AYU_BARRIER = 16,        //!< O_AYU_BARRIER
        O_AYU_ADDWAITONTASK = 17   //!< O_AYU_ADDWAITONTASK
    } o_ayu_event_t;

    /**
     * map new to old
     * TODO TO be completed
     */
    std::map <unsigned, unsigned> new_events_to_old = { { AYU_EVENT_NULL, O_AYU_EVENT_NULL },
            { AYU_INIT, O_AYU_PREINIT },
            { AYU_FINISH, O_AYU_FINISH },
            { AYU_START, O_AYU_INIT },
            { AYU_END, O_AYU_FINISH },
            { AYU_ADDTASK, O_AYU_ADDTASK },
            { AYU_ADDDEPENDENCY, O_AYU_ADDDEPENDENCY },
            { AYU_REGISTERPROPERTY, 0 },
            { AYU_REGISTERPROPERTYVALUE, 0 },
            { AYU_SETPROPERTY, 8 },
            { AYU_BARRIER, O_AYU_BARRIER },
            { AYU_ATTACH, 0 },
            { AYU_DETACH, 0 } };


};

#endif /* CONNECT_HANDLER_TEMANEJO_OLD_HPP_ */
