/**
 * \file connect_handler.hpp
 * \brief header of the class connect_handler
 *
 * \author Mathias Nachtmann <nachtmann@hlrs.de>
 * \copyright
 * (C) 2015 HLRS, University of Stuttgart.\n
 *    This software  is published under the terms of the BSD license.
 *    See the LICENSE file for details.
 *
 */
#ifndef AYUDAME_CONNECT_HANDLER_HPP
#define AYUDAME_CONNECT_HANDLER_HPP


#include "ayudame_log.h"
#include <memory>

#include "ayudame_types.h"
#include "intern_event/intern_event.hpp"
#include "intern_event/intern_event_task.hpp"
#include "intern_event/intern_event_dependency.hpp"

class Ayudame;
class Connect_manager;

/**
 * \class Connect_handler
 * \brief Class Connect_handler
 * */
class Connect_handler {

public:

    /**
     *
     * @param manager
     * @param ayu
     */
    Connect_handler(Connect_manager& manager, Ayudame& ayu);

    /**
     *
     */
    Connect_handler() = delete;

    /**
     *
     */
    virtual ~Connect_handler();

    virtual void shutdown() = 0;

    /**
     *
     * @param data
     */
    virtual void msg_send_out(std::shared_ptr<Intern_event> data) = 0;
    virtual const std::shared_ptr<Intern_event> msg_read() =0;


private:

protected:

    /**
     *
     */
    Ayudame& m_ayu;

    /**
     *
     */
    Connect_manager& m_manager;

};

#endif // CONNECT_HANDLER_HPP
