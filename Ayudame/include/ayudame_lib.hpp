/**
 * \file ayudame_lib.hpp
 * \brief Header of the class Ayudame and the namspace ayu
 *
 * \author Steffen Brinkmann <brinkmann@hlrs.de>
 * \author Mathias Nachtmann <nachtmann@hlrs.de>
 *
 * \copyright
 * (C) 2015 HLRS, University of Stuttgart.\n
 *    This software  is published under the terms of the BSD license.
 *    See the LICENSE file for details.
 *
 */

#ifndef AYUDAME_LIB_HPP
#define AYUDAME_LIB_HPP

//#include "ayudame.h"
#include "connect_handler/connect_manager.hpp"
#include "client_event_handler/client_event_manager.hpp"
#include "configuration.hpp"
#include "ayudame_types.h"
#include "intern_event/intern_event.hpp"
#include "ayudame_log.h"


#include <atomic>         // std::atomic, std::atomic_flag, ATOMIC_FLAG_INIT
#include <memory>
#define __STDC_LIMIT_MACROS
#include <cstdint>
#include <chrono>
#include <ctime>
#include <exception>
#include <string>
#include <sstream>

using chrono_clock = std::chrono::system_clock;

#ifndef AYULOG
namespace el {
class Configurations;
}
#endif // AYULOG

//#pragma GCC poison printf fprintf cout cerr

/**
 * \defgroup ayudame_core Core classes and functions of Ayudame
 *
 * These are the central objects of Ayudame.
 *
 * @{
 */

/**
 * \brief Central class of the Ayudame library.
 *
 * The class Ayudame will be created exactly once bei the factory function
 * ayu::dame().
 *
 *
 * */
class Ayudame {

public:

    /**
     * \brief Constructor of Ayudame
     *
     * Only valid constructor for Ayudame.
     *
     * \todo call manager connect and event ?
     */
    explicit Ayudame(std::string config_filename = "");

    /**
     * \brief Deleted copy constructor of Ayudame
     *
     * Ayudame is a singleton, therefore there will never be
     * the need to copy objects
     */
    Ayudame(const Ayudame& ayu) = delete;

    /**
     * \brief Deleted assignemnt operator
     *
     * Ayudame is a singleton, therefore there will never be
     * the need to copy objects
     *
     * \return
     */
    const Ayudame& operator=(const Ayudame& ayu) = delete;

    /**
    * \brief ayu::dame() is friend to Ayudame
    *
    * Only ayu::dame is allowed to create an instance of Ayudame.
    */
    //friend ayu::dame();

    /**
     * \brief Destructor of Ayudame
     *
     * \todo Desctructor call manager connect and event ?
     */
    virtual ~Ayudame();

    /**
     * \brief shutdown routine for ayudame
     * \param void
     * \return void
     */
    void shutdown();

    /**
     * \brief getter for specific client handler
     * \param client
     * \return int
     */
    int get_buffer_size();


    /**
     * \brief getter for specific client handler
     * \param [in] client type
     * \return a pointer to the client event handler object
     */
    Client_event_handler* get_client_handler(const ayu_client_t client);

    /**
     * \brief getter for Configuration / Connect_manager / Client_event_manager class
     * \return a constant reference to the configuration object
     */
    const Configuration& get_config() const;

    /**
     * \brief getter for the connect manager
     * \return a pointer to the connect manager object
     */
    Connect_manager* get_connect_manager();

    /**
     * \brief getter for the client event manager
     * \return a pointer to the client event manager object
     */
    Client_event_manager* get_client_event_manager();

    /**
     * \brief event handling
     * \param data
     */
    void event(std::shared_ptr<Intern_event> data);

    /**
     * \brief initialise and configure components
     */
    void init_logging();

#ifdef AYULOG
    /**
     * \brief
     *
     *
     * \todo fill Ayudame::set_default_logging_ayu()
     *
     */
    void set_default_logging_ayu();
#else
    /**
     * \brief setter for the easylogging++ configuration
     *
     * \param a reference to the easylogging++ configuration
     */
    void set_default_logging_el(el::Configurations &el_default_conf);
#endif //AYULOG

#ifdef AYULOG
    /**
     * \brief force reconfiguration of ayudame's logger
     */
    void reconfigure_logging_ayu();
#else
    /**
     * \brief force reconfiguration of the easylogging++ logger
     */
    void reconfigure_logging_el();
#endif // AYULOG

    /**
     * \brief initialise connect manager
     */
    void init_connect_manager();

    /**
     * \brief generate a timestamp
     * \return timestamp
     */
    uint64_t get_timestamp() const;

    /**
     * \brief setter for m_connection_tree
     */
    void set_connection_tree_true();

private:

    /**
     * \brief constructor
     * \param config_filename
     */
    //explicit Ayudame(std::string config_filename = "");

    /**
     * \brief copy constructor
     * \param ayu
     */
    //Ayudame(const Ayudame& ayu) = delete;

    /**
     * \brief configuration object
     */
    Configuration m_config;

    /**
     * \brief connect manager object
     */
    Connect_manager m_connect_manager;

    /**
     * \brief client event manager object
     */
    Client_event_manager m_client_event_manager;

    /**
     * \brief timestamp of start of Ayudame
     */
    const chrono_clock::time_point m_start_time;

    /**
     * \brief mutex for event processing
     */
    std::mutex m_mutex_event;

    /**
     * \brief MPI rank
     *
     * if MPI is present this is myrank, else it is 0
     */
    int my_rank;


    /**
     * \brief whether there is a communication tree
     *
     * will be false until connection tree was inalized
     */
    std::atomic<bool> m_connection_tree;


    /**
     * \brief event buffer
     *
     * \todo shouldn't this be a std:deque???
     */
    std::vector<std::shared_ptr <Intern_event>> m_buffer;

    /**
     * \brief create the thread for processing events in buffer
     */
    void create_buffer_loop();
    void create_request_socket_loop();

    /**
     * \brief thread for processing events in buffer
     */
    std::shared_ptr<std::thread> m_read_buffer_thread;
    std::shared_ptr<std::thread> m_request_socket_thread;

    /**
     * \brief whether thread for processing events in buffer is active
     */
    std::atomic<bool> m_active_read_buffer_thread;
    std::atomic<bool> m_active_request_socket_thread;

    /**
     * \brief loop method for reading from buffer
     */
    void read_buffer_loop();
    void request_socket_loop();

    /**
     * \brief process event from buffer
     *
     * \return true if there was an event, false otherwise
     */
    bool read_buffer();

    /**
     * \brief get next event in buffer
     *
     * \return shared pointer to event
     */
    std::shared_ptr<Intern_event> buffer_pop();

    /**
     * \brief put event in buffer
     *
     * \param shared pointer to event
     */
    void buffer_push(std::shared_ptr<Intern_event> data);

};

/**
 * \brief Namespace ayu
 * */
namespace ayu {
/**
 * \brief factory function for class Ayudame
 * \return reference to unique Ayudame object
 */
Ayudame& dame();
}
/**
 * @}
 */

#endif // AYUDAME_LIB_HPP
