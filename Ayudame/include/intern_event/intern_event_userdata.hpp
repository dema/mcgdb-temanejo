/**
 * \file intern_event_userdata.hpp
 * \brief Header of the internal event object for userdata
 *
 * \author Steffen Brinkmann <brinkmann@hlrs.de>
 * \author Mathias Nachtmann <nachtmann@hlrs.de>
 *
 * \copyright
 * (C) 2015 HLRS, University of Stuttgart.\n
 *    This software  is published under the terms of the BSD license.
 *    See the LICENSE file for details.
 *
 */
#ifndef INTERN_ADD_USERDATA_HPP_
#define INTERN_ADD_USERDATA_HPP_

#include <iostream>
#include <vector>

#include "intern_event/intern_event.hpp"
#include "ayudame_types.h"


/**
 * \class Intern_event_userdata
 * \brief Class Intern_event_userdata
 *
 * \ingroup intern_event
 *
 */
class Intern_event_userdata: public Intern_event {

public:

    /**
     * \brief no creation withou initialisation (RAII)
     */
    Intern_event_userdata() = delete;

    /**
     * \brief ctor with information passed one by one
     *     *
     * \param _client_id
     * \param _event
     * \param _timestamp
     * \param _size
     * \param _data
     */
    Intern_event_userdata(ayu_client_id_t _client_id,
                                  ayu_event_t _event,
                                  ayu_count_t _timestamp,
                                  std::string _data);

    /**
     * \brief
     * \param data
     */
    Intern_event_userdata(ayu_event_data_t data);

    /**
     * \brief ctor from packed data
     *
     * \param message
     * \todo maybe pass a protocol version eventually
     */
    Intern_event_userdata(const char *message);

    /**
     * \brief
     * \param message
     */
    void pack(char* message);

    /**
     * \brief
     * \param printformat
     * \param os
     */
    void print(ayu_printformat_t printformat,
               std::ostream &os = std::cout);

    /**
     * \brief
     * \return
     */
    ayu_count_t get_size() {
        return m_data.size();
    }

    /**
     * \brief
     * \return
     */
    std::string get_data() {
        return m_data;
    }

    /**
     * \brief
     * \return
     */
    virtual ayu_count_t get_message_size() const;

private:

    /**
     * \brief
     */
    std::string m_data;

protected:


};

#endif /* INTERN_ADD_USERDATA_HPP_ */
