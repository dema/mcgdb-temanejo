/**
 * \file intern_event_task.hpp
 * \brief Header of the internal event object for task
 *
 * \author Steffen Brinkmann <brinkmann@hlrs.de>
 * \author Mathias Nachtmann <nachtmann@hlrs.de>
 *
 * \copyright
 * (C) 2015 HLRS, University of Stuttgart.\n
 *    This software  is published under the terms of the BSD license.
 *    See the LICENSE file for details.
 *
 */
#ifndef INTERN_ADD_TASK_HPP_
#define INTERN_ADD_TASK_HPP_

#include <iostream>

#include "intern_event/intern_event.hpp"
#include "ayudame_types.h"

/**
 * \class Intern_event_task
 * \brief Class Intern_event_task
 *
 * tbd: detailed description of Intern_event_task
 *
 * \ingroup intern_event
 */
class Intern_event_task: public Intern_event
{

public:

    /**
     * \brief Deleted default constructor.
     *
     * \see Intern_event::Intern_event()
     *
     * \remark something with Intern_event::Intern_event()
     *
     * no creation without initialisation (RAII)
     */
    Intern_event_task() = delete;

    /**
     * \brief Detailed constructor.
     *
     * ctor with information passed one by one
     *
     *
     * \param _client_id
     * \param _event
     * \param _timestamp
     * \param _task_id
     * \param _scope_client_id
     * \param _scope_id
     * \param _task_label
     */
    Intern_event_task(ayu_client_id_t _client_id,
                      ayu_event_t _event,
                      ayu_count_t _timestamp,
                      ayu_id_t _task_id,
                      ayu_id_t _scope_client_id,
                      ayu_id_t _scope_id,
                      std::string _task_label);

    /**
     * \brief Contructor taking a ayu_event_data_t object
     * @copydoc Intern_event::Intern_event(ayu_event_data_t)
     * \param data
     */
    Intern_event_task(ayu_event_data_t data);

    /**
     * \brief ctor from packed data
     *
     * @copydoc  Intern_event::Intern_event(const char *)
     *
     * \todo maybe pass a protocol version eventually
     * \param message
     */
    Intern_event_task(const char *message);

    /**
     * \brief tbd
     * \param message
     */
    void pack(char* message);

    /**
     * \brief tbd
     * \return
     */
    ayu_count_t get_message_size() const;

    /**
     * \brief tbd
     * \param printformat
     * \param os
     */
    void print(ayu_printformat_t printformat,
               std::ostream &os = std::cout);

    /**
     * \brief tbd
     * \return
     */
    ayu_id_t get_task_id() const
    {
        return m_task_id;
    }

    /**
     * \brief tbd
     * \return
     */
    ayu_id_t get_scope_id() const
    {
        return m_scope_id;
    }

    /**
     * \brief tbd
     * \return
     */
    void set_scope_id(ayu_id_t id)
    {
        m_scope_id = id;

    }
    /**
     * \brief tbd
     * \return
     */
    ayu_id_t get_scope_client_id() const
    {
        return m_scope_client_id;
    }

    /**
     * \brief tbd
     * \return
     */
    void set_scope_client_id(ayu_id_t id)
    {
        m_scope_client_id = id;

    }
    /**
     * \brief tbd
     * \return
     */
    std::string get_task_label() const
    {
        return m_task_label;
    }

private:

    /**
     * \brief tbd
     */
    ayu_id_t m_task_id;

    /**
     * \brief tbd
     */
    ayu_id_t m_scope_client_id;

    /**
     * \brief tbd
     */
    ayu_id_t m_scope_id;

    /**
     * \brief tbd
     */
    std::string m_task_label;

protected:

};

#endif /* AYUDAME_INTERN_ADD_TASK_HPP_ */
