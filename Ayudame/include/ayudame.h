/**
 * \file ayudame.h
 * \brief Main Ayudame header
 *
 * This header is going to be included in user code.
 *
 * \author Steffen Brinkmann <brinkmann@hlrs.de>
 * \author Mathias Nachtmann <nachtmann@hlrs.de>
 * \author Jose Gracia <gracia@hlrs.de>
 *
 * \todo The client request callback. May look something like this:<br/>
 * <code>
 * ayu_error_t client_request(ayu_request_t request,
 *    ayu_request_data_t data)
 *    __attribute__ ((weak));
 * </code>
 *
 * \copyright
 * (C) 2015 HLRS, University of Stuttgart.\n
 *    This software  is published under the terms of the BSD license.
 *    See the LICENSE file for details.
 * */

#ifndef AYUDAME_H
#define AYUDAME_H

#include "ayudame_types.h"

#ifdef __cplusplus
class Client_event_handler;
#endif

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * \brief Callback for the client runtime
 *
 * This function is called from the client runtime to communicate
 * with the Ayudame package.
 * AYU_event will connect to a client via tcp sockets and pass on
 * the information.
 *
 * \param event type of event
 * \param data event data
 *
 */
void ayu_event(ayu_event_t event, ayu_event_data_t data)
    __attribute__ ((weak));




void ayu_register_get_callback(int (*f)(),tca_callback_t type, ayu_event_data_t data) __attribute__ ((weak));

/**
 * \brief Retrieve a unique client id
 *
 * This client id has to be passed along with every event the client runtime
 * sends. It can be retrieved several time and is assured to be the same.
 * It is unique for every client runtime, within the same process as well as
 * across processes and compute nodes.
 *
 * \remark If this is the mythical **skoi**, then it will need more parameters.
 * The declaration could look something like this:<br/>
 * <code>
 * ayu_client_id_t get_client_id(ayu_client_t client,
 *    ayu_error_t (*request_handler)(ayu_request_t request,
 *    ayu_request_data_t data), ayu_label_t client_label)
 *    __attribute__ ((weak));
 * </code>
 *
 * \param client The client type
 * \return The unique id for the client runtime
 */
ayu_client_id_t get_client_id(ayu_client_t client)
    __attribute__ ((weak));

/**
 * \brief Retrieve a pointer to the Ayudame client handler
 *
 * This pointer refers to Client_event_handler, which is an object that serves
 * as an interface to the Ayudame infrastructure. It offers several ways of
 * issueing events, either directly, e.g.<br/>
 * <code> handler->add_task(task_id, scope_id, task_label)</code><br/>
 * or passing an event data structure:<br/>
 * <code>handler->event(event, data)</code>
 *
 * \remark see remark for get_client_id(ayu_client_t client)
 *
 * \param client The client type
 * \return The pointer to the Client_event_handler object responsible for the
 * client runtime.
 */
#ifdef __cplusplus
Client_event_handler *get_ayudame_ptr(ayu_client_t client)
    __attribute__ ((weak));
#endif

/**
 * \brief Reset the data structure passed as an argument
 *
 * This is a convenience function for the client runtime programmer. It resets
 * al fields to (binary) 0.
 *
 * \param data event data
 */
void ayu_wipe_data(ayu_event_data_t *data)
    __attribute__ ((weak));

#ifdef __cplusplus
}
#endif
#endif				// AYUDAME_H
