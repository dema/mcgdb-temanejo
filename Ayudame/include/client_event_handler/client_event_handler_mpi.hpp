/**
 * \file client_event_handler_mpi.hpp
 * \brief header of the class Client_event_handler_mpi
 *
 * \author Mathias Nachtmann <nachtmann@hlrs.de>
 *
 * \copyright
 * (C) 2015 HLRS, University of Stuttgart.\n
 *    This software  is published under the terms of the BSD license.
 *    See the LICENSE file for details.
 *
 */
#ifndef CLIENT_EVENT_HANDLER_MPI_HPP_
#define CLIENT_EVENT_HANDLER_MPI_HPP_

class Client_event_manager;
class Matcher;

#include "ayudame_types.h"


#include "client_event_handler/client_event_handler.hpp"
#include "connect_handler/connect_handler_socket.hpp"
#include "ayu_socket.hpp"

#include <algorithm>
#include <atomic>
#include <map>
#include <memory>
#include <mutex>
#include <thread>
#include <vector>

/**
 * these extern function are used as callback to the mpi runtime
 * */
extern "C" {
    int get_rank();
    int get_hostnames(int *, int *, char **);
}

/**
 * \class Client_event_handler_mpi
 *  @copydoc Client_event_handler
 * \brief Class Client_event_handler_mpi
 * */
class Client_event_handler_mpi: public Client_event_handler {

    /**
     *
     */
    typedef std::map <int, std::shared_ptr <Socket>> client_socket_map;

public:

    /* as soon as gcc 4.8 reaches your realms, uncomment this and get
     rid of the previous lines */
    //  using Client_event_handler::Client_event_handler;

    /**
     * @copydoc Client_event_handler::Client_event_handler()
     * @param manager
     * @param ayu
     */
    Client_event_handler_mpi(Client_event_manager& manager, Ayudame& ayu, ayu_client_t client);

    /**
     *
     */
    Client_event_handler_mpi() = delete;

    /**
     *
     */
    virtual ~Client_event_handler_mpi();

    /**
     *
     * @param request
     */
    virtual void msg_send_out(std::shared_ptr<Intern_event> request);


    virtual void shutdown();

    /**
     *
     * @param event
     * @param data
     */
    virtual void event(const ayu_event_t event, ayu_event_data_t data);

    /**
     *
     * @param data
     */
    virtual void event(std::shared_ptr<Intern_event>  data);

private:


    /**
     * function creates the thread looping over all client sockets
     */
    void create_sockets_loop();

    /**
     * function to be passed to new thread for reading messages from all sockets
     */
    void read_sockets_loop();

    /**
     * function deactivates all connect_handlers and activates the ones needed for this rank
     * rank 0 will have the configuration set in the configuration file
     * all other ranks will only have an activated connect_handler_socket
     */
    void set_connect_handlers_status();

    /**
     *
     */
    std::shared_ptr<Ayu_Socket> m_server_socket;

    /**
     *  master
     */
    bool m_master;

    /**
     *  master
     */
    bool m_super;

    /**
     *  vector of all hostnames for each rank
     */
    std::vector <std::string> m_hostname_for_rank;

    /**
     *  my hostname
     */
    std::string m_my_hostname;

    /**
     *  mutex lock for m_user_data_MPI
     */
    std::mutex m_mutex_user_data_MPI;

    /**
     *  thread reading from sockets and writing to m_user_data_MPI
     */
    std::shared_ptr<std::thread> m_read_sockets_thread;

    /**
     *  flag which indicates whether m_read_sockets_thread should be active.
     */
    std::atomic<bool> m_active_read_sockets_thread;

    /**
     *  the mpi matcher
     */
    std::shared_ptr<Matcher> m_matcher;

};

#endif /* CLIENT_EVENT_HANDLER_MPI_HPP_ */
