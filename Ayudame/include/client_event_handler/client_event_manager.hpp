/**
 * \file client_event_manageri.hpp
 * \brief header of the class Client_event_manager
 *
 * \author Mathias Nachtmann <nachtmann@hlrs.de>
 *
 * \copyright
 * (C) 2015 HLRS, University of Stuttgart.\n
 *    This software  is published under the terms of the BSD license.
 *    See the LICENSE file for details.
 *
 */
#ifndef CLIENT_EVENT_MANAGER_HPP
#define CLIENT_EVENT_MANAGER_HPP

class Ayudame;
class Client_event_handler;

#include "ayudame_types.h"
#include "client_event_handler/client_event_handler.hpp"
#include "client_event_handler/client_event_handler_ompss.hpp"
#ifdef AYU_MPI
#include "client_event_handler/client_event_handler_mpi.hpp"
#endif

#include <map>
#include <memory>

/**
 * \class Client_event_manager
 * \brief Class Client_event_manager
 * */
class Client_event_manager {
    /**
     *
     */
    //todo make this vector of pair
    typedef std::map <ayu_client_t, std::shared_ptr <Client_event_handler>> client_event_handlers_map;

public:
    /**
     *
     * @param ayu
     */
    Client_event_manager(Ayudame& ayu);

    /**
     *
     */
    Client_event_manager() = delete;

    /**
     *
     */
    ~Client_event_manager();

    /**
     *
     */
    void shutdown();

    /**
     *
     */
    void force_shutdown();

    /**
     *
     * @param client
     * @return
     */
    std::shared_ptr <Client_event_handler> get_client_handler(const ayu_client_t client);


    void msg_send_out(std::shared_ptr<Intern_event> request);


    bool get_nesting_level(std::shared_ptr<Intern_event_task> data);



private:

    /**
     *
     * @param client
     * @return
     */
    std::shared_ptr <Client_event_handler> create_client_handler(const ayu_client_t client);

    /**
     *
     */
    Ayudame& m_ayu;





    /**
     *
     */
    client_event_handlers_map m_client_event_handlers;


    std::size_t shutdown_counter;

};

#endif //CLIENT_EVENT_MANAGER_HPP
