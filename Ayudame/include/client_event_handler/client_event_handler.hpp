/**
 * \file client_event_handler.hpp
 * \brief header of the class Client_event_handler
 *
 * \author Mathias Nachtmann <nachtmann@hlrs.de>
 *
 * \copyright
 * (C) 2015 HLRS, University of Stuttgart.\n
 *    This software  is published under the terms of the BSD license.
 *    See the LICENSE file for details.
 *
 */
#ifndef CLIENT_EVENT_HANDLER_HPP
#define CLIENT_EVENT_HANDLER_HPP

class Ayudame;
class Client_event_manager;

#include "ayudame_types.h"

#include "intern_event/intern_event.hpp"
#include "intern_event/intern_event_userdata.hpp"
#include "intern_event/intern_request.hpp"
#include "ayu_helper.hpp"

#include "ayudame_log.h"
#include <sys/types.h>
#include <unistd.h>

#include "tca_tool.h"

#include <memory>
#include <sstream>

/**
 * \class Client_event_handler
 * \brief Class Client_event_handler
 * */
class Client_event_handler
{
public:


    tca_function_lookup_t lookup;
    int (*m_get_wd_id_function)();

    /**
     *
     * @param manager
     * @param ayu
     */
    Client_event_handler(Client_event_manager& manager, Ayudame& ayu, ayu_client_t client);

    /**
     *
     */
    Client_event_handler() = delete;

    /**
     *
     */
    virtual ~Client_event_handler();

    /**
     *
     * @param request
     */
    virtual void msg_send_out(std::shared_ptr <Intern_event> request);

    virtual void shutdown() = 0;
    /**
     *
     * @param event
     * @param data
     */
    virtual void event(const ayu_event_t event,
                       ayu_event_data_t data);


    virtual void tca_initialize(tca_function_lookup_t lookup,
                        const char *runtime_version,
                        unsigned int tca_version){
        LOG(ERROR) << "tca_initialize not implmented";
    }



    void register_get_callback(int (*f)(), tca_callback_t type);
    /**
     *
     * @param task_id
     * @param scope_id
     * @param task_label
     */
    virtual void add_task(const ayu_id_t task_id,
                          const ayu_id_t scope_id,
                          const ayu_label_t task_label);

    /**
     *
     * @param dependency_id
     * @param from_id
     * @param to_id
     * @param dependency_label
     */
    virtual void add_dependency(const ayu_id_t dependency_id,
                                const ayu_id_t from_id,
                                const ayu_id_t to_id,
                                const ayu_label_t dependency_label);
    /**
     *
     * @param dependency_id
     * @param from_client_id
     * @param from_id
     * @param to_client_id
     * @param to_id
     * @param dependency_label
     */
    virtual void add_dependency_matched(const ayu_id_t dependency_id,
                                        const ayu_client_id_t from_client_id,
                                        const ayu_id_t from_id,
                                        const ayu_client_id_t to_client_id,
                                        const ayu_id_t to_id,
                                        const ayu_label_t dependency_label);

    /**
     *
     * @param property_owner_id
     * @param key
     * @param value
     */
    virtual void set_property_matched(const ayu_client_id_t client_id,
                                      const ayu_id_t property_owner_id,
                                      const ayu_label_t key,
                                      const ayu_label_t value);

    /**
     *
     * @param property_owner_id
     * @param key
     * @param value
     */
    virtual void set_property(const ayu_id_t property_owner_id,
                              const ayu_label_t key,
                              const ayu_label_t value);

    /**
     *
     * @param _data
     */
    virtual void add_userdata(const ayu_label_t _data);

    virtual void finish();

    uint64_t get_m_id()
    {
        return m_id;
    }

protected:

    /**
     *
     */
    Client_event_manager& m_manager;

    uint64_t m_id;

    /**
     *
     */
    Ayudame& m_ayu;

    ayu_client_t m_client;



private:

};

/**
 * \class Client_event_handler_generic
 *  @copydoc Client_event_handler
 * \brief Class Client_event_handler_generic
 * */
class Client_event_handler_generic: public Client_event_handler
{
public:

    /**
     * @copydoc Client_event_handler::Client_event_handler()
     * @param manager
     * @param ayu
     */
    Client_event_handler_generic(Client_event_manager& manager, Ayudame& ayu, ayu_client_t client)
        : Client_event_handler(manager, ayu, client)
    {
    }

    /**
     *
     */
    Client_event_handler_generic() = delete;
    void shutdown()
    {
    }
    ;

    /* as soon as gcc 4.8 reaches your realms, uncomment this and get
     rid of the previous lines */
    //	using Client_event_handler::Client_event_handler;
};

/**
 * \class Client_event_handler_cppss
 *  @copydoc Client_event_handler
 * \brief Class Client_event_handler_cppss
 * */
class Client_event_handler_cppss: public Client_event_handler
{
public:

    /**
     * @copydoc Client_event_handler::Client_event_handler()
     * @param manager
     * @param ayu
     */
    Client_event_handler_cppss(Client_event_manager& manager, Ayudame& ayu, ayu_client_t client)
        : Client_event_handler(manager, ayu, client)
    {
    }

    /**
     *
     */
    Client_event_handler_cppss() = delete;
    void shutdown()
    {
    }
    ;

    /* as soon as gcc 4.8 reaches your realms, uncomment this and get
     rid of the previous lines */
    //	using Client_event_handler::Client_event_handler;
};

/**
 * \class Client_event_handler_smpss
 *  @copydoc Client_event_handler
 * \brief Class Client_event_handler_smpss
 * */
class Client_event_handler_smpss: public Client_event_handler
{
public:

    /**
     * @copydoc Client_event_handler::Client_event_handler()
     * @param manager
     * @param ayu
     */
    Client_event_handler_smpss(Client_event_manager& manager, Ayudame& ayu, ayu_client_t client)
        : Client_event_handler(manager, ayu, client)
    {
    }

    /**
     *
     */
    Client_event_handler_smpss() = delete;
    void shutdown()
    {
    }
    ;

    /* as soon as gcc 4.8 reaches your realms, uncomment this and get
     rid of the previous lines */
    //	using Client_event_handler::Client_event_handler;
};



/**
 * \class Client_event_handler_test
 *  @copydoc Client_event_handler
 * \brief Class Client_event_handler_test
 * */
class Client_event_handler_test: public Client_event_handler
{
public:

    /**
     * @copydoc Client_event_handler::Client_event_handler()
     * @param manager
     * @param ayu
     */
    Client_event_handler_test(Client_event_manager& manager, Ayudame& ayu, ayu_client_t client)
        : Client_event_handler(manager, ayu, client)
    {
    }

    /**
     *
     */
    Client_event_handler_test() = delete;
    void shutdown()
    {
    }
    ;

    /* as soon as gcc 4.8 reaches your realms, uncomment this and get
     rid of the previous lines */
    //	using Client_event_handler::Client_event_handler;
};



/**
 * \class Client_event_handler_starpu
 *  @copydoc Client_event_handler
 * \brief Class Client_event_handler_starpu
 * */
class Client_event_handler_starpu: public Client_event_handler
{
public:

    /**
     * @copydoc Client_event_handler::Client_event_handler()
     * @param manager
     * @param ayu
     */
    Client_event_handler_starpu(Client_event_manager& manager, Ayudame& ayu, ayu_client_t client)
        : Client_event_handler(manager, ayu, client)
    {
    }

    /**
     *
     */
    Client_event_handler_starpu() = delete;
    void shutdown()
    {
    }
    ;

    /* as soon as gcc 4.8 reaches your realms, uncomment this and get
     rid of the previous lines */
    //	using Client_event_handler::Client_event_handler;
};


#endif // EVENT_HANDLER_HPP
