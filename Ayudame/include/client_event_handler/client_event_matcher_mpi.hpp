/**
 * \file client_event_matcher.hpp
 * \brief header of the class Client_event_matcher
 *
 * \author Mathias Nachtmann <nachtmann@hlrs.de>
 * \author Vladimir Marjanovic <marjanovic@hlrs.de>
 * \copyright
 * (C) 2015 HLRS, University of Stuttgart.\n
 *    This software  is published under the terms of the BSD license.
 *    See the LICENSE file for details.
 *
 */
#ifndef _MATCHER_H__
#define _MATCHER_H_

#include <iostream>
#include <vector>
#include <string>
#include <stdint.h>

#include "ayudame_log.h"
//TODO needed ?
#include <ayudame.h>
#include "client_event_handler/client_event_handler.hpp"
#include "intern_event/intern_event.hpp"
#include "intern_event/intern_event_userdata.hpp"



class Matcher
{

public:
    Matcher() = delete;
    Matcher( Client_event_handler * );

    Matcher(const Matcher&) = delete;

    Matcher& operator=(const Matcher&) = delete;

    bool update_buffer(std::shared_ptr<Intern_event>  data);

    int get_size_pending(){
        return m_size_pending;
    }


    void update(void);

private:

    typedef struct
    {
        int partner;
        int tag;
        int routine;
        int comm;
        uint64_t task_id;
        uint64_t rank;
        uint64_t client_id;
    } envelope;

    int m_size_pending;
    Client_event_handler * m_client;

    envelope m_envelope;
    uint64_t *m_buffer;

    std::vector< std::vector< envelope > > pending_mpi;

    std::vector<std::string> split(std::string str, char delimiter);

    uint64_t m_unique_dep_id;
};
#endif
