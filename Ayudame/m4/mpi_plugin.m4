# this will evaluate the argument --with-ompss
# and do a minimal check that the header files are usuable
#
# outputs:
#    conditional:   ENABLE_MPI_PLUGIN
AC_DEFUN([AC_AYUDAME_MPI], [
AC_CONFIG_FILES([instrumentations/MPI/Makefile])

AC_ARG_ENABLE([mpi],
  [AS_HELP_STRING([--enable-mpi], [Enable mpi support @<:@no@:>@])],
  [:],
  [enable_mpi=no])
  CC_OLD=$CC
  CC=mpicc
  #CC=cc
  AS_IF([test "$enable_mpi" != "no"],
  [AC_CHECK_HEADERS([mpi.h],
    [enable_mpi=yes],
    [AS_IF([test "$enable_mpi" = "yes"],
      		[AC_MSG_ERROR([mpi required, but not found.])],
      		[enable_mpi=no])],
    [])
  ])
  CC=$CC_OLD

  AM_CONDITIONAL(ENABLE_MPI_PLUGIN, test "$enable_mpi" != "no")


])
