'''!
@author: Stefan Reinhardt
@contact temanejo@hlrs.de
@copyright  (C) 2015, HLRS, University of Stuttgart
            This software  is published under the terms of the BSD license.
            See the LICENSE file for details.


@brief The main Module for Temanejo2

This is the main Module of Temanejo2.\n 
The application will be started here.\n
This module has 2 parts:\n
- The first part is the Temanejo class:
All controller are created and the connections to views and 
of the Temanejo class.\n
Also all models are initialized in order to connect them to their views via the 
controller/mvcConnector\n
- The second part just starts the Application.\n
It creates a Instance if the Temanejo object and calls the show method in order
to show the window. Finally it exectutes the qt app. this is done according to the qt
specs

'''

import logging
import sys
import optparse

import os
import os.path
import subprocess
from time import localtime, strftime
from PySide import QtGui, QtCore, QtSvg

import temanejo2.lib.easymodel.treemodel as easyModel
from temanejo2.controller import (EdgeController, NodeController,
                                  mainApplicationController, menuActionConnector)
from temanejo2.controller.graph import GraphUserInputController
from temanejo2.controller.graph.graphAction import GraphActionEvents
from temanejo2.controller.graph.graphApearance import GraphAppearanceEvents
from temanejo2.controller.graph.remoteControls import (DataParser,
                                                       GraphRemoteController, ddtComunicator)
from temanejo2.controller.importExportExternAction import GraphImporterAndExportEvents, DDDEvents

from temanejo2.core import mvcConnector, configParseAndGen
from temanejo2.core.io.graphLayout import GraphLayout as graphlayoutIO
from temanejo2.core.io.layout_worker import LayoutWorker
from temanejo2.lib import configuredValues, randomColors, temPaths
from temanejo2.model import PropertyModels, TemanejoGraphicsScene
from temanejo2.view import mainwindow as tmw
from temanejo2.view.view_items import widgetCollections

# Initialize Logger and get handle to it
logging.basicConfig(level=logging.ERROR) # ,filename='temanejo2.log')
logger = logging.getLogger(__name__)
logger.setLevel(logging.ERROR)

# tell everybody about the resources path
import os
import temanejo2.lib.constants


from temanejo2.lib import resources

from threading import Lock



##
#     The main Temanejo Class. The UI will be launched and all 
#     connections should be set up here
#     the class has 3 parts:\n
#     - Init-section\n
#     - Controllercreation-section\n
#     - Modelcreation-section\n
#     The Views are also initialized here but not created.
#     This is done in the mainwindow.ui File according to qt specs.\n
#     To add custom views to the GUI you need to add the base view to 
#     the ui File in the qtdesigner and promote your widget onto it.\n
#     For further information how to do that, please read the README file.
class Temanejo(QtGui.QMainWindow):

    ##
    #   Setup the ui:
    #   - Connect the ui-File via setupUi
    #   - Create all Models
    #   - Create all Controller
    #   - Connect the Controller to the views and the Models
    def __init__(self, parent=None):
        # initiaise  option parser
        parser = self.init_optparser()
        # stop prsing when first unknown option occurs
        # this will probably be the application and everythin after it
        # is an application argument
        parser.disable_interspersed_args()
        # process options
        (options, args) = parser.parse_args(sys.argv[1:])

        #Read in userdefinedConfiguration
        self.pathsToConfigFile = configParseAndGen.getConfigFilePaths(options)
        logger.info('Reading config ...')
        self.configParser = configParseAndGen.readConfig(self.pathsToConfigFile)

        # Initialize Debug
        logger.debug("Started Temanejosession %s"%strftime("%a, %d %b %Y %H:%M:%S +0000", localtime()))
        # Initialize QMainWindow
        super(Temanejo,self).__init__(parent)
        ##@member Get Handle onto the UI
        self.ui = tmw.Ui_MainWindow()
        ##@member The handle to the current active layout routine
        self.currentActiveLayout = configuredValues.STANDARD_LAYOUT_ROUTINE
        #Set Stylesheet
        self.applyStyleSheet()
        # Initialize the UI
        self.ui.setupUi(self)
        # Make custom Mods for the existing ui
        self.modifyUI()
        ##@member The Eventhandler for all graphevents which relate to the appearance of the graph
        self.graphAppearanceEventHandler    = GraphAppearanceEvents(self.ui.temanejoGraph)
        self.graphActionEventHandler        = GraphActionEvents(self)
        self.importExportExternEventHandler = GraphImporterAndExportEvents(self)
        self.dddEventHandler                = DDDEvents(self)

        ##@member A list of unknown Properties. If properties come along alone and cannot be asigned
        #         to a task or a dependency they will be cached in here and later added to the 
        #         accoring item the moment the item is send from remote 
        self.unknownProperties = []

        self.graph_is_dirty_lock = Lock()
        self.currently_layouting_lock = Lock()

        #Create Models
        self.createModels()
        #Create Controller
        self.createController()
        #Create ObjectCollections
        self.createCollections()
        #Connect everything
        mvcConnector.connectAll(self)
        logger.debug("UI setup finished")

        self.debugger= None
    
        #===============================================================================================
        # PROCESS OPTIONS
        #===============================================================================================
        env_dict = os.environ
        port = 0
        if not options.port == '':
            port=options.port
        elif 'AYU_PORT' in env_dict:
            port = env_dict['AYU_PORT']
        if not port == 0:
            self.remoteControl.openServer(int(port))

        ddt_port = 0
        if not options.ddt_port == '':
            ddt_port = options.ddt_port
        elif 'DDT_PORT' in env_dict:
            port = env_dict['DDT_PORT']
        if not ddt_port == 0:
            self.remoteControl.openDDTServer(port=int(ddt_port))

        if options.verbose == 1:
            logging.getLogger().setLevel(logging.WARNING)
        if options.verbose == 2:
            logging.getLogger().setLevel(logging.INFO)
        if options.verbose > 2:
            logging.getLogger().setLevel(logging.DEBUG)

        ##@member A convenience Parameter which can prevent the graph from relayouting if wished
        self.block_relayout = False

        if not options.file_import == '':
            import_file=options.file_import
            self.importExportExternEventHandler.importFromXML(import_file)


        preload=""
        library_path=""
        application=""

        ## start application
        if not options.preload == '':
            preload = "LD_PRELOAD="+options.preload
        if not options.library_path == '':
            library_path= "LD_LIBRARY_PATH="+options.library_path+":$LD_LIBRARY_PATH"
        if not options.application == '':
            application =options.application

        if not application == '':
            print preload, library_path, application
            self.runApplication(preload, library_path, application )




    def runApplication(self, ld_preload,ld_library_path, application):
        #NX_INSTRUMENTATION= "NX_ARGS=\"--instrumentation=ayudame\""
        NX_INSTRUMENTATION= "NX_INSTRUMENTATION=ayudame"
        NX_SMP_WORKERS ="NX_SMP_WORKERS=2"
        debugger_command_line = NX_INSTRUMENTATION+" "+ NX_SMP_WORKERS +" "+ld_library_path+" "+ld_preload+" "+application
        debugger_command_line = "\""+debugger_command_line+"\""


        ext_application='application'
        known_terminals = {
            "xterm" : ['xterm', '-geometry', '80x40',
                        '-title', '\"', ext_application, ' started by Temanejo\"',
                        '-e',debugger_command_line],
            'xfce4-terminal' : ['xfce4-terminal', '--geometry', '80x40',
                        '--title', '\"', ext_application, ' started by Temanejo\"',
                        '-e',debugger_command_line],
            'rxvt' : ['rxvt', '-geometry', '80x40',
                        '-title', '\"', ext_application, ' started by Temanejo\"',
                        '-e',debugger_command_line],
            'urxvt' : ['urxvt', '-geometry', '80x40',
                        '-title', '\"', ext_application, ' started by Temanejo\"',
                        '-e',debugger_command_line],
            'rxvt-unicode' : ['rxvt-unicode', '-geometry', '80x40',
                        '-title', '\"', ext_application, ' started by Temanejo\"',
                        '-e',debugger_command_line]
        }

        try:
            terminal = os.environ['TERM']
        except:
            logging.warning('terminal %s not found. Falling back to xterm.' % terminal)
            terminal = 'xterm'
        if not terminal in known_terminals:
            logging.warning('terminal %s not known. Falling back to xterm.' % terminal)
            terminal = 'xterm'


        cmd = known_terminals[terminal]
        print cmd
        print ' '.join(cmd)
        msg = ''

        try:
            self.debugger = subprocess.Popen(' '.join(cmd), shell=True)  # , stdin=subprocess.PIPE)
        except  (OSError, msg):
            logging.error(msg)
            logging.error(
                'Opening subprocess for external debugger failed:\n\t%s', ' '.join(cmd))
            return

        print "Application STARTED"

    #===============================================================================================
    # CONTROLLER CREATION PART
    #===============================================================================================
    ##
    #   Creates all ControlObjects so they can be connected afterwards.\n
    #   This is just a container where all controller-creation methods are called
    def createController(self):
        self.createMainAppController()
        self.createGraphController()
        self.createNodeEdgeController()
        self.createMenuItemController()
        self.createIntermediateDataController()
        #self.createSequenceDiagramController()
        logger.debug("Created Controller")

    ##
    #   Creates the controller for the main Application 
    def createMainAppController(self):
        ##@member A handle to the main Application controller where all application level events 
        #         will be handled
        self.mainAppCtrl = mainApplicationController.MainAppControll(self)

    ##
    #   Creates all controller for the graph in order to handle graphevents
    def createGraphController(self):
        # Get handle of the graphicsView from the UI and set it to the controllerObject
        ##@member A handle onto the controller for all User Input events
        self.graphUserInputCtrl = GraphUserInputController.GraphUserInputEvents(self.graphAppearanceEventHandler)
        ##@member A handle onto the controller for all Remote Events
        self.remoteControl = GraphRemoteController.RemoteEvents(self)
        # Create layoutselector
        routineProvider = "hlrs_custom" if self.currentActiveLayout == "hlrs_custom" else "graphviz"
        self.graphlayoutIO = graphlayoutIO(self, routineProvider)
        #Create layoutWorker
        self.layoutWorker = LayoutWorker(self)
        #Connect the layoutworker Signal
        self.graphlayoutIO.makeGraphDirty.connect(self.layoutWorker.dirtyGraph)

        self.layoutWorker.start()

        #TODO: Remove this and call layout worker
        # Layouts the graph for first initializing temanejo
        self.layoutWorker.force_relayout = True
        #self.remoteControl.layoutCtrl.layoutPgvGraph()

    ##
    #   Create the node/edge controller.\n
    #   All the node/edgeEvents will be handled in the node/edgeController 
    def createNodeEdgeController(self):
        ##@member A handle onto the controller for all NodeEvents
        self.nodeCtrl = NodeController.NodeEvents(self.ui.temanejoGraph)
        ##@member A handle onto the controller for all EdgeEvents
        self.edgeCtrl = EdgeController.EdgeEvents(self.ui.temanejoGraph)

    ## 
    #   Create Controller for MenuActions which is not more than just a mvcConnector to the
    #   correct Actionmethod
    def createMenuItemController(self):
        ##@member A handle onto the controller for all Actions caused by any Menu item
        self.menuActionCtrl = menuActionConnector.MenuActionConnector(self)

    ##
    #   Creates all controller which handle intermediate Data like the Dataparser which
    #   parses Data from the remote controller and sets/updates the GraphModel 
    def createIntermediateDataController(self):
        self.dataParser = DataParser.DataParser(self)

    def createSequenceDiagramController(self):
        # disabled
        self.seqDiagramView = seqDiagram.SeqDiagramView(self.ui.seqDiagram)
        self.seqDiagramController = seqDiagram.SeqDiagramController(self, self.seqDiagramView)

    #===============================================================================================
    # MODEL CREATION PART
    #===============================================================================================
    ##
    #  Creates all Models
    def createModels(self):
        self.createGraphModel()
        self.createPropertyModel()
        logger.debug("Created Models")

    ##
    #  Creates the Graph Model where all Nodes, Edges etc can be held...
    def createGraphModel(self):
        ##@member A handle onto the the graphmodel
        self.graphModel = TemanejoGraphicsScene.TemanejoGraphicsScene(self)

    ##
    #   Creates all neccessary Property models which basically are:
    #   - A property list model in which the kind of props are held
    #   - A property tree model in which the values of the props are captured
    def createPropertyModel(self):
        # NodeProperties
        self.propSelectionModel = PropertyModels.PropertySelectionModel()
        #The overall root item of the model
        self.propDisplayModelRootItem = easyModel.TreeItem(
            easyModel.ListItemData(["Values", "#Nodes", "Color"], seperateListitemsInView=False))
        self.propDisplayModel = PropertyModels.DisplayViewModel(self, propDisplayModelRootItem=self.propDisplayModelRootItem)

        # DepProperties
        self.depPropSelectionModel = PropertyModels.PropertySelectionModel()
        #The overall root item of the model
        self.depPropDisplayModelRootItem = easyModel.TreeItem(
            easyModel.ListItemData(["Values", "#Dep's", "Color"], seperateListitemsInView=False))
        self.depPropDisplayModel = PropertyModels.DisplayViewModel(self, propDisplayModelRootItem=self.depPropDisplayModelRootItem)
    # ===============================================================================================
    # COLLECTION CREATION PART
    # ===============================================================================================

    ##
    #   This method creates all kind of collections for GUI-Elements which will be added onto the UI
    def createCollections(self):
        self.createColorCollection()
        self.createShapeCollection()
        self.createButtonCollections()
        self.createPropertyCollections()
        logger.debug("Created Collections")

    ##
    #   Creates all Button-collections
    def createButtonCollections(self):
        ##@member A handle onto the zoomButtonCollection
        self.zoomButtons = widgetCollections.zoomButtonCollection(self)
        self.stepButtons = widgetCollections.graphManipButtonCollection(self)

    ##
    #   This method will create all PropertyCollections
    #   For now these are only the 3 Property windows on the left dockWindow
    def createPropertyCollections(self):
        self.propertyWidgets, self.dep_propertyWidgets = widgetCollections.propertyWidgetCollection(self)


    ##
    #   Creates a colorPalette in order to be able to paint colors by attribute 
    def createColorCollection(self):
        palette_num_colours = configuredValues.NUM_OF_DIFFERENT_COLORS
        self.colorPalette = list(map(randomColors.palette_rgb_inf, range(palette_num_colours)))

    ##
    #  
    def createShapeCollection(self):
        self.shapePalette = configuredValues.ACCEPTABLE_SHAPES

    ##
    #   Here the Cascading Style Sheet will be read in.
    #   In the CSS File the appearance of every GUI-Element can be modified l
    #   so it gets an individual look and feel. This can be done without changing
    #   any code by simply manipulating the stylesheet.css file found in the lib package
    def applyStyleSheet(self):
        css = QtCore.QFile(':css/stylesheet.css')
        css.open(QtCore.QIODevice.ReadOnly)
        if css.isOpen():
            self.setStyleSheet(bytearray(css.readAll()).decode("utf-8"))
        css.close()
        logger.debug("Applied StyleSheet")

    ##
    #   Eventhandler for closing the application
    def closeEvent(self, evt):
        configParseAndGen.writeConfig(self.pathsToConfigFile, self.configParser)
        if self.debugger is not None:
            self.debugger.kill()
        self.mainAppCtrl.closeApp(evt)

    ##
    # Convenience Function for blocking and unblocking the relayout
    def block_unblock_relayout(self):
        self.block_relayout = not self.block_relayout

    # ===============================================================================================
    # UI Modifiers
    # ===============================================================================================
    ##
    #   All UI modifications which are no widgets are handled here.\\
    #   This is needed to e.g. create ActionGroups for MenuItems so they behave like a Radiobutton 
    #   group 
    def modifyUI(self):
        self.layoutRadioMenuActions()

    ##
    # @TODO Find an appropriate place for the following...
    ##
    #   Creates A Radiobuttongroup out of all posibilities to Layout the graph.\\
    #   This enforces that there can be only one layout routine active at once...
    def layoutRadioMenuActions(self):
        # Create ActionGroup
        self.layoutActionGroup = QtGui.QActionGroup(self)
        self.layoutActionGroup.addAction(self.ui.actionCirco)
        self.layoutActionGroup.addAction(self.ui.actionDOT)
        self.layoutActionGroup.addAction(self.ui.actionNeato)
        self.layoutActionGroup.addAction(self.ui.actionTwopi)
        self.layoutActionGroup.addAction(self.ui.actionFdp)
        self.layoutActionGroup.addAction(self.ui.actionNop)
        self.layoutActionGroup.addAction(self.ui.actionHLRS_Layout)
        self.ui.actionNop.setEnabled(False)
        self.layoutActionGroup.setExclusive(True)
        # Check the correct button at initial state
        if self.currentActiveLayout == "circo":
            self.ui.actionCirco.setChecked(True)
        elif self.currentActiveLayout == "dot":
            self.ui.actionDOT.setChecked(True)
        elif self.currentActiveLayout == "neato":
            self.ui.actionNeato.setChecked(True)
        elif self.currentActiveLayout == "twopi":
            self.ui.actionTwopi.setChecked(True)
        elif self.currentActiveLayout == "fdp":
            self.ui.actionFdp.setChecked(True)
        elif self.currentActiveLayout == "nop":
            self.ui.actionNop.setChecked(True)
        elif self.currentActiveLayout == "hlrs_custom":
            self.ui.actionHLRS_Layout.setChecked(True)

    ##
    #   It returns the current selected layoutroutine by checking
    #   which action is currently selected
    def createOnFlyLayoutCodeForLayoutAction(self):
        if self.ui.actionCirco.isChecked():
            self.currentActiveLayout = "circo"
            return "circo"
        if self.ui.actionDOT.isChecked():
            self.currentActiveLayout = "dot"
            return "dot"
        if self.ui.actionNeato.isChecked():
            self.currentActiveLayout = "neato"
            return "neato"
        if self.ui.actionTwopi.isChecked():
            self.currentActiveLayout = "twopi"
            return "twopi"
        if self.ui.actionFdp.isChecked():
            self.currentActiveLayout = "fdp"
            return "fdp"
        if self.ui.actionNop.isChecked():
            self.currentActiveLayout = "nop"
            return "nop"
        if self.ui.actionHLRS_Layout.isChecked():
            self.currentActiveLayout = "hlrs_custom"
            return "hlrs_custom"

    def init_optparser(self):
        __version__ =2.0
        parser = optparse.OptionParser(
            usage='%prog [options] ',
            description='Temanejo starts a server for task-parallel application '
            'which has been instrumented with calls to the Ayudame library. '
            'Supported runtimes include SMPSs, OmpSs, StarPU, and CppSs. ',
            epilog='Temanejo is published under the terms of the BSD license. '
            '(C) HLRS, University of Stuttgart. For further information '
            'please refer to the '
            'LICENSE file.',
            version='%%prog %s' % __version__)

        parser.add_option(
            '-v', '--verbose',
            action='count', dest='verbose', default=0,
            help='Switch on verbose output, which prints warnings. Use -vv '
            '(info) and -vvv (debug) for increasingly epic information.')
        parser.add_option(
            '-q', '--quiet',
            action='store_true', dest='quiet', default=False,
            help='Switch off any output except critical error messages. This '
            'overwrites -v, -vv and -vvv.')

        group_conn = optparse.OptionGroup(parser, "Connection Options")
        group_conn.add_option(
            '-p', '--port',
            action='store', dest='port', default='', metavar='PORT',
            help='Specify parameters for inital server port opening')

        group_conn.add_option(
            '-d', '--ddt_port',
            action='store', dest='ddt_port', default='', metavar='DDT_PORT',
            help='Specify parameters for inital ddt_server port opening')

        group_conn.add_option(
            '-F', '--file_import',
            action='store', dest='file_import', default='', metavar='FILE_IMPORT',
            help='Specify parameters for file import')

        parser.add_option_group(group_conn)

        group_conf = optparse.OptionGroup(parser, "Path to Config File")
        group_conf.add_option(
            '-c', '--configpath',
            action='store', dest='configpath', default='', metavar='CONFIGPATH',
            help='Specify the (full) path to your custom config file')
        parser.add_option_group(group_conf)
        group_conf = optparse.OptionGroup(parser, "Applocation Options")
        group_conf.add_option(
            '-P', '--LD_PRLOAD',
            action='store', dest='preload', default='', metavar='CONFIGPATH',
            help='Specify the LD_PRELOAD')
        group_conf.add_option(
            '-L', '--LD_LIBRARY_PATH',
            action='store', dest='library_path', default='', metavar='CONFIGPATH',
            help='Specify the LD_LIBRARY_PATH')
        group_conf.add_option(
            '-A', '--Application',
            action='store', dest='application', default='', metavar='CONFIGPATH',
            help='Specify the Application path')
        parser.add_option_group(group_conf)

        return parser

    def render_temanejo_to_svg(self, file_path, title="Temanejo2 Screenshot", desc="Temanejo2"):

        self.svg_gen = QtSvg.QSvgGenerator()
        self.svg_gen.setFileName(file_path)
        self.svg_gen.setSize(QtCore.QSize(self.width(), self.height()))
        self.svg_gen.setViewBox(QtCore.QRect(0, 0, self.width(), self.height()))
        self.svg_gen.setTitle(title)
        self.svg_gen.setDescription(desc)

        self.svg_painter = QtGui.QPainter()
        self.svg_painter.begin(self.svg_gen)
        self.svg_painter.setRenderHint(QtGui.QPainter.Antialiasing)
        self.render(self.svg_painter, QtCore.QPoint(0, 0))
        self.svg_painter.end()

        # self.svg_painter = QtGui.QPainter(self.svg_gen)
        # self.render(self.svg_painter, QtCore.QPoint(0,0))


#   Start the main Application
if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    mainWin = Temanejo()
    mainWin.show()
    sys.exit(app.exec_())
