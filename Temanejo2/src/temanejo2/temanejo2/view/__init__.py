"""!
@brief This package usually only contains the uncompiled *.ui files produced by the Qt-Designer as well as 
the compiled files with all the modifications which were necessary

@author Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com

@attention All the modifcations made on a compiled *.ui file will be undone by recompiling it. So 
make sure you place your modifications to a save folder too.\n

@todo One could think about wrapping the compilation process and save the old compiled ui file into
a backup folder located in here.

"""
__all__=[]