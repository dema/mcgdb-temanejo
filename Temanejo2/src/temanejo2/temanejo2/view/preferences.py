# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'src/temanejo2/temanejo2/view/preferences.ui'
#
# Created: Wed Sep 28 09:32:38 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Preferences(object):
    def setupUi(self, Preferences):
        Preferences.setObjectName("Preferences")
        Preferences.resize(564, 350)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("../../resources/Icon_128x128.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Preferences.setWindowIcon(icon)
        self.gridLayout = QtGui.QGridLayout(Preferences)
        self.gridLayout.setObjectName("gridLayout")
        self.splitter = QtGui.QSplitter(Preferences)
        self.splitter.setOrientation(QtCore.Qt.Horizontal)
        self.splitter.setObjectName("splitter")
        self.categories = QtGui.QListWidget(self.splitter)
        self.categories.setObjectName("categories")
        self.scrollArea = QtGui.QScrollArea(self.splitter)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.categoryWidget = QtGui.QWidget()
        self.categoryWidget.setGeometry(QtCore.QRect(0, 0, 222, 297))
        self.categoryWidget.setObjectName("categoryWidget")
        self.scrollArea.setWidget(self.categoryWidget)
        self.gridLayout.addWidget(self.splitter, 0, 0, 1, 1)
        self.cancelOKbuttonBox = QtGui.QDialogButtonBox(Preferences)
        self.cancelOKbuttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.cancelOKbuttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.cancelOKbuttonBox.setObjectName("cancelOKbuttonBox")
        self.gridLayout.addWidget(self.cancelOKbuttonBox, 1, 0, 1, 1)

        self.retranslateUi(Preferences)
        QtCore.QObject.connect(self.cancelOKbuttonBox, QtCore.SIGNAL("accepted()"), Preferences.accept)
        QtCore.QObject.connect(self.cancelOKbuttonBox, QtCore.SIGNAL("rejected()"), Preferences.reject)
        QtCore.QMetaObject.connectSlotsByName(Preferences)

    def retranslateUi(self, Preferences):
        Preferences.setWindowTitle(QtGui.QApplication.translate("Preferences", "Preferences", None, QtGui.QApplication.UnicodeUTF8))

