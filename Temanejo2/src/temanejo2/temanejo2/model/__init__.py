"""!
@brief The model package contains all data wrapped in a (custom) Qt-Model container. 

@author Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com


All Object Container are created here as defined in the core.
The Items to set into each container will be derived from the controller package where the main application
will be running. To be more specific all Models should be filled from a layout-routine-wrapper which
parses all data in and redirects it to the correct model.    
"""
__all__=[]
