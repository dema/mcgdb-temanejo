'''!
@brief This Module helds the Scene Model for the Graph

@todo: make nice description

@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''
import logging

from PySide import QtGui

logger = logging.getLogger(__name__)


#from temanejo2.temanejo2.core.items import EdgeItem, NodeItem
##
#  This class generates an extension of the QGraphicsScene
#  Where all graph-items will be held possible types to add
#  can be found in the core.items package 
class TemanejoGraphicsScene(QtGui.QGraphicsScene):

    ##
    #  Constructs the object
    # @param temanejo: The handle on the main app in order to get global vars
    def __init__(self,temanejo,parent=None):
        QtGui.QGraphicsScene.__init__(self)
        self._nodes = dict()
        self._edges = dict()
        self.temanejo = temanejo
        self.setItemIndexMethod(QtGui.QGraphicsScene.NoIndex)
        self.sceneBoundaries = self.sceneRect()
        
    ##
    #   Adds the item really to the model
    def addItemToModel(self, item):
        self.addItem(item)
    
    ##
    #   Adds many items to the model
    #   @param items Should be a iterable list
    def addItemsToModel(self, items):
        for item in items: 
            self.addItemToModel(item)
    
    ##
    #   Removes a node from the model
    def removeNode(self, id):
        self.removeItem(self._nodes[id])
    
    ##
    #   Removes a edge from the model
    def removeEdge(self, id):
        self.removeItem(self._edges[id])
    
    ##
    #   Clears the whole model
    def clearGraph(self):
        self.clear()
        self._nodes.clear()
        self._edges.clear()
    
    ##
    #   Returns all cached Nodes\n
    #   @WARN A Node added to the GraphicsScene does not necessarily mean it will be painted
    #          This is only a container of items which could be painted when refresh scene is called
    def getNodes(self):
        return self._nodes.values()

    def getNodeKeys(self):
        return self._nodes.keys()

    def getEdgeKeys(self):
        return self._edges.keys()

    def getEdgeKeysOrigIds(self):
        edgeList = []
        for edge in self._edges.values():
            edgeList.append(edge.getOrigId())
        return edgeList


    ## 
    #   Returns all cached Egdes\n
    #   @WARN A Edge added to the GraphicsScene does not necessarily mean it will be painted
    #          This is only a container of items which could be painted when refresh scene is called
    def getEdges(self):
        return self._edges.values()

    ##
    #   Returns the specified cached Node\n
    #   @WARN A Node added to the GraphicsScene does not necessarily mean it will be painted
    #          This is only a container of items which could be painted when refresh scene is called    
    def getNode(self, nid):
        try:
            return self._nodes[nid]
        except:
            logger.debug("getNode id(%s) not in dict" %nid)
            return None

    ##
    #   Returns the specified cached Edge\n
    #   @WARN A Edge added to the GraphicsScene does not necessarily mean it will be painted
    #          This is only a container of items which could be painted when refresh scene is called    
    def getEdge(self, eid):
        try:
            return self._edges[eid]
        except:
            logger.debug("getEdge id(%s) not in dict" %eid)
            return None

    def getEdgeByOrigId(self, oid):
        for edge in self._edges.values():
            if edge.getOrigId() == oid:

                return edge
        return None
        # try:
        #     return self._edges[eid]
        # except:
        #     logger.debug("getEdge id(%s) not in dict" %eid)
        #     return None


    ## 
    #   Sets a node to the NodeSceneDict
    #   @WARN A Node added to the GraphicsScene does not necessarily mean it will be painted
    #          This is only a container of items which could be painted when refresh scene is called
    def setNode(self, nid, node):
        self._nodes[nid] = node
    ## 
    #   Sets a edge to the NodeSceneDict    
    #   @WARN A Edge added to the GraphicsScene does not necessarily mean it will be painted
    #          This is only a container of items which could be painted when refresh scene is called
    def setEdge(self, eid, edge):
        self._edges[eid] = edge
       
    ## 
    #   Returns the count of the cashed nodes 
    def getNodeCount(self):
        return len(self._nodes.values())
    ##
    #   Returns the count of the cashed edges
    def getEdgeCount(self):
        return len(self._edges.values())
    
    ##
    #   Checks if a node already exists in the Model
    def nodeExists(self,nid):
        return nid in self._nodes.keys()

    ##
    #   Checks if a node already exists in the Model
    def edgeExists(self,eid):
        return eid in self._edges.keys()


    ##
    #   Refreshes the scene\n
    #   It clears the scene and then adds the cached edges and then the cached nodes
    def refreshScene(self):
        self.clear()
        self.addItemsToModel(self._edges.values())
        self.addItemsToModel(self._nodes.values())
    ##
    #   Updates the Qgraphicsscene Boundary so you can specify a custom rect for the scene.\n
    #   It is recommended to set the rect symmetrical to the origin. So get the max absolut
    #   value of any SceneItem and use this value for all entries.\n
    #   E.g. if the max position of a node is (-4000.542,5124.0) set bounds to
    #   bounds = [-5124.0, -5124.0, 5124.0, 5124.0] 
    def updateSceneBoundaries(self,bounds):
        self.setSceneRect(bounds[0], bounds[1], bounds[2], bounds[3])

