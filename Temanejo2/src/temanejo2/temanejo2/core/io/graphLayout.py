'''!
@brief Provides a class which acts as an interface to the different layoutroutines

This module provides a class GraphLayout which will act as an interface to the different
layout routines. This provides the opportunity to put multiple layoutroutines into
temanejo which provide some special methods. For an explenation which methods are needed
please refer to the class description

The routine then has to be imported here and integrated into the class. For now only graphviz
is fully integrated.

@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''


import logging
import copy

import networkx as nx
from PySide import QtCore

from temanejo2.temanejo2.lib import configuredValues



logger = logging.getLogger(__name__)

##
# This class acts as an interface to the outer layout routines. In order to integrate a new layout
# routine one needs to provide the following methods on a graph object:
# * getNodePos(nodeId) which returns a nodes position by the given id
# * addDep(fromNode, toNode) which adds a dependency to the graph by the given node ids
# * addNode(nodeId) which adds a node to the graph by the given id
# * layoutGraph(*args) which layouts the graph itself.
#
# The routine needs then to be integrated into the different methods here by adding a new if
# case (with a string based condition) to the different methods.
# Finally the case can be activated by setting the constant EXTERNAL_GRAPH_LAYOUT_ROUTINE in
# temanejo2.lib.configuredValues to the according routine.
class GraphLayout(QtCore.QObject):
    ##
    # Signal in order to trigger that the graph has to layouted
    makeGraphDirty = QtCore.Signal(bool)

    ##
    #   Initializes the different possible layoutroutines.
    #
    #   For now the pygraphvizgraph is fully integrated and therefore the member agraph is initizialised
    #   in the remote control.
    #   All other graphobjects from other layoutroutines will be initialized in here
    #   @param routine For now acceptable values are graphviz and hlrs_custom
    def __init__(self, temanejo, routine=None):
        super(GraphLayout,self).__init__()
        ##@member The keyword of the given routine
        self.__routine = "graphviz" if routine is None else routine
        ##@member The main temanejo object
        self.temanejo = temanejo
        ##@member The pygraphviz graph which will be layouted in case the graphviz routine is active
        self.agraph = self.temanejo.remoteControl.graph
        from style_provider import StyleProvider
        self.layout_routine = self.temanejo.remoteControl.layout_routine

        self.nodestoadd=[]
        self.deptoadd=[]

    ##
    #   Queries the currently active layoutroutine for a node position given by id
    #   and will return it as a list with 2 floating entries
    #   @param nodeId The id of the node for which the position will be queried
    #   @return The nodeposition to the according node given by the id
    def getNodePos(self, nodeId, currentGraph):

        if self.__routine is "graphviz":
            try:
                nodePos = currentGraph.get_node(nodeId).attr["pos"].split(",")
                fl_node_pos = [float(nodePos[0]), float(nodePos[1])]
            except:
                logger.error("Couldn't find the position attribute in the graph")
                fl_node_pos = [0, 0]
            return fl_node_pos

        elif self.__routine is "hlrs_custom":
            #Maiksens fancy stuff
            logger.debug("getNodePos is %s of nodeId %s",
                         currentGraph.getNodePos(nodeId), nodeId)
            return currentGraph.getNodePos(nodeId)

    ##
    #   Adds a dependency to the currently active layout routine
    #   @param fromNode The node id of the parent node
    #   @param toNode The node id of the child node
    def addDep(self, fromNode, toNode, depId):
        with self.temanejo.currently_layouting_lock:
            self.deptoadd.append([fromNode,toNode, depId])
            # The graphvisroutine to be filled
            #self.agraph.add_edge(fromNode, v=toNode)
            # The HLRS Routine to be filled
            #self.layout_routine.addEdge(fromNode, toNode)
        self.makeGraphDirty.emit(False)

    ##
    #   Adds a node to the currently active layout routine
    #   @param nodeId The id of the node which shall be added to the graphobject of the active
    #                 layout routine
    def addNode(self, nodeId):
        with self.temanejo.currently_layouting_lock:
            self.nodestoadd.append(nodeId)
            # The graphvisroutine to be filled
            #self.agraph.add_node(nodeId)
            # The HLRS Routine to be filled
            #self.layout_routine.addNode(nodeId)
        self.makeGraphDirty.emit(False)

    ##
    #   Returns the whole graph...
    def getGraph(self):
        self.update()
        if self.__routine is "graphviz":
            # This works for networkx version smaller than 1.10
            try:
                gr = nx.to_agraph(self.agraph).to_directed()
            # This works at least for networkx version 1.11
            except:
                logger.debug("Using new specs of the networkx lib")
                gr = nx.nx_agraph.to_agraph(self.agraph).to_directed()

            return gr
            #return nx.to_agraph(nx.from_agraph(self.agraph))
        elif self.__routine is "hlrs_custom":
            return self.layout_routine

    def update(self):
        with self.temanejo.currently_layouting_lock:
            nodestoadd_copy = copy.copy(self.nodestoadd)
            deptoadd_copy = copy.copy(self.deptoadd)
            self.nodestoadd=[]
            self.deptoadd=[]

        for nodeId in nodestoadd_copy:
            self.agraph.add_node(nodeId)
            self.layout_routine.addNode(nodeId)

        for fromNode, toNode, depId in deptoadd_copy:
            self.agraph.add_edge(fromNode, toNode, depId)

            self.layout_routine.addEdge(fromNode, toNode)


    ##
    #   Calls the layout method of the currently active layout routine where the graph then will
    #   be layouted
    #   @param routine ONLY suitable for graphviz where different routines can be activated for
    #                  different layout styles
    def layoutGraph(self, graph_to_layout, routine=None):
        if self.__routine is "graphviz":
            graph_to_layout.layout(prog=routine)

        elif self.__routine is "hlrs_custom":
            graph_to_layout.ls.do_layout()

    def setRoutine(self,routine):
        configuredValues.EXTERNAL_GRAPH_LAYOUT_ROUTINE = routine
        self.__routine = routine

    def getRoutine(self):
        return self.__routine

    def getNodes(self, currentGraph):
        if self.__routine is "graphviz":
            return currentGraph.nodes()
            # return self.agraph.nodes()
        elif self.__routine is "hlrs_custom":
            return currentGraph.getNodes()
            # return self.layout_routine.getNodes()

    def getEdges(self, currentGraph):
        if self.__routine is "graphviz":
            return currentGraph.edges()
            # return self.agraph.edges()
        elif self.__routine is "hlrs_custom":
            return currentGraph.getEdges()
            # return self.layout_routine.getEdges()
