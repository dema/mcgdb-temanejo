import logging

import base_style
import hlrsCustomLayout

logger = logging.getLogger(__name__)

##
#     This class us used as an interface to temanejo. As temanejo builds and
#     modifies graph layouts it does that using a defined structure see
#     :class:`StyleProvider` for the actual metaclass.
class HLRSStyle(base_style.BaseStyle):

    def __init__(self, temanejo):
        super(HLRSStyle, self).__init__()
        self.ls = hlrsCustomLayout.HlrsCustomLayout()
        self.ls.register_temanejo_to_graph(temanejo)
        self._nodes_dict = dict()  # custom nodes dictionary for faster access
        self._nodes_dict_is_up_to_date = 0

    def getNodes(self):
        return self.ls.nodes(data=False)

    def getNodePos(self, nodeId):
        if self._nodes_dict_is_up_to_date:
            return self._nodes_dict[int(nodeId)]['pos']
        else:
            self._update_nodes_dict()
            return self._nodes_dict[int(nodeId)]['pos']

    def getEdges(self):
        return self.ls.edges()

    def addEdge(self, fromNode, toNode):
        self._nodes_dict_is_up_to_date = 0
        self.ls.add_edge(int(fromNode), int(toNode))
        #self.ls.do_layout()

    def addNode(self, nodeId):
        self._nodes_dict_is_up_to_date = 0
        self.ls.add_node(int(nodeId), pos=[0, 0])
        #self.ls.do_layout()

    def _update_nodes_dict(self):
        nodes = self.ls.nodes(data=True)
        self._nodes_dict = dict(nodes)
        self._nodes_dict_is_up_to_date = 1

    def reset_graph(self):
        self.ls.clear()
        self._nodes_dict = dict()


