'''!
@brief The connector Module for Temanejo2 - Connects Views to the controller

This is a module where the connectAll method will be held.\n
The Connect all only calls other methods which do specific connections
between models and controller or views and controller etc.  
For now it can only be called by any instance of the temanejo object.
Or any other object which has build up all necessary controller 

@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''

import logging

logger = logging.getLogger(__name__)

##
# This method connects all views to their controller  
# also models to views etc.
def connectAll(temanejo):
    #Connect controller-views
    connectControllerToViewsAndItems(temanejo)
    connectModelsToViews(temanejo)
    #ConnectMenuitemsToActions
    temanejo.menuActionCtrl.connectMenuActions()
    #Connect Misc Actions such as signals and slots
    miscConnections(temanejo)
    
#===============================================================================================
# CONTROLLER CONNECTION PART
#===============================================================================================
##
#   Connect all controller\n
#   Just calls the specific connectors
def connectControllerToViewsAndItems(temanejo):
    connectGraphController(temanejo)
    connectNodeItemController(temanejo)
    connectEdgeItemController(temanejo)

##
#   Connects all controller to the Graphview
def connectGraphController(temanejo):
    #Connect the controller class to the Graphicsview
    temanejo.ui.temanejoGraph.connectController(temanejo.graphUserInputCtrl)
    
##
#
def connectNodeItemController(temanejo):
    items = temanejo.graphModel.getNodes()
    for item in items:
        item.connectController(temanejo.nodeCtrl)
        
##
#    
def connectEdgeItemController(temanejo):
    items = temanejo.graphModel.getEdges()
    for item in items:
        item.connectController(temanejo.edgeCtrl)

##
#
def connectModelsToViews(temanejo):
    temanejo.ui.temanejoGraph.setScene(temanejo.graphModel)
    connectPropertyModelsToviews(temanejo)
    
##
#    
def connectPropertyModelsToviews(temanejo):
    for widget in temanejo.propertyWidgets:
        widget.setModels(temanejo.propSelectionModel, temanejo.propDisplayModel)
    for widget in temanejo.dep_propertyWidgets:
        widget.setModels(temanejo.depPropSelectionModel, temanejo.depPropDisplayModel)

##
#   Connects the RemoteController to the according Object
#   This connections will be setUp in the GraphRemoteController after
#   setting up the connection to the remote.\n
#   This needs to be done in this way unlike the other connections, since
#   a remoteCommunikation can only be set up when the application is 
#   already running...
def connectRemoteControllerToDataParser(temanejo):
    remoteCommunicator = temanejo.remoteControl.rmc
    if remoteCommunicator is not None:
        remoteCommunicator.signalizer.addedTask.connect(temanejo.dataParser.taskAdded)
        remoteCommunicator.signalizer.addedDependency.connect(temanejo.dataParser.depAdded)
        remoteCommunicator.signalizer.addedProperty.connect(temanejo.dataParser.propAdded)
        remoteCommunicator.signalizer.addedUserdata.connect(temanejo.dataParser.userdataAdded)
        
        remoteCommunicator.signalizer.addedUserdata.connect(temanejo.seqDiagramController.userdataAdded)

##
# Connects the ddt Comunicator to the different slots
def connectDDTRemote(temanejo):
    # TODO Define signals and slots for the different ddt events
    temanejo.remoteControl.ddt_communicator.ddt_signals.dtt_communicated.connect(temanejo.remoteControl.ddt_parser.parse_ddt_event)

def miscConnections(temanejo):
    connectPropSelectionModelWithPropViewModel(temanejo)

##
#
def connectPropSelectionModelWithPropViewModel(temanejo):
    for widget in temanejo.propertyWidgets:
        widget.comboBox.currentIndexChanged.connect(widget.setPropRootIndex)
    for widget in temanejo.dep_propertyWidgets:
        widget.comboBox.currentIndexChanged.connect(widget.setPropRootIndex)
