'''!
@brief The basic abstract Item of the graph will be provided here. 

In the GraphItem Module just a basic abstract object is provided in order to get a consistent 
way for processing all Items of the Graph. A Item can be everything from a node or a Edge up to
a set of nodes or a barrier

@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''

import logging

from PySide import QtGui

logger = logging.getLogger(__name__)


##    This class defines a abstract Item where all specific Items in the Graph will be inherited from.
#     All basic attributes, a item must have, are defined in here. Like the items id,status and a 
#     paint Function derived from QGraphicsItem.
class BaseItem(QtGui.QGraphicsItemGroup):

##
#        Nothing specific to do here,
#        except to make sure a node ID is provided when created in here
#        and call superclass constructor so all methods from QGraphicsItem are provided
#        Also adding some basic flags for all items
#        @param nodeId: This should be a unique ID for each node in order to
#                     Identify the node for modifications etc.
#                     This parameter should be a list! Always! This is handled that way
#                     in order to stay consistent, because a Item can be a single Item but also 
#                     a set of Items 
    def __init__(self,nodeId,parent=None):
        #Call superclassconstructor
        QtGui.QGraphicsItemGroup.__init__(self,parent)
        ## @var  __id 
        #   This should be a unique nodeId for each node in order to
        #   Identify the node for modifications etc.
        #   If this Id stays unique is in the responsibility of 
        #   the caller
        self.__id = nodeId
        
        ## @var _status
        #  Helds the status of the node in the graph, if it is processed
        #  or not. This will mainly influence the appearance of the NodeItem  
        #  but also the data Node since all the data shall be held there.\n
        #  <b>Status has 2 values</b>\n
        #  <b>True</b>     If the node is processed\n
        #  <b>False</b>    If the node is not processed yet        
        self._status = False
        
        # Set standard flags to the item
        self.setFlags(QtGui.QGraphicsItem.ItemIsMovable^
                      QtGui.QGraphicsItem.ItemIsFocusable^
                      QtGui.QGraphicsItem.ItemIsSelectable)
        
    #-----------------------------------------------------------------------------------------------
    #--------------- Getter and Setter Section ----------------------------------------------------- 
    #-----------------------------------------------------------------------------------------------    
    
    ##
    #   Returns the (hopefully) unique Id of the item. 
    def getId(self):
        return self.__id
    
    ##
    #   Returns the current status of the item 
    def getStatus(self):
        return self._status
    
    ##
    # Set the current status of the item
    #    @param status  Should be a boolean Value  
    def setStatus(self,new_status):
        self._status = new_status

    #-----------------------------------------------------------------------------------------------
    #--------------- Draw Item Section -------------------------------------------------------------
    #-----------------------------------------------------------------------------------------------    
    def paint(self,painter, option, widget = None):
        raise NotImplementedError()
    
    def boundingRect(self):
        raise NotImplementedError()
