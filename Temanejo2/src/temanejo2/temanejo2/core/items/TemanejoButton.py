'''!
@brief The basic Button is implemented here

This Module Provides a Standard Button for any Button added onto the GUI\n  
All custom redirections are created in here.

@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''
import logging

from PySide import QtCore, QtGui

logger = logging.getLogger(__name__)


##
#   This class defines a button used in the GUI it provides a method to set Icons
#   which shouldn't be used from out of this object except to visualize any change
#   of the functionality of the button, e.g. block the play button because there is no 
#   remote connection. There could be a special item added.
class TemanejoButton(QtGui.QPushButton):
        
    ## Initializes the button\n
    #  Just calls the baseClassConstruktor and calls the setIcon Method on the button
    #  @var icon_path: The path to the according Icon
    def __init__(self,icon_path,text=None,tool_tip=None,parent=None):
        QtGui.QPushButton.__init__(self,parent)
        self.setIcon(icon_path)
        self._text = text
        self._tool_tip = tool_tip
        self.setTooltip(tool_tip)
        self.setIconSize(QtCore.QSize(30,30))
        self.setMinimumSize(40, 40)
        self.setMaximumSize(40, 40)

        self.setSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)

    # -----------------------------------------------------------------------------------------------
    # --------------- Getter and Setter Section -----------------------------------------------------
    # -----------------------------------------------------------------------------------------------
        
    def setIcon(self,icon_path):
        self._icon = QtGui.QIcon(icon_path)
        super(TemanejoButton,self).setIcon(self._icon )
        
    def getIcon(self):
        return self._icon;
    
    def setTooltip(self, tool_tip):
        super(TemanejoButton, self).setToolTip(tool_tip)
        self._tool_tip = tool_tip
    
    def getToolTip(self):
        return self._tool_tip
    
    def getText(self):
        return self._text
    
    def setText(self,text):
        super(TemanejoButton,self).setText(text)
        self._text = text
    
    ##
    #   Override of the paintEvent in order to be able to apply a stylesheet
    def paintEvent(self, evt):
        super(TemanejoButton,self).paintEvent(evt)
        opt = QtGui.QStyleOption()
        opt.initFrom(self)
        p = QtGui.QPainter(self)
        s = self.style()
        s.drawPrimitive(QtGui.QStyle.PE_Widget, opt, p, self)
