'''!
@brief This module provides some Funktions for customizing the standard dialogs

@todo: Make description

@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''
import logging
from functools import partial
from PySide import QtCore, QtGui




logger = logging.getLogger(__name__)


def saveFileDialog(filter=None):
    filename = QtGui.QFileDialog.getSaveFileName(filter=filter)
    if len(str(filename[0]))==0:
        logger.debug("Cancelled FileDialog returning None")
        return None
    return str(filename[0])

def loadFileDialog(filter=None):
    filename = QtGui.QFileDialog.getOpenFileNames(filter=filter)
    if len(filename[0]) == 0:
        logger.debug("Cancelled FileDialog returning None")
        return None
    return filename[0]



def colordialog():
    colour_chooser = QtGui.QColorDialog()
    colour_chooser.blockSignals(True)
    liveColor = colour_chooser.getColor()
    colour_chooser.blockSignals(False)
    if liveColor.isValid():
        return liveColor
    return None

def textInputDialog():
    inputDialog = QtGui.QInputDialog()
    inputDialog.blockSignals(True)
    text = inputDialog.getText(None, "Choose Label for the Node", "Set Label")
    inputDialog.blockSignals(False)
    return text[0]

def messageDialog(message,title="Message"):
    messageDialog = QtGui.QMessageBox()
    messageDialog.setWindowTitle(title)
    temIcon = QtGui.QPixmap(':images/temanejo_logo.png')
    temIcon = temIcon.scaledToWidth(150, mode=QtCore.Qt.SmoothTransformation)
    messageDialog.setIconPixmap(temIcon)
    messageDialog.setText(message)
    messageDialog.exec_()

##
#  This object creates a sliderDialog which interactivly communicates
#  Provide the object a method and it will be called whenever the value of the slider is
#  changed
class SliderDialog(QtGui.QDialog):

    def __init__(self,label,actionmethod,parent=None):
        super(SliderDialog,self).__init__(parent)
        ##@member The label Displayed on the Dialog
        self.label = label
        ##@member The Method which is connected to the slot valueChanged of the Slider on the dialog
        self.actionmethod = actionmethod

        #Setup the dialog
        self.vboxlayout = QtGui.QVBoxLayout()
        self.setLabel()
        self.setSlider()
        self.setExitButton()
        self.setLayout(self.vboxlayout)
        self.setModal(False)

    def setLabel(self):
        self.vboxlayout.addWidget(QtGui.QLabel(self.label))

    def setSlider(self):
        # The slider
        slider = QtGui.QSlider(QtCore.Qt.Orientation.Horizontal)
        slider.setTickPosition(QtGui.QSlider.TicksBelow)
        slider.valueChanged.connect(partial(self.actionmethod))
        self.vboxlayout.addWidget(slider)

    def setExitButton(self):
        exitButton = QtGui.QPushButton("Close")
        exitButton.pressed.connect(self.exitTheDialog)
        self.vboxlayout.addWidget(exitButton)

    def exitTheDialog(self):
        self.setVisible(False)
        self.destroy()

class ConnectionDialog(QtGui.QDialog):


    def __init__(self, parent=None, label="Open Server (Port)"):
        super(ConnectionDialog,self).__init__(parent)
        ##@member The label Displayed on the Dialog
        self.label = label
        self.vboxlayout = QtGui.QVBoxLayout()

        #Set the Label
        self.vboxlayout.addWidget(QtGui.QLabel(self.label))

        #Set text input
        self.portField = QtGui.QLineEdit()
        self.vboxlayout.addWidget(self.portField)

        #Set the ConnectionButton
        self.vboxlayout.addWidget(self.connectButton())

        #@member The port to connect to
        self.__connPort = 0

        #Set Layout
        self.setLayout(self.vboxlayout)

        self.setModal(False)

    ##
    #   Creates the Connectbutton.\n
    #   The Connectbutton
    #   - reads the QLineEdit Input,
    #   - saves it into the __connPort member
    #   - and finally closes the dialog
    def connectButton(self):
        exitButton = QtGui.QPushButton("Open")
        exitButton.pressed.connect(self.exitTheDialog)
        return exitButton

    ##
    #   The ActionMethod for the connectButton which handles all
    #   the stuff what need to be done by pressing the connectButton
    def exitTheDialog(self):
        self.__connPort = self.portField.text()

        if(self.__connPort.isdigit()):
            if (int(self.__connPort) < 65535 and int(self.__connPort) > 1024):
                self.setVisible(False)
                self.destroy()
            else:
                message = "Couldn't open server on port %s because \nthe given port is invalid. Only numbers\n between 1024 and 65535 are allowed"%self.__connPort
                title = "Connection Error"
                messageDialog = QtGui.QMessageBox()
                messageDialog.setWindowTitle(title)
                temIcon = QtGui.QPixmap(':images/temanejo_logo.png')
                temIcon = temIcon.scaledToWidth(150, mode=QtCore.Qt.SmoothTransformation)
                messageDialog.setIconPixmap(temIcon)
                messageDialog.setText(message)
                messageDialog.exec_()


        else:
            message = "Couldn't open server because \nthe given value is not a number\n"
            title = "Connection Error"
            messageDialog = QtGui.QMessageBox()
            messageDialog.setWindowTitle(title)
            temIcon = QtGui.QPixmap(':images/temanejo_logo.png')
            temIcon = temIcon.scaledToWidth(150, mode=QtCore.Qt.SmoothTransformation)
            messageDialog.setIconPixmap(temIcon)
            messageDialog.setText(message)
            messageDialog.exec_()

    ##
    #   Returns the given Port
    def connPort(self):
        return int(self.__connPort)
