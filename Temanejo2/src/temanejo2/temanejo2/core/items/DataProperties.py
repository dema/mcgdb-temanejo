'''!
@brief 



@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''
import logging

logger = logging.getLogger(__name__)


##
#
class DataProperty(object):
    
    ##
    #  @param name The name of the property
    #  @param attributes The attributes of the property
    def __init__(self, name, attributes=None, p_type=None):
        self.__name       = name
        self.__attributes = "Not defined" if attributes is None else attributes
        self.type         = p_type
        
    ##
    #   Returns the name of the property
    def name(self):
        return self.__name
    
    ##
    #   Returns the attributes of the property
    def attributes(self):
        return self.__attributes
