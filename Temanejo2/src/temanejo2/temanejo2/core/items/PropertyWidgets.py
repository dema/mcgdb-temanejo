'''!
@brief All widgets which will display any kind of properties are defined in here.

There will be several widgets needed in order to display and properties, as also maipulate the 
graph appearance based on properties.\n
In order to get this done different widgets will be defined in here and will be connected to the 
according actions which are needed
 
@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''

import logging

from PySide import QtGui, QtCore

from temanejo2.temanejo2.lib import configuredValues
from temanejo2.temanejo2.view.view_items import propertyTreeView

logger = logging.getLogger(__name__)
##
#  This class will generate a widget which will give the user the ability to manipulate the   
#  graphAppearance based on properties... 
class PropertyDisplayWidget(QtGui.QWidget):

    ##
    #   Initialization on the propertyDisplayWidget
    def __init__(self,temanejo, wid_type, parent=None, labelText="Not set", tool_tip=""):
        QtGui.QWidget.__init__(self,parent)
        self.labelText = labelText
        self.temanejo = temanejo
        self.comboBoxModel = None
        self.listOfTableModel = None
        self.initUi()
        self.setTooltip(tool_tip)
        self._wid_type = wid_type

    def setTooltip(self, tool_tip):
        super(PropertyDisplayWidget, self).setToolTip(tool_tip)

    ##
    #   Initializes the UiWidgets which will be placed on this widget
    def initUi(self):
        self.label = QtGui.QLabel(text=self.labelText)

        self.comboBox = QtGui.QComboBox()
        self.treeview = propertyTreeView.PropertyTreeView(self.temanejo, self)

        headLayout = QtGui.QHBoxLayout()
        headLayout.addWidget(self.label)
        headLayout.addWidget(self.comboBox)

        baseLayout = QtGui.QVBoxLayout()
        baseLayout.addItem(headLayout)
        baseLayout.addWidget(self.treeview)

        self.setLayout(baseLayout)

    ##
    #
    def setModels(self, comboBoxModel, loTModel):
        # Set Handle to models
        self.comboBoxModel = comboBoxModel
        self.listOfTableModel = loTModel
        #Set models to the views
        self.treeview.setModel(loTModel)
        self.comboBox.setModel(comboBoxModel)

    ##
    #   Set the root index of the property values tree view to the root index of the 
    #   property so all childs are sorted in correctly\n
    #   Furthermore the coloring of the items needs to be processed here so every row gets a 
    #   different color and therefore the nodes will get the correct color and shape according to
    #   their property
    def setPropRootIndex(self, ind):
        index = self.listOfTableModel.index(ind, 0)
        #run through all childs and set a color
        # The try except claues is due to initialisation process since there is nothing selected
        # for the time the first property is parsed we only catch the indexerror in case a 
        # unused index is caught. This can only happen if there are no properties sent yet,
        # since there is no index then available in the treeview 

        #self.temanejo.remoteControl.layoutCtrl.layoutPgvGraph()
        try:
            propRootItem = self.listOfTableModel.propertyRootItems[ind]
            self.changeNodeDisplayPropAccordingToItemProp(propRootItem)
        except(IndexError):
            logger.error("Index %s was not found in Roots of the treemodels"%ind)

        self.treeview.setRootIndex(index)

    def enforceUpdate(self):
        # get Currently selected index of the combobox
        currInd = self.comboBox.currentIndex()
        #check if there exists a property at all
        if currInd < 1:
            return
        self.comboBox.setCurrentIndex(0)
        self.comboBox.setCurrentIndex(currInd)


    ##
    #   Updates the nodeItem Appereance eg. the color according to the property Value
    #TODO REFAAAACTOR THIS SPLIT INTO DEP AND NODE
    def changeNodeDisplayPropAccordingToItemProp(self, propRootItem):
        colors = self.temanejo.colorPalette
        listOfColoredItems = []
        for i, item in enumerate(propRootItem.childItems):
            #Get the items to the propval
            itemIds = item.itemdata().internal_data()[1]
            listOfColoredItems.extend(itemIds)#[int(iitem) for iitem in itemIds])
            #create Color for Propval
            fpColor = colors[i%configuredValues.NUM_OF_DIFFERENT_COLORS]
            colorToPropValue = QtGui.QColor()
            colorToPropValue.setRgbF(fpColor[0],fpColor[1],fpColor[2])
            #get shape for Propval
            shape = self.temanejo.shapePalette[i%len(self.temanejo.shapePalette)]
            item.itemdata().internal_data()[-1] = colorToPropValue
            for itemId in itemIds:
                node = self.temanejo.graphModel.getNode(int(itemId))
                if node is not None:
                    if self._wid_type == "n_col":
                        node.setNodeColor(color=colorToPropValue)
                    elif self._wid_type == "s_col":
                        node.setMarginColor(color=colorToPropValue)
                    elif self._wid_type == "shape":
                        node.setShape(shape)
                    continue
                else:
                    edge = self.temanejo.graphModel.getEdgeByOrigId(int(itemId))
                    if self._wid_type == "d_col" and edge is not None:
                        edge.setEdgeColor(color=colorToPropValue)

        listOfNotFoundNodes = list(set(self.temanejo.ui.temanejoGraph.scene().getNodeKeys()) - set(map(int, listOfColoredItems)))
        for itemId in listOfNotFoundNodes:
            node = self.temanejo.graphModel.getNode(itemId)
            if node is not None:
                if self._wid_type == "n_col":
                    node.setNodeColor(color=QtCore.Qt.lightGray)
                elif self._wid_type == "s_col":
                    node.setMarginColor(color=QtCore.Qt.black)
                elif self._wid_type == "shape":
                    node.setShape('circle')
            else:
                print "Node not found"

        listOfNotFoundEdges = list(set(self.temanejo.ui.temanejoGraph.scene().getEdgeKeysOrigIds()) - set(map(int, listOfColoredItems)))
        for itemId in listOfNotFoundEdges:
            edge = self.temanejo.graphModel.getEdgeByOrigId(itemId)
            if edge is not None:
                if self._wid_type == "d_col":
                    edge.setEdgeColor(color=QtCore.Qt.blue)

        # After processing all nodes update the viewport
        self.temanejo.ui.temanejoGraph.viewport().update()



    ##
    #   Override of the paintEvent in order to be able to apply a stylesheet
    def paintEvent(self, evt):
        super(PropertyDisplayWidget,self).paintEvent(evt)
        opt = QtGui.QStyleOption()
        opt.initFrom(self)
        p = QtGui.QPainter(self)
        s = self.style()
        s.drawPrimitive(QtGui.QStyle.PE_Widget, opt, p, self)
