'''!
@brief This module provides classes for the preferences Dialog

In here the logic to the ui_preferences.py is written. Additionally the custom tabs for
the different prefferencegroups are defined here.

@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''

from PySide import QtCore, QtGui

from temanejo2.temanejo2.view import preferences
from temanejo2.temanejo2.lib import resources


##
# @todo Make Description
class PreferencesDialog(QtGui.QDialog):
    ##
    # Initialize the dialog
    def __init__(self, temanejo, parent=None):
        super(PreferencesDialog, self).__init__(parent)
        self.parent = parent
        self.temanejo = temanejo
        self.dialog_ui = preferences.Ui_Preferences()
        self.dialog_ui.setupUi(self)
        self.modify_ui_startup()
        self.show()

        self.prefLayout = QtGui.QGridLayout()
        self.dialog_ui.categories.currentItemChanged.connect(self.prefchange_slot)


    # Modify the startup
    def modify_ui_startup(self):
        # Create the list of categories
        for section in self.temanejo.configParser.sections():
            self.dialog_ui.categories.addItem(CathegoryWidget(section, parent=self.dialog_ui.categories))

        # Set the width of the listwidget
        self.dialog_ui.categories.setMaximumWidth(150)

    def prefchange_slot(self):
        actCategory = ActualCategory(self.temanejo.configParser)
        actCategory.set_category_identifier(self.dialog_ui.categories.currentItem().text())
        actCategory.construct_actual_widget()

        self.dialog_ui.categories.currentItem().focus_gained(self.dialog_ui, actCategory)


class CathegoryWidget(QtGui.QListWidgetItem):

    def __init__(self, text, parent=None):
        super(CathegoryWidget, self).__init__(parent)
        self.setText(text)

    # Basically Sets the defined layout of the specific cathegory
    def focus_gained(self, dialog_ui, layout):
        catWid = QtGui.QWidget()
        dialog_ui.scrollArea.setWidget(catWid)
        catWid .setLayout(layout)


class ActualCategory(QtGui.QVBoxLayout):

    def __init__(self, configParser, parent=None):
        super(ActualCategory, self).__init__(parent)
        self.categoryIdentifier = None
        self._configParser = configParser


    def set_category_identifier(self, identifier):
        self.categoryIdentifier = identifier

    def construct_actual_widget(self):
        list_of_items_to_set = []
        for (pref_name, pref_val) in self._configParser.items(self.categoryIdentifier):
            list_of_items_to_set.append(CustomPropertyItem(pref_name, custom_type=type(pref_val)))
        for item in list_of_items_to_set:
            self.addWidget(item.get_layouted_item())
##
#
class CustomPropertyItem(QtGui.QWidget):

    ##
    #
    def __init__(self, name, custom_type=None, init_value=None, parent=None):
        super(CustomPropertyItem, self).__init__(parent)

        self.label = QtGui.QLabel(name)
        self.inputWidget = self.generate_custom_input_widget(custom_type, init_value)
        self.layout = QtGui.QHBoxLayout()
        self.layout.addWidget(self.label)

        if self.inputWidget is not None:
            self.layout.addWidget(self.inputWidget)
        self.setLayout(self.layout)
    ##
    #
    def generate_custom_input_widget(self, custom_type, init_value=None):
        if custom_type == None or custom_type == 'number':
            return QtGui.QLineEdit(text=init_value)
        return QtGui.QLineEdit()# QtGui.QLabel("The type %s has no representation yet"%custom_type)

    def get_layouted_item(self):
        return self