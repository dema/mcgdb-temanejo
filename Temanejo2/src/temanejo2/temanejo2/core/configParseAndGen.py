'''!
@brief The user configuration of Temanejo will be read and written with the functions provided in this module



@author: Stefan Reinhardt
@contact temanejo@hlrs.de
'''

import os
import ast
import ConfigParser
import logging

import temanejo2.temanejo2.lib.configuredValues as cfgVals

logger = logging.getLogger(__name__)

##
# Looks for the correct path to the config file\n
# The Priorities are:
# 1. Commandline Argument\n
# 2. $Home/.Temanejo/temconf.cfg\n
# 3. A not yet specified sitewide dir\n
#
# If there ist not found any configfile in these places the standardvalues defined in lib/configuredValues.py
# will be used
def getConfigFilePaths(cmdArgs):

    #TODO Look for site wide path and query it in here
    fileLocations = []
    for loc in cmdArgs.configpath, os.curdir, os.path.join(os.path.expanduser("~"), '.Temanejo'):
        if os.path.isfile(os.path.join(loc, 'temconf.cfg')):
            fileLocations.append(os.path.join(loc, 'temconf.cfg'))

    # If there is no configfile in any place then a empty string is returned and the standard variables are used
    return fileLocations


##
# Does read in the config files found at the paths
def readConfig(paths):
    parser = ConfigParser.SafeConfigParser()
    temp_parser_dict = {}
    # First read in all cfg Files and store the values to the configuredValues Attributes
    for path in paths:
        tmp_parser = ConfigParser.SafeConfigParser()
        temp_parser_dict[path] = tmp_parser
        if len(tmp_parser.read(path)) == 0:
            logger.info("No Configfile was found at %s\n"%path)
            return None

        allArgs = []
        for section in parser.sections():
            allArgs.extend(parser.items(section))

        for (key, val) in allArgs:
            if hasattr(cfgVals, key.upper()):
                try:
                    setattr(cfgVals, key.upper(), ast.literal_eval(val))
                # IF it fails the value should be parsed as a string
                except:
                    setattr(cfgVals, key.upper(), val)
                    logging.warning("The configvalue %s was set as a string"%key.upper())
            else:
                logger.error("The configuration Attribute %s does not exist"%key)
        logger.info("The user config at %s was loaded"%path)
    # Now set all the configurations to the combined parser for handling the values at Runtime

    for cfg in temp_parser_dict.values():
        for section in cfg.sections():
            if not parser.has_section(section):
                parser.add_section(section)
            for (key, val) in cfg.items(section):
                parser.set(section, key, val)

    return parser


##
#
def writeConfig(paths, overallParser):
    temp_parser_dict = {}

    # First read in all cfg Files and store the values to the configuredValues Attributes
    for path in paths:
        tmp_parser = ConfigParser.SafeConfigParser()
        if len(tmp_parser.read(path)) == 0:
            continue
        temp_parser_dict[path] = tmp_parser

    # Run through all sections and keys of the overall parser
    for section in overallParser.sections():
        for (key, val) in overallParser.items(section):
            # Run through all parsers backwards and override the value if found but only in the first found file
            # so the accoriding value is only overridden in the last file which would be read in
            for path in paths[::-1]:
                currPars = temp_parser_dict[path]
                if currPars.has_option(section, key):
                    currPars.set(section, key, val)
                    break
    # Now write out all files
    for path in paths:
        with open(path, 'wb') as cfg_file:
            temp_parser_dict[path].write(cfg_file)
        print "Writing config to %s"%path