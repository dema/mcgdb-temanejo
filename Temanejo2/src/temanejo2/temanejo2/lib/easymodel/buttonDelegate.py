'''!
@brief 


@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''
import logging
from functools import partial

from PySide import QtGui

from temanejo2.temanejo2.core.items.miscDialogs import colordialog
from temanejo2.temanejo2.lib.easymodel.widgetdelegate import WidgetDelegate

logger = logging.getLogger(__name__)




class ButtonDelegate(WidgetDelegate):
    
    def __init__(self, temanejo, parent=None):
        """
        :param parent: the parent object
        :type parent: QObject
        :raises: None
        """
        self.temanejo = temanejo
        super(ButtonDelegate, self).__init__( parent)
        self.parentWidget = parent
        
    def create_widget(self, parent=None):
        """Return a widget that should get painted by the delegate
        You might want to use this in :meth:`WidgetDelegate.createEditor`
        :returns: The created widget | None
        :rtype: QtGui.QWidget | None
        :raises: None
        """
        #Get the current View on which the delegate is displayed
        viewLabel = None
        try:
            viewLabel = self.parentWidget.labelText
        except:
            pass
        #Create the widget
        button = ButtonWidget(self.temanejo, viewLabel, parent=parent)
        button.setText("Change Color")
        return button
    
    def set_widget_index(self, index):
        """Set the index for the widget. The widget should retrieve data from the index and display it.
        You might want use the same function as for :meth:`WidgetDelegate.setEditorData`.
        :param index: the index to paint
        :type index: QtCore.QModelIndex
        :returns: None
        :rtype: None
        :raises: None
        """
        self.widget.set_index(index)
        
    def create_editor_widget(self, parent, option, index):
        """Return the editor to be used for editing the data item with the given index.
        Note that the index contains information about the model being used.
        The editor's parent widget is specified by parent, and the item options by option.
        :param parent: the parent widget
        :type parent: QtGui.QWidget
        :param option: the options for painting
        :type option: QtGui.QStyleOptionViewItem
        :param index: the index to paint
        :type index: QtCore.QModelIndex
        :returns: None
        :rtype: None
        :raises: None
        """
        self.set_widget_index(index)
        return self.create_widget(parent)
    
    def setEditorData(self, editor, index):
        """Sets the contents of the given editor to the data for the item at the given index.
        Note that the index contains information about the model being used.
        :param editor: the editor widget
        :type editor: QtGui.QWidget
        :param index: the index to paint
        :type index: QtCore.QModelIndex
        :returns: None
        :rtype: None
        :raises: None
        """
        editor.set_index(index)
        
    def createEditor(self, parent, option, index):
        """Return the editor to be used for editing the data item with the given index.
        Note that the index contains information about the model being used.
        The editor's parent widget is specified by parent, and the item options by option.
        :param parent: the parent widget
        :type parent: QtGui.QWidget
        :param option: the options for painting
        :type option: QtGui.QStyleOptionViewItem
        :param index: the index to paint
        :type index: QtCore.QModelIndex
        :returns: The created widget | None
        :rtype: :class:`QtGui.QWidget` | None
        :raises: None
        """
        self._edit_widget = self.create_editor_widget(parent, option, index)
        if self._edit_widget:
            self._edit_widget.destroyed.connect(self.editor_destroyed)
        return self._edit_widget

    def editor_destroyed(self, *args, **kwargs):
        """Callback for when the editor widget gets destroyed. Set edit_widget to None
        :returns: None
        :rtype: None
        :raises: None
        """
        self._edit_widget = None
        
        
class ButtonWidget(QtGui.QPushButton):
    """A widget to display comments
    """
    def __init__(self, temanejo, viewLabel, parent=None):
        """Create a new CommentWidget
        :param parent: widget parent
        :type parent: QtGui.QWidget
        :raises: None
        """
        self.note = None
        super(ButtonWidget, self).__init__(parent)
        self.setAutoFillBackground(True)
        self.clicked.connect(lambda:temanejo.graphAppearanceEventHandler.changeNodeColorOnPropDep(self.note, viewLabel))
        
    def set_index(self, index):
        """Display the data of the given index
        :param index: the index to paint
        :type index: QtCore.QModelIndex
        :returns: None
        :rtype: None
        :raises: None
        """
        item = index.internalPointer()
        self.note = item.internal_data()
