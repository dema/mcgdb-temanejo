'''!
@brief configuredValues will be stored in here

All configuredValues used in Temanejo are defined here.\n
This means all keyboard modifiers like move widget or similar.
Also zoomfactor etc. are defined in here so for modifications only the according variable has to be
overridden...

!!! NOTE !!!
!!! If a new attribute is added and shall be saved in the custom config it also has to be added into the __all__ dict !!!
!!!!!!!!!!!

@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''
__all__ = ['ABOUT_STRING', 'ACCEPTABLE_SHAPES', 'DEFAULT_COORD_DIRECTION', 'DEFAULT_EDGE_WIDTH',
           'DEFAULT_MAGRIN_SEP_FACTOR', 'DEFAULT_MARGINSIZE', 'DEFAULT_NODESIZE', 'DEFAULT_SEP_FACTOR',
           'DEFAULT_ZOOM_FACTOR', 'DISTANCE_CURVED_EDGE_MULT', 'DISTANCE_STRAIGHT_EDGE_MULT', 'EDGE_LABEL_SHIFT_MULT',
           'FACTOR_FOR_DETAIL_APPEARANCE', 'LAYOUT_DIRECTION_DOT', 'MOVE_SCENE_MULTIPLICATOR', 'MOVE_WIDGET',
           'NUM_OF_DIFFERENT_COLORS', 'NUM_OF_STEPS_FOR_FAST_FORWARD', 'PROP_DISPLAY_IGNORE_LIST',
           'SELECT_ITEM_NOT_SCENE', 'SQRT_THREE', 'STANDARD_LAYOUT_ROUTINE', 'STRAIGHT_EDGES', 'ZOOM_FACTOR']


import math

from PySide import QtCore

# ==================================================================================================
# ====== Shape Modifier ============================================================================
# ==================================================================================================
DISTANCE_CURVED_EDGE_MULT = 2.0
DISTANCE_STRAIGHT_EDGE_MULT = 3.0
EDGE_LABEL_SHIFT_MULT = 10.0
STRAIGHT_EDGES = True
SHOW_MULTI_EDGES = False
SHOW_EDGE_LABELS = False


# ==================================================================================================
# =====  Property Mapper ===========================================================================
# ==================================================================================================
PROP_DISPLAY_IGNORE_LIST = ["id", "depId", "None", "clientId", "paused", "scopeClient"]


# ==================================================================================================
# =====  Factors  ==================================================================================
# ==================================================================================================
ZOOM_FACTOR = 1.2
FACTOR_FOR_DETAIL_APPEARANCE = 0.2
DEFAULT_ZOOM_FACTOR = 1.0


# ==================================================================================================
# ===== Keyboard Modifiers =========================================================================
# ==================================================================================================
MOVE_SCENE_MULTIPLICATOR = QtCore.Qt.Key_Shift
MOVE_WIDGET = QtCore.Qt.Key_Control

SELECT_ITEM_NOT_SCENE = QtCore.Qt.Key_X

# ==================================================================================================
# ===== Graphlayout configuredValues ===============================================================
# ==================================================================================================
STANDARD_LAYOUT_ROUTINE = "hlrs_custom"
LAYOUT_DIRECTION_DOT = "BT"
DEFAULT_EDGE_WIDTH = 2
DEFAULT_NODESIZE = 40
DEFAULT_MARGINSIZE = 5
DEFAULT_MAGRIN_SEP_FACTOR = 1.5
DEFAULT_SEP_FACTOR = 4
# Use 1 if Coord system is from left BOTTOM to right TOP
# use -1 if Coord system is from left TOP to right BOTTOM
DEFAULT_COORD_DIRECTION = -1

NUM_OF_DIFFERENT_COLORS = 256
ACCEPTABLE_SHAPES = ['circle', 'box', 'trapezium', 'invtrapezium', 'diamon',
                     'triangle', 'invtriangle', 'pentago', 'invpentago', 'pentagra', 'varpentagra',
                     'hexago', 'house', 'invhouse']  # 'arrow'

# ==================================================================================================
# ===== Graphaction configuredValues ======================================================================
# ==================================================================================================
NUM_OF_STEPS_FOR_FAST_FORWARD = 10

# ==================================================================================================
# ===== Math configuredValues =============================================================================
# ==================================================================================================
SQRT_THREE = math.sqrt(3)


# ==================================================================================================
# ===== Misc Information ===========================================================================
# ==================================================================================================
ABOUT_STRING = u"""Temanejo v2.0.1
Created by: HRLS of the University of Stuttgart

\u00a9 HLRS 2014
"""
