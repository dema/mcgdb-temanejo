__author__ = 'Stefan Reinhardt'

'''!
@brief Import/Export Actions are defined here, as well as starting and communicating with external apps like ddd

Different importer and exporter are written in here

@author: Stefan Reinhardt
@contact temanejo@hlrs.de
'''

import logging
import os
import os.path
import subprocess

import pygraphviz as pvg
from PySide import QtGui

from temanejo2.temanejo2.core.items import miscDialogs, DataEdge, DataNode
from temanejo2.temanejo2.lib import configuredValues
from temanejo2.temanejo2.controller.graph.remoteControls import helpers


logger = logging.getLogger(__name__)



class GraphImporterAndExportEvents(object):

    def __init__(self, temanejo):
        self.temanejo = temanejo

    # ========================================================================
    # ========================== Export Section ==============================
    # ========================================================================
    def exportScreenshot(self):
        # Open Filechooser and get filelocation and name
        logger.debug("Opening Filechooser")
        fileName = miscDialogs.saveFileDialog(filter="*.svg")
        if fileName is None:
            logger.debug("Operation Cancelled")
            self.temanejo.statusBar().showMessage("Screenshot was cancelled")
            return
        if not os.path.splitext(fileName)[-1] == ".svg":
            fileName = fileName + ".svg"
        logger.debug("Got name of file: %s", fileName)
        # If ok was pressed save the graph to a dot file
        logger.debug("Generating and saving svg file")
        self.temanejo.render_temanejo_to_svg(fileName)
        logger.debug("Saved svg")
        self.temanejo.statusBar().showMessage("Screenshot was saved to: %s" % fileName)


    def exportToDot(self):
        #Open Filechooser and get filelocation and name
        logger.debug("Opening Filechooser")
        fileName = miscDialogs.saveFileDialog(filter ="*.dot")
        if fileName is None:
            logger.debug("Operation Cancelled")
            self.temanejo.statusBar().showMessage("Saving dotfile was cancelled")
            return
        if not os.path.splitext(fileName)[-1] == ".dot":
            fileName = fileName + ".dot"
        logger.debug("Got name of file: %s",fileName)
        #If ok was pressed save the graph to a dot file
        logger.debug("Saving dot file")
        self.temanejo.graphActionEventHandler._copy_graph_and_add_attributes_to_graph(self.temanejo.remoteControl.graph).write(fileName)
        logger.debug("Saved dotfile")
        self.temanejo.statusBar().showMessage("Dotfile was saved to: %s"%fileName)

    def exportToPdf(self):
        fileName = miscDialogs.saveFileDialog(filter = "*.pdf")
        if fileName is None:
            logger.debug("Operation Cancelled")
            self.temanejo.statusBar().showMessage("Saving pdf was cancelled")
            return
        if not os.path.splitext(fileName)[-1] == ".pdf":
            fileName = fileName + ".pdf"

        logger.debug("Printing PDF")
        printer = QtGui.QPrinter()
        printer.setPageSize(QtGui.QPrinter.A4)
        printer.setOutputFormat(QtGui.QPrinter.PdfFormat)
        # printer.setOrientation(QtGui.QPrinter.Portrait);
        printer.setOutputFileName(fileName)
        paint = QtGui.QPainter()
        if not paint.begin(printer):
            logger.debug("Something wen't wrong in producing a pdf")
            return
        self.temanejo.ui.temanejoGraph.render(paint)
        paint.end()
        logger.debug("Done Printing PDF")
        self.temanejo.statusBar().showMessage("Pdf was saved to: %s"%fileName)

    def exportToPng(self):
        fileName = miscDialogs.saveFileDialog(filter = "*.png")
        if fileName is None:
            logger.debug("Operation Cancelled")
            self.temanejo.statusBar().showMessage("Saving png was cancelled")
            return
        if not os.path.splitext(fileName)[-1] == ".png":
            fileName = fileName + ".png"

        self.temanejo.graphModel.setSceneRect(self.temanejo.graphModel.itemsBoundingRect())
        pixmap = QtGui.QPixmap.grabWidget(self.temanejo.ui.temanejoGraph)
        pixmap.save(fileName)

        #image = QtGui.QImage(fileName)
        #painter = QtGui.QPainter(image)
        # painter.setRenderHint(QtGui.QPainter.Antialiasing)
        #self.temanejo.ui.temanejoGraph.render(painter)
        #image.save(fileName)

        #self.temanejo.graphModel.clearSelection()
        #self.temanejo.graphModel.setSceneRect(self.temanejo.graphModel.itemsBoundingRect())
        #image = QtGui.QImage(self.temanejo.graphModel.sceneRect().size().toSize(), QtGui.QImage.Format_ARGB32)
        #image.fill(QtCore.Qt.transparent)
        #painter = QtGui.QPainter(image)
        #self.temanejo.graphModel.render(painter)
        #image.save(fileName)

    # ========================================================================
    # ========================== Import Section ==============================
    # ========================================================================
    # ========================================================================
    # ========================== Import Section ==============================
    # ========================================================================

    def openFile(self,filetype):
        logger.debug("Closing open remoteconnections")
        self.temanejo.remoteControl.disconnectFromRemote()
        logger.debug("Deleting previous graph")
        self.temanejo.graphAppearanceEventHandler.clearAllModels(self.temanejo)
        logger.debug("Opening Filechooser")
        # Open Dialog and Choose File

        fileNameList = miscDialogs.loadFileDialog(filter="*."+filetype)
        # Check if okaybutton was pressed
        if fileNameList is None:
            self.temanejo.statusBar().showMessage("Cancelled open graph from file")
            return 0
        # Check if more than one file was selected
        if(len(fileNameList)>1):
            self.temanejo.statusBar().showMessage("Please select only one file. Multiple fileload is not supported for now")
            logger.error("Please select only one file. Multiple fileload is not supported for now")
            return 0
        fileName = fileNameList[0]
        logger.debug("Got name of file: %s", fileName)

        return fileName

    def importFromDot(self):
        fileName = self.openFile("dot")
        if (fileName == 0):
            return

        logger.debug("Loading Graph")
        # Add all the stuff to the graph
        aGraphToReadIn = pvg.agraph.AGraph(fileName, rankdir=configuredValues.LAYOUT_DIRECTION_DOT)
        datanodes = self.temanejo.remoteControl.datanodes
        dataedges = self.temanejo.remoteControl.dataedges
        #Parse Node
        for i in aGraphToReadIn.nodes():
            nodeAttributes = aGraphToReadIn.get_node(i).attr
            nodeAttributes["status"] = False
            nodeAttributes["paused"] = False
            nodeAttributes["taskId"] = i
            self.temanejo.remoteControl.datanodes[int(i)] = DataNode.DataNode(int(i), None, properties=nodeAttributes)
            self.temanejo.graphlayoutIO.addNode(i)
            #Create a DataNode for the Task
            datanodes[i] = self.temanejo.remoteControl.datanodes[int(i)]
            if self.temanejo.graphModel.nodeExists(i):
                self.temanejo.graphModel.getNode(i).setDataNode(datanodes[i])
            # Create the Dataproperty
            for attr in nodeAttributes.keys():
                self.temanejo.dataParser.parseTaskDepAddedProperty((attr, nodeAttributes[attr]), int(i), "node")

        #Parse Edge
        for i in aGraphToReadIn.edges(keys=True):

            edgeAttributes = aGraphToReadIn.get_edge(i[0], i[1]).attr
            self.temanejo.remoteControl.dataedges[i] = DataEdge.DataEdge((i[0], i[1]), properties=edgeAttributes)
            self.temanejo.graphlayoutIO.addDep(i[0], i[1], i[2])

            dataedges[i] = self.temanejo.remoteControl.dataedges[i]

            if self.temanejo.graphModel.edgeExists(i):
                self.temanejo.graphModel.getEdge(i).setDataEdge(dataedges[i])
            # Create the Dataproperty
            for attr in edgeAttributes.keys():
                self.temanejo.dataParser.parseTaskDepAddedProperty((attr, edgeAttributes[attr]), i, "edge")


    def importFromXML(self,fileName=None):
        print "----------"
        print "IMPORT XML"
        print "----------"
        if fileName is None:
            fileName = self.openFile("xml")
        if (fileName == 0):
            return

        with open(fileName, 'r') as xmlFile:
            for line in xmlFile:
                if len(line) < 2:
                    continue
                lineList= line.split("#")
                #os <<  m_event << xml_seperator << m_client_id << xml_seperator <<  m_timestamp;
                event  =lineList[0]
                clientId  =lineList[1]
                timestamp  =lineList[2]

                if int(event) == 5: #AYU_ADDTASK
                    #os << xml_seperator << m_task_id <<  xml_seperator << m_scope_client_id << xml_seperator << m_scope_id << xml_seperator << m_task_label << "\n";
                    task_id  = lineList[3]
                    scope_client_id  = lineList[4]
                    scope_id  = lineList[5]
                    task_label = lineList[6]

                    nodeAttributes = dict()
                    unique_taskId = int("%s%s"%(clientId, task_id))

                    #Set the nodeAttributes
                    nodeAttributes["None"]        = "None"
                    nodeAttributes["id"]          = unique_taskId
                    nodeAttributes["taskId"]      = task_id
                    nodeAttributes["clientId"]    = clientId
                    nodeAttributes["hostId"]      = helpers.get_hostId(int(clientId))
                    nodeAttributes["processId"]   = helpers.get_processId(int(clientId))
                    nodeAttributes["label"]       = task_label
                    nodeAttributes["state"]       = False
                    nodeAttributes["paused"]      = False
                    nodeAttributes["scope"]       = scope_id
                    nodeAttributes["scopeClient"] = scope_client_id

                    # self.temanejo.dataParser.parseTask_plain(unique_taskId, clientId, nodeAttributes)
                    self.temanejo.dataParser.parseTask_plain(unique_taskId, nodeAttributes)

                elif int(event) == 6:#AYU_ADDDEPENDENCY
                    #os << xml_seperator << m_dependency_id <<  xml_seperator << m_from_id << xml_seperator << m_to_id << xml_seperator << m_to_client_id  << xml_seperator << m_dependency_label <<"\n";
                    dependency_id  = lineList[3]
                    from_id  = lineList[4]
                    to_id  = lineList[5]
                    to_client_id = lineList[6]
                    dependency_label = lineList[7]

                    edgeAttributes = dict()
                    unique_depId = int(u"%s%s"%(clientId, dependency_id))
                    fromToId = (u"%s%s"%(clientId, from_id), u"%s%s"%(to_client_id, to_id))

                    #Set the dependency attributes
                    edgeAttributes["None"] = "None"
                    edgeAttributes["depId"] = unique_depId
                    edgeAttributes["id"] = dependency_id
                    edgeAttributes["label"] = dependency_label
                    edgeAttributes["fromToId"] = fromToId

                    self.temanejo.dataParser.parseDependency_plain(unique_depId, fromToId, edgeAttributes)

                elif int(event) == 9:#AYU_SETPROPERTY
                    #os << xml_seperator << m_property_owner_id <<  xml_seperator << m_key << xml_seperator << m_value << "\n";
                    property_owner_id  = lineList[3]
                    key  = lineList[4]
                    value  = lineList[5]

                    propOwnerId   = int("%s%s"%(clientId, property_owner_id))

                    self.temanejo.dataParser.parseProperty_plain(propOwnerId, key, value)

                else:
                    logger.error("not recognized XML import ")

class DDDEvents(object):

    def __init__(self, temanejo):
        self.temanejo = temanejo

    def startDebugger(self):
        datanodes = self.temanejo.remoteControl.datanodes
        hostnamesPIDs=""
        applicationPathName=""
        for node in datanodes:
            nodeAttributes = datanodes[node].getProperties()
            if  nodeAttributes["taskId"] == nodeAttributes["scope"] :
                #print nodeAttributes["taskId"], nodeAttributes["scope"]
                #TODO check if attribute availible
                if 'hostname' in nodeAttributes.keys() and 'pid' in nodeAttributes.keys():
                    host = nodeAttributes["hostname"]
                    pid = nodeAttributes["pid"]
                    if len(hostnamesPIDs)>0 and hostnamesPIDs[-1:] != ',' :
                        hostnamesPIDs+=","
                    hostnamesPIDs+=host+":"+pid
                # not needed any more
                #if 'path+application' in nodeAttributes.keys():
                #    applicationPathName=nodeAttributes["path+application"]


        print "applicationPathName",applicationPathName
        print "hostnamesPIDs",hostnamesPIDs
        debugger_command_line = 'ddt --plugins="Temanejo2 Task-Orientated Debugger (Internal only)" --attach=%s %s ' % (hostnamesPIDs, applicationPathName)
        print debugger_command_line

        ext_debugger='ddt'
        known_terminals = {
            "xterm" : ['xterm', '-geometry', '80x40',
                        '-title', '\"', ext_debugger, ' started by Temanejo\"',
                        '-e', debugger_command_line],
            'xfce4-terminal' : ['xfce4-terminal', '--geometry', '80x40',
                        '--title', '\"', ext_debugger, ' started by Temanejo\"',
                        '-e', debugger_command_line],
            'rxvt' : ['rxvt', '-geometry', '80x40',
                        '-title', '\"', ext_debugger, ' started by Temanejo\"',
                        '-e', debugger_command_line],
            'urxvt' : ['urxvt', '-geometry', '80x40',
                        '-title', '\"', ext_debugger, ' started by Temanejo\"',
                        '-e', debugger_command_line],
            'rxvt-unicode' : ['rxvt-unicode', '-geometry', '80x40',
                        '-title', '\"', ext_debugger, ' started by Temanejo\"',
                        '-e', debugger_command_line]
        }

        try:
            terminal = os.environ['TERM']
        except:
            logging.warning('terminal %s not found. Falling back to xterm.' % terminal)
            terminal = 'xterm'
        if not terminal in known_terminals:
            logging.warning('terminal %s not known. Falling back to xterm.' % terminal)
            terminal = 'xterm'


        cmd = known_terminals[terminal]


        logging.info('executing external debugger:\n%s' % ' '.join(cmd))

        msg = ''

        try:
            self.debugger = subprocess.Popen(' '.join(cmd), shell=True)  # , stdin=subprocess.PIPE)
        except  (OSError, msg):
            logging.error(msg)
            logging.error(
                'Opening subprocess for external debugger failed:\n\t%s', ' '.join(cmd))
            return

        print "DEBUGGER STARTED"
