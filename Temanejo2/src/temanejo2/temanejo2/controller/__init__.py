"""!
@brief The controller package is the main Application package. Everything will be started and Controlled from
Objects in this package 

@author Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com

So this package contains the main application loop where everything will be controlled and updated if marked dirty.
It contains a Connector to Ayudame, a data parser/layout-routine etc.
Also all events should be handled in here. So in case that Events are triggered anywhere else as on Objects in
this package, there will be a corresponding object/method which will handle everything out of this package,
just to make sure all objects which cause changes to the current state can be found in one location.
"""

__all__=[]