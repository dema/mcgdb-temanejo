'''!
@brief All graph interactions will be defined here

This Module provides all objects needed for interactions with the graph
All the dataflow will be controlled here but also the user interactions.


@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''
import logging

from PySide import QtCore, QtGui

from temanejo2.temanejo2.lib import configuredValues

logger = logging.getLogger(__name__)


## 
#  This class will provide all methods for handling user caused events, like 
#  key events, mouse events etc. The events will be catched in the %TemanejoGraph Class 
#  and will be redirected into this class. This class is then the container for 
#  all events and will handle all the stuff which is needed to react on users options
#  for changing the look of the graph.     
class GraphUserInputEvents(object):

    ##
    # Constructs the UserInput controller object.
    # @param widget The Graph which should be controlled 
    def __init__(self,graphEventHandler):
        self._eventHandler = graphEventHandler
        self.widget = graphEventHandler.widget
        self.pressedKey = None
        self.mousePressedOrigin = None
        self.scalingFactor = 1.0
        self.translation = [0,0]
        
    #===============================================================================================
    # KEY EVENTS
    #===============================================================================================
    def keyPressEvent(self, evt):
        self.pressedKey = evt.key()
        
        
    def keyReleaseEvent(self,evt):
        self.pressedKey = None
        
    
    #===============================================================================================
    # MOUSE EVENTS
    #===============================================================================================
    def mousePressEvent(self,evt):
        if evt.buttons() == QtCore.Qt.RightButton:
            return
        self.mousePressedOrigin = evt.pos()
        itemAtEvt = self.widget.itemAt(evt.pos())
        self.widget.setDragMode(QtGui.QGraphicsView.DragMode.ScrollHandDrag)
        if itemAtEvt != None and (self.pressedKey == configuredValues.SELECT_ITEM_NOT_SCENE):
            itemAtEvt.mousePressEvent(evt)
        else:
            logger.debug("nothing implemented on this Event") 
    
    def mouseReleaseEvent(self,evt):
        self.mousePressedOrigin = None
        itemAtEvt = self.widget.itemAt(evt.pos())
        self.widget.setDragMode(QtGui.QGraphicsView.DragMode.RubberBandDrag)
        if itemAtEvt != None and (self.pressedKey == configuredValues.SELECT_ITEM_NOT_SCENE):
            itemAtEvt.mouseReleaseEvent(evt)
        else:
            logger.debug("nothing implemented on this Event")
    
    def mouseDoubleClickEvent(self,evt):
        itemAtEvt = self.widget.itemAt(evt.pos())
        if itemAtEvt != None and (self.pressedKey == configuredValues.SELECT_ITEM_NOT_SCENE):
            itemAtEvt.mouseDoubleClickEvent(evt)
        else:
            logger.debug("nothing implemented on this Event")


    def mouseMoveEvent(self,evt):
        #Deactivate Transformation and resize anchor
        self.widget.setTransformationAnchor(QtGui.QGraphicsView.NoAnchor)
        self.widget.setResizeAnchor(QtGui.QGraphicsView.NoAnchor)
        
#        itemAtEvt = self.widget.itemAt(evt.pos())
#        if itemAtEvt != None:
#            itemAtEvt.mouseMoveEvent(evt)
        if self.mousePressedOrigin:
            multiplier = 1.0
            if self.pressedKey == configuredValues.MOVE_SCENE_MULTIPLICATOR:
                multiplier = 10.0
            self._eventHandler.translate_scene(multiplier*(evt.x() - self.mousePressedOrigin.x()),
                                               multiplier*(evt.y() - self.mousePressedOrigin.y()))
            self.translation[0] += multiplier*(evt.x() - self.mousePressedOrigin.x())
            self.translation[1] += multiplier*(evt.y() - self.mousePressedOrigin.y()) 
            self.mousePressedOrigin = evt.pos()
        
        #Reactivate transformation and resize anchor
        self.widget.setTransformationAnchor(QtGui.QGraphicsView.AnchorUnderMouse)
        self.widget.setResizeAnchor(QtGui.QGraphicsView.AnchorUnderMouse)    
            
        
    
    ##
    # If the mousewheel is used the graph can be zoomed in and out
    def wheelEvent(self, evt):
        #Deactivate Transformation and resize anchor
        self.widget.setTransformationAnchor(QtGui.QGraphicsView.NoAnchor)
        self.widget.setResizeAnchor(QtGui.QGraphicsView.NoAnchor)
        #Get mouseposition
        target_viewport_pos = self.widget.mapToScene(evt.pos())
        #Translate scene
        self.widget.translate(target_viewport_pos.x(),target_viewport_pos.y())
        self.translation[0] += target_viewport_pos.x()
        self.translation[1] += target_viewport_pos.y() 
        #Zoom Scene
        if evt.delta() > 0:
            self._eventHandler.zoom_ctrl(configuredValues.ZOOM_FACTOR)
        else:
            self._eventHandler.zoom_ctrl(1/configuredValues.ZOOM_FACTOR)
        #Translate scene backwards
        self.widget.translate(-target_viewport_pos.x(),-target_viewport_pos.y())
        self.translation[0] -= target_viewport_pos.x()
        self.translation[1] -= target_viewport_pos.y()
        
        #Reactivate transformation and resize anchor
        self.widget.setTransformationAnchor(QtGui.QGraphicsView.AnchorUnderMouse)
        self.widget.setResizeAnchor(QtGui.QGraphicsView.AnchorUnderMouse)
