'''!
@brief In this module the remote connection will be established and all events which result of 
       this will be spawned.
       



@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''
import logging
from time import sleep

#import pygraphviz as pvg
import networkx as nx

from ayudame import ayu_socket
from temanejo2.temanejo2.controller.graph.remoteControls import RemoteCommunicator, ddtComunicator
from temanejo2.temanejo2.core import graphLayout, mvcConnector
from temanejo2.temanejo2.core.io.style_provider import StyleProvider
from temanejo2.temanejo2.core.items import DataEdge, DataNode, miscDialogs, DataProperties
from temanejo2.temanejo2.lib import constants

logger = logging.getLogger(__name__)





##
#    This object handles all connections to the remote Application
#    here it will be the connection to Ayudame.
#    The socket-connection will be set up here, but also the Graphmodelentries will
#    be created, updated and deleted
class RemoteEvents(object):
    ##
    #    Creates the Remote Events object.
    #    @param temanejo The running instance of the application. It needs to be passed in here 
    #                     in order to get a handle to the Graphmodel
    def __init__(self, temanejo):
        self.temanejo = temanejo
        ##
        # @member This is a dictionary in which all tasks etc. will be held as DataNodes
        #         according to their id
        self.datanodes = dict()
        
        ##
        # @member This is a dictionary in which all dependency information will be held as DataEdges
        #         keyed by their id
        self.dataedges = dict() 
        
        ##
        # @member This is a dict of properties which can be send by from Ayudame directly or
        #         by sending a node or a dependency...
        self.dataproperties = dict()
        self.dep_dataproperties = dict()
        
        # Initialize Layout Object

        # self.createGraphByReadingDot("./dot_files/simple_graph.dot")
        # self.createGraphByReadingDot("./dot_files/J_jacobi_80.dot")
        self.createEmptyGraphToBeFilledByRemote()

        #Initialize Handle to the remoteController
        self.rmc = None
        # Initialize Handle to ddt comunicator
        self.ddt_communicator = None
        self.ddt_parser = None
        

        ##
        # @TODO: layoutCtrl should be created in the Temanejo class
        # @member The layout control handles all the graphlayout
        self.layoutCtrl = graphLayout.GraphLayout(self.temanejo, self.graph, self.datanodes)

        # Set the active port to None in initialisation
        self._activePort = None

# ---------------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------
# ----- PGV-Graphcreation ---------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------

    ##
    #  Creates a graph by reading it from a dot file. 
    #  Done for standalone mode
    #  @param path_to_file The path to the dotFile which should be displayed on the graph 
    def createGraphByReadingDot(self, path_to_file):
        # The ranksep is computed by configuredValues.DEFAULT_SEP_FACTOR * configuredValues.DEFAULT_NODESIZE /120. since the ranksep is kind of counterintuitiv
        self.graph = nx.MultiDiGraph(path_to_file)
        # self.graph = pvg.agraph.AGraph(path_to_file, rankdir=configuredValues.LAYOUT_DIRECTION_DOT, strict=False, directed = True,
        #                                ranksep=str(configuredValues.DEFAULT_SEP_FACTOR * configuredValues.DEFAULT_NODESIZE / 120.),
        #                                nodesep=str(configuredValues.DEFAULT_SEP_FACTOR * configuredValues.DEFAULT_NODESIZE / 120.))


        self.layout_routine = StyleProvider.register()

        for i in self.graph.nodes():
            nodeAttributes = self.graph.get_node(i).attr
            nodeAttributes["status"] = False
            nodeAttributes["paused"] = False
            self.datanodes[int(i)] = DataNode.DataNode(int(i), None, nodeAttributes)
            self.layout_routine.addNode(i)
        for i in self.graph.edges():
            edgeAttributes = self.graph.get_edge(i[0],i[1]).attr
            edgeAttributes["status"] = False
            edgeAttributes["paused"] = False
            self.dataedges[i] = DataEdge.DataEdge((i[0],i[1]), edgeAttributes)
            self.layout_routine.addEdge(i[0],i[1])
    

    ##
    #   Creates an empty PgVGraph which will be filled by the remote Application
    def createEmptyGraphToBeFilledByRemote(self):
        # The ranksep is computed by constants.DEFAULT_SEP_FACTOR * constants.DEFAULT_NODESIZE /120. since the ranksep is kind of counterintuitiv
        self.graph = pvg.agraph.AGraph(rankdir=constants.LAYOUT_DIRECTION_DOT,
                                       ranksep=str(constants.DEFAULT_SEP_FACTOR * constants.DEFAULT_NODESIZE / 120.),
                                       nodesep=str(constants.DEFAULT_SEP_FACTOR * constants.DEFAULT_NODESIZE / 120.))
        self.layout_routine = StyleProvider.register()
        #self.dataproperties['None']     = DataProperties.DataProperty('None', 'None', p_type='node')
        #self.dep_dataproperties['None'] = DataProperties.DataProperty('None', 'None', p_type='dep')


#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
#----- RemoteConnections ---------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
               
    ##
    #   This method opens a server 
    #   - Building up a AyuSocket
    #   - Setting the server_data (connecting tho the specified port)
    #   - Initializing the socket
    #   - Building an instance of the remoteCommunicator
    #   - And finally starting the  remoteCommunication
    #
    #   @param port The port which have to be specified in order to read the data from the correct port
    def openServer(self,port):
        self._activePort = port
        #Create a AyuSocket
        s = ayu_socket.Ayu_Socket()
        #Connect to specific port
        s.set_server_data(port)
        #Initialize socket
        connectionFeedback = s.init()
        if connectionFeedback == -1:
            self.temanejo.statusBar().showMessage("Couldn't open server on port %s"%port)
            logger.error("Couldn't open server on port.\nGiven Port was: %s"%port)
            erM = "Couldn't open server on port %s because \nthe given port is invalid or is already\nin use!"%port
            miscDialogs.messageDialog(erM,
                                     title = "Connection Error")
            return
        #needed to bypass //TODO
        s.set_Temanejo()
        #Create RemoteConnection
        self.rmc = RemoteCommunicator.RemoteComunicator()
        self.rmc.setSocket(s)
        self.rmc.start()
        mvcConnector.connectRemoteControllerToDataParser(self.temanejo)
        self.temanejo.statusBar().showMessage("Opened server on port %s"%port)
    ##
    #   This method provides a functionality to disconnect from the graph by just calling the 
    #   terminate method of the active RemoteComunicator instance
    def disconnectFromRemote(self):
        if self.rmc is None:
            logger.warning("There is no remote Connection to disconnect from")
            return
        if self.rmc.isActive or self.rmc.isRunning():
            self.rmc.terminate()
            self.rmc.quit()
        self.temanejo.statusBar().showMessage("Disconnected from remote Application")
    
    ##
    #
    def getActivePort(self):
        return self._activePort

    #===============================================================================================
    # DDT Communications
    #===============================================================================================

    def openDDTServer(self, host='localhost', port=8889):
        self.ddt_communicator = ddtComunicator.DDTComunicator(self.temanejo, host=host, port=port)
        self.ddt_parser = ddtComunicator.DDTMessage(self.temanejo)
        mvcConnector.connectDDTRemote(self.temanejo)
        self.ddt_communicator.start()
        self.temanejo.statusBar().showMessage("Opened server on port %s"%port)

    ##
    #
    def closeDDTconnection(self):
        if self.ddt_communicator is None:
            logger.info("There is no ddt Connection open to disconnect from")
            return
        self.ddt_communicator.close()    
        if self.ddt_communicator.isActive or self.ddt_communicator.isRunning():
            self.ddt_communicator.terminate()
            self.ddt_communicator.quit()

    ##
    #
    def connector_for_update_ddt_button(self):
        if (self.ddt_parser == None):
            logger.error("No active Connection to ddt")
        else:
            self.ddt_parser.ddt_update_state()
