__author__ = 'Mathias Nachtmann'

'''!
@brief Import/Export Actions are defined here, as well as starting and communicating with external apps like ddd

Different importer and exporter are written in here

@author: Mathias Nachtmann
@contact temanejo@hlrs.de
'''

def get_hostId(clientId):
    return (clientId & 0xFFFFFF) >> 0

def get_processId(clientId):
    return (clientId & 0xFFFFFF000000) >> 24

def get_eventHandlerId(clientId):
    return (clientId & 0xFFFF000000000000) >> 48
