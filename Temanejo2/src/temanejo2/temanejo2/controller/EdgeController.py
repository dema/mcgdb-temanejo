'''!
@brief All Edge Interactions will be defined here

This Module provides a EdgeEvents object in which all events caused by the edge will be handled

@author: Stefan Reinhardt, stefan.reno.reinhardt@googlemail.com
@contact temanejo@hlrs.de
@copyright  (C) 2015, HLRS, University of Stuttgart
            This software  is published under the terms of the BSD license.
            See the LICENSE file for details.
'''
import logging

logger = logging.getLogger(__name__)

## 
#  This class will provide all methods for handling node caused events, like 
#  key events, mouse events etc.    
class EdgeEvents(object):

    ##
    # Constructs the Node controller object.
    # @param viewWidget The View where all the items are painted 
    def __init__(self,viewWidget):
        self.viewWidget = viewWidget
        
    #===============================================================================================
    # MOUSE EVENTS
    #===============================================================================================
    def mousePressEvent(self,evt):
        logger.debug("nothing implemented on this EgdeEvent") 
    
    def mouseReleaseEvent(self,evt):
        logger.debug("nothing implemented on this EgdeEvent")
    
    def mouseDoubleClickEvent(self,evt):
        logger.debug("nothing implemented on this EgdeEvent")
        
    def mouseMoveEvent(self,evt):
        logger.debug("nothing implemented on this EgdeEvent")
