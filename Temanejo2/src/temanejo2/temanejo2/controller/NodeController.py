'''!
@brief All Node Interactions will be defined here

This Module provides a NodeEvents object in which all events caused by the node will be handled

@author: Stefan Reinhardt
@contact stefan.reno.reinhardt@googlemail.com
'''
import logging

logger = logging.getLogger(__name__)


## 
#  This class will provide all methods for handling node caused events, like 
#  key events, mouse events etc.    
class NodeEvents(object):

    ##
    # Constructs the Node controller object.
    # @param viewWidget The View where all the items are painted 
    def __init__(self,viewWidget):
        self.viewWidget = viewWidget
        
    #===============================================================================================
    # MOUSE EVENTS
    #===============================================================================================
    def mousePressEvent(self,evt):
        logger.info("nothing implemented on this NODEEvent") 
    
    def mouseReleaseEvent(self,evt):
        logger.info("nothing implemented on this NODEEvent")
    
    def mouseDoubleClickEvent(self,evt):
        logger.info("nothing implemented on this NODEEvent")
        
    def mouseMoveEvent(self,evt):
        logger.info("nothing implemented on this NODEEvent") 
