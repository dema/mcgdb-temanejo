print("THIS FILE SHOULD NOT BE LOADED !")

import logging as log
import inspect
import os, sys, subprocess, tempfile, re

from multiprocessing import Queue

from PySide.QtWebKit import *
from PySide.QtGui import *
from PySide.QtCore import *
from PySide.QtCore import Signal as pyqtSignal
from PySide.QtCore import Slot as pyqtSlot

class QtToJSWorker(QObject):
    evaluateJavaScript = pyqtSignal('QString', name="evaluateJavaScript")

    def __init__(self, to_qt):
        super(self.__class__, self).__init__()
        self.to_qt = to_qt
        self.dead = False
    def run(self):
        item = None

        print("Listening for mcGDB instructions ...")
        while not self.dead:
            try:
                item = self.to_qt.get()
                print(item)
            except EOFError:
                print("Connection to GDB terminated, turning off the GUI.")
                break

            try:
                if item == "quit": break
                elif item.startswith("python"):
                    item = item.replace("python ", "")
                    exec(item)
                else:
                    self.evaluateJavaScript.emit(item)
                    
            except Exception as e:
                msg = "QtQueueWorker failed with item='{}': {}".format(item, e)
                print(msg)
                log.error(msg)
        print("Threat for JS quits, bye.")

class ObjectForQT(QObject):
    """ See http://blog.mathieu-leplatre.info/le-piege-des-qthread-fr.html for
    explanations of this class. """
    
    startWorker = pyqtSignal(name="startWorker")

    def emitStartWorker(self):
        self.startWorker.emit()
    
# http://pysnippet.blogspot.fr/2010/01/calling-python-from-javascript-in-pyqts.html
class QtFromJS (QObject):
    def __init__(self, to_gdb):
        super(self.__class__, self).__init__()
        self.to_gdb = to_gdb
        
    @pyqtSlot(str)
    def py_exec(self, msg):
        self.to_gdb.put("exec")
        self.to_gdb.put(msg)

    @pyqtSlot(str)
    def execute(self, msg):
        self.to_gdb.put(msg)
        
    @pyqtSlot()
    def release_control(self):
        self.to_gdb.put("release control")
        
    @pyqtSlot(str)
    def switch_thread(self, s):
        self.to_gdb.put("thread {}".format(s))
        self.to_gdb.put("omp sequence --gui --sync")
        
    def _svg(self):
        RUN_SVG = "/home/kevin/travail/sample/ompss-examples/run2.svg"
        if not os.path.exists(RUN_SVG):
            return ""
        
        with open(RUN_SVG) as run_svg:
            return "".join(run_svg.readlines())

    svg = Property(str, fget=_svg)  
    
class SeqDiagramController():
    def __init__(self, temanejo, seqDiagramView):
        self.temanejo = temanejo
        self.seqDiagramView = seqDiagramView
        
    def userdataAdded(self, userdata):
        target, _, data = userdata.get_data().partition("#")
        if target != "MCGDB":
            log.info("USER DATA: ignore target {}".format(target))
            return
        self.seqDiagramView.reload(what=data)
        
class SeqDiagramView(object):
    def __init__(self, webView):
        super(self.__class__, self).__init__()

        self.short = True
        
        self.proxy = None
        self.win = webView

        #signal.signal(signal.SIGINT, signal.SIG_DFL)
        
        to_qt = Queue()
        to_gdb = Queue()

        self.load_assets()

        self.proxy = QtFromJS(to_gdb)
        self.win.page().mainFrame().addToJavaScriptWindowObject("mcgdb", self.proxy)

        
        self.reload("INIT")
        
        self.win.setWindowTitle(u"mcGDB")
        self.win.page().settings().setAttribute(QWebSettings.DeveloperExtrasEnabled, True)

        worker = QtToJSWorker(to_qt)
        worker.evaluateJavaScript.connect(self.win.page().mainFrame().evaluateJavaScript)
        
        self.thread = QThread()
        worker.moveToThread(self.thread)

        obj = ObjectForQT()
        obj.startWorker.connect(worker.run)
        self.thread.start()
        obj.emitStartWorker()

    def load_assets(self):
        RESOURCE_RELATIVE_PATH = "../../resources/mcgdb-seqdiag/"
        this_file = os.path.abspath(inspect.getsourcefile(self.__class__))
        this_dir = os.path.dirname(this_file)
        this_dir = os.path.abspath(os.path.dirname(this_dir+"/"+RESOURCE_RELATIVE_PATH))
        
        with open("{}/graphdisplay.css".format(this_dir)) as css_file:
            self.css = "".join(css_file.readlines())
        with open("{}/jquery.js".format(this_dir)) as js_file:
            self.jquery = "".join(js_file.readlines())
        with open("{}/graphdisplay.js".format(this_dir)) as js_file:
            self.js = "".join(js_file.readlines())
        with open("{}/template.html".format(this_dir)) as fhtml:
            self.html = "".join(fhtml.readlines())
        with open("{}/init_content.html".format(this_dir)) as fhtml:
            self.init = "".join(fhtml.readlines())
            
    def transform(self, src):
        tmp_src = tempfile.NamedTemporaryFile(prefix="mcgdb_", suffix=".seq")
        tmp_dst = tempfile.NamedTemporaryFile(prefix="mcgdb_", suffix=".svg")

        with tmp_src as fsrc:
            fsrc.write(src)
            fsrc.flush()
            seqdiag = subprocess.Popen(["seqdiag", "-T", "svg", tmp_src.name, "-o", tmp_dst.name], stderr=subprocess.PIPE)
            out, err = seqdiag.communicate()
            failed = False
            for line in err.split("\n"):
                if "Error" in line or "Exception" in line:
                    if not failed:
                        print("ERROR: seqdiag {} failed:".format(tmp_src.name))
                        failed = True
                    print(line)
            if failed:
                return False
        
        with tmp_dst as fdst:
            svg_lines = fdst.readlines()

        with open("/home/kevin/travail/sample/ompss-examples/run.svg", "w") as fsvg:
            fsvg.write("".join(svg_lines))
        
        return svg_lines
        
    def reload(self, what=""):
        is_init = False

        if what:
            order, _, what = what.partition("#")
            log.warn("order: {} ({})".format(order, what))
            if order == "INIT":
                is_init = True
            elif order == "RELOAD":
                self.load_assets()
                log.warn("HTML assets reloaded.")
                return
            elif order == "WHEN ?":
                import pdb;pdb.set_trace()
            elif order == "SEQUENCE":
                run_src = what
            else:
                log.warn("Unknown order: {} ({})".format(order, what))
                return
        else:
            log.warn("SEQVIEW: empty message received, update from disk.")
            with open("/home/kevin/travail/sample/ompss-examples/run") as frun:
                run_src = "".join(frun.readlines())

        def get_svg():
            # text --> svg
            svg_lines = self.transform(run_src)

            if svg_lines is False:
                return ["<b>Sequence diagram generation failed.</b><br/>\n"]+[run_src.replace("\n", "<br/>\n").replace(" ", "&nbsp;")]
            
            try:
                # fix svg declaration
                svg_dec = "<svg dec not set>"
                
                svg_dec = svg_lines[2]
                # viewBox="tx, ty, bx, by"
                viewBox = re.findall('"(.*?)"', svg_dec)[0]
                tx, ty, bx, by = viewBox.split()

                # height="by" width="by"
                new_svg_dec = svg_dec.replace('viewBox="{}"'.format(viewBox),
                                              'height="{}" width="{}"'.format(by, bx))
                svg_lines[2] = new_svg_dec
            except Exception as e:
                print("Error, could not parse width from SVG declaration: {}".format(e))
                print("SVG declaration found is: {}".format(svg_dec))
                
            return svg_lines
        
        svg_lines = self.init if is_init else get_svg()
            
        self.win.setHtml(self.html.format(
                jquery=self.jquery,
                js=self.js,
                css=self.css,
                svg="".join(svg_lines))
        )
