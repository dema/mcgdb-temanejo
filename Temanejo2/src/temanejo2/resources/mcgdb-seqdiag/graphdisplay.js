if (!window.Set) {
    window.Set = function (l) {
        this._s = {};
        l.forEach(
            (function (e) {
                this._s["%" + e] = e;
            }).bind(this)
        );
    };

    window.Set.prototype.contains = function (e) {
        return this._s.hasOwnProperty("%" + e);
    };

    window.Set.prototype.forEach = function (f) {
        for (var i in this._s) {
            if (this._s.hasOwnProperty(i)) {
                f(this._s[i]);
            }
        }
    };
}
function isNormalInteger(str) {
    var n = ~~Number(str);
    return String(n) === str && n >= 0;
}

/********************/

function refresh() {
    alert("can't refresh")
}

function bind() {
    $("body").html(mcgdb.svg)
    bind_svg()
}

function bind_svg() {
    $("rect").click(function() {
        var worker_id = $(this).next().text()
        
        if (isNormalInteger(worker_id)) {
            mcgdb.switch_thread(worker_id)
        }
    });
}

window.onload = function () {

    //bind()

    $("body").keydown(function(evt) {
        //mcgdb.log(event.which)
        if (event.which == 84) { // key 't'
            evt.preventDefault();
            mcgdb.py_exec("print('hello')")
        }
        if (event.which == 82) { // key 'r'
            evt.preventDefault();
            refresh()
        }
        if (event.which == 81) { // key 'q'
            evt.preventDefault();
            mcgdb.release_control()
        }
    })
    window.scrollTo(0,document.body.scrollHeight);
};
